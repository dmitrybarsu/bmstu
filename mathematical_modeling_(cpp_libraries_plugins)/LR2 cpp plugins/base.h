//
// Created by dmitry on 01.12.2020.
//

#ifndef LAB2_BASE_H
#define LAB2_BASE_H

#include <string>
#include <cmath>
#include <map>

class interfaceBasePlugin {
public:
    virtual std::string getInterfaceName(std::string objectName)const{
        objectName = "interfaceBasePlugin";
        return  objectName;
    };

    virtual bool isInstanceOf(const std::string ptrInterfaceName)const{
        return (ptrInterfaceName == "interfaceBasePlugin");
    };

    virtual std::string getPluginName(std::string objectName)const = 0;
    virtual ~interfaceBasePlugin ()= default;;
};

class  interfaceSolverPlugin : public interfaceBasePlugin {
public:
    virtual std::string getInterfaceName(std::string objectName)const {
        objectName = "interfaceSolverPlugin";
        return objectName;
    };

    virtual  bool  isInstanceOf(const std::string ptrInterfaceName)const {
        return (ptrInterfaceName == "interfaceSolverPlugin") || interfaceBasePlugin::isInstanceOf(ptrInterfaceName);
    };
    virtual std::string getPluginName(std::string objectName)const = 0;
    virtual void Solver(std::vector<double> &T, struct Constants c) = 0;
    ~interfaceSolverPlugin() override = default;
};

class  interfaceShowResultsPlugin : public interfaceBasePlugin {
public:
    virtual  std::string getInterfaceName(std::string objectName)const {
        objectName = "interfaceShowResultsPlugin";
        return  objectName;
    };

    virtual  bool  isInstanceOf(const std::string ptrInterfaceName)const {
        return (ptrInterfaceName == "interfaceShowResultsPlugin") || interfaceBasePlugin::isInstanceOf(ptrInterfaceName);
    };

    virtual std::string getPluginName(std::string objectName)const = 0;
    virtual void PrintResults(const std::vector<std::string>& solverNames, const std::vector<std::vector<double>>& T,const struct Constants& c) = 0;
    virtual void PrintL2(std::vector<std::vector<double>>& T, struct Constants& c, const std::vector<interfaceSolverPlugin *>& solverPlugins, std::vector<int>& selectedSolversId) = 0;
    ~interfaceShowResultsPlugin() override = default;
};

struct Constants {
    std::string method = "euler";
    const int NumberOfIterations =  100;    // iterating the solution ode
    const double T0 = 0.;                   // start object temperature
    const double T1 = 100.;                 // env temperature
    double gamma = 0.5;                     // aspect ratio
    double gammaBounds[2] = {0.5, 0.5};
    double gammaStep = 0.01;
    double step = 0.05;                     // time step = (t_i - t_i-1)
};

void analit(std::vector<double> &T, const struct Constants& c) {
    for(int i = 1; i < T.size(); i++)
        T[i] = c.T1 + (c.T0 - c.T1) * exp(-c.gamma * (i * c.step)); // t = i * c.step
}

double L2(std::vector<double> &MethodT, std::vector<double> &analitT) {
    double xPow2Sum = 0.;
    for(int i = 0; i < MethodT.size(); i++) {
        analitT[i] = analitT[i] ? analitT[i] : 1e-15;  // 1e-15 - для избежания деления на ноль
        xPow2Sum += pow((MethodT[i] - analitT[i]) / analitT[i], 2);
    }
    return sqrt(xPow2Sum / MethodT.size());
}

std::string plgName;

#endif //LAB2_BASE_H
