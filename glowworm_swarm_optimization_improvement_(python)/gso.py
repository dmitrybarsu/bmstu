import numpy as np
import matplotlib.pyplot as plt

# model constants
DIM = 2
leftBound = 0
rightBound = 1
ro = 0.4
gamma = 0.6
betta = 0.08
nt = 5
s = 0.03
lStart = 5.0
rSensory = 1
glowwormsNum = 50
maxIter = 1e3
EPS = 1e-5
dtDefault = 30


def d(x_i, x_j):
    return np.sqrt(sum([(x_i[i] - x_j[i]) ** 2 for i in range(len(x_i))]))


def Fitness(x):
    return sum([x_i ** 2 for x_i in x])


def createGlowwormsMovementPlot(x, l, radius):
    x1, x2 = np.mgrid[
             leftBound:rightBound + 1:100j,
             leftBound:rightBound + 1:100j
             ]
    f = Fitness([x1, x2])
    fig, axe = plt.subplots()
    levelValues = np.arange(0, 6, 1)
    cs = axe.contour(x1, x2, f, levels=levelValues)
    axe.clabel(cs)
    axe.scatter(0, 0, color="red")
    plt.xlim(leftBound, rightBound)
    plt.ylim(leftBound, rightBound)

    i = 0
    for x_i in x:
        # # uncomment to see neighborhood zones
        # neighborZone = plt.Circle((x_i[0], x_i[1]), radius[i], color='r', fill=False)
        # axe.add_patch(neighborZone)
        plt.scatter(x_i[0], x_i[1], s=l[i] * 50, color="yellow")
        i += 1
    for x_i in x:
        plt.scatter(x_i[0], x_i[1], s=10, color="black")
    plt.show()


def plotAvgTarget(avgTarget_t):
    fig, ax = plt.subplots()
    ax.plot(avgTarget_t)
    ax.grid()
    ax.set_xlabel("t")
    ax.set_ylabel("f(x)")
    plt.savefig('convergence.png')
    plt.show()



def gso():
    x = np.random.rand(glowwormsNum, DIM) * (rightBound - leftBound) + leftBound
    l = np.full(glowwormsNum, lStart)
    r = np.full(glowwormsNum, rSensory)

    avgTarget_t = [sum([Fitness(x_i) for x_i in x]) / glowwormsNum]
    prevDif = 0

    t = 0
    dt = dtDefault
    while t < maxIter and dt:
        # step 1
        for i in range(glowwormsNum):
            l[i] = (1 - ro) * l[i] + gamma * Fitness(x[i])

        # step 2
        N = []
        for i in range(glowwormsNum):
            N_i = []
            for j in range(glowwormsNum):
                if d(x[i], x[j]) < r[i] and l[i] < l[j]:
                    N_i.append(j)
            N.append(N_i)

        # step 3
        P = []
        for i in range(glowwormsNum):
            P_i = []
            denSum_i = 0
            for k in N[i]:
                denSum_i += l[k] - l[i]
            for j in N[i]:
                P_i.append((l[j] - l[i]) / denSum_i)
            P.append(P_i)

        # step 4
        for i in range(glowwormsNum):
            if len(P[i]) != 0:
                j = np.nanargmax(P[i])
                if d(x[j], x[i]) > 0.1 ** DIM and Fitness(x[i] + s * ((x[j] - x[i]) / d(x[j], x[i]))) < Fitness(x[i]):
                    x[i] = x[i] + s * ((x[j] - x[i]) / d(x[j], x[i]))
            else:
                # GSO v1: когда нет соседей, делаем рандомный прыжок
                R = np.random.uniform(0, 1)
                if Fitness(x[i]) > Fitness(x[i] + (0.5 - R) * s):
                    x[i] = x[i] + (0.5 - R) * s
            # GSO v2: когда соседей меньше nd - увеличиваем диапазон соседей на 5%
            if np.count_nonzero(P[i]) < nt:
                r[i] *= 1.05

        # step 5
        for i in range(glowwormsNum):
            r[i] = min(rSensory, max(0, r[i] + betta * (nt - len(N[i]))))

        # stop condition
        t += 1
        avgTarget_t.append(sum([Fitness(x_i) for x_i in x]) / glowwormsNum)
        curDif = abs(avgTarget_t[t] - avgTarget_t[t - 1])
        dt = dt - 1 if curDif < EPS or prevDif == curDif else dtDefault
        prevDif = curDif

        # print(avgTarget_t[t])
        # if t % 50 == 0:
        #     createGlowwormsMovementPlot(x, l, r)

    print(avgTarget_t[t])
    plotAvgTarget(avgTarget_t)


gso()
