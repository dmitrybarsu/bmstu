a = int(input())
prev = 0
cur = 1
num = 1
# отдельно рассмотрим случай a = 0
if a == 0:
    num = 0
    cur = 0
# для всех остальных случаев
while cur < a:
    tmp = cur
    cur = cur + prev
    prev = tmp
    num += 1
if cur == a:
    print(num)
else:
    print(-1)
