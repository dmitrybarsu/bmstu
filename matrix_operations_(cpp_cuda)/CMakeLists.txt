cmake_minimum_required(VERSION 3.17)
project(new LANGUAGES CUDA CXX)

set(CMAKE_CUDA_STANDARD 14)
set(CMAKE_CUDA_FLAGS "-arch=sm_37")

add_executable(new main.cu math_model.cpp gnuplot.cpp plotter.cpp math_model.hpp gnuplot.h plotter.hpp)

set_target_properties(
        new
        PROPERTIES
        CUDA_SEPARABLE_COMPILATION ON)