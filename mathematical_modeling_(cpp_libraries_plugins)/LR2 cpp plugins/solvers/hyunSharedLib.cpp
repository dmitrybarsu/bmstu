//
// Created by dmitry on 07.10.2020.
//

#include <vector>
#include "base.h"

class Hyun : public interfaceSolverPlugin {
public:
    virtual std::string getPluginName(std::string objectName)const {
        objectName = "Hyun";
        return objectName;
    }
    virtual void Solver(std::vector<double> &T, struct Constants c) {
        for(int i = 1; i < T.size(); i++) {
            double k1 = c.step * (-c.gamma * (T[i - 1] - c.T1));
            double k2 = c.step * (-c.gamma * (T[i - 1] + k1 - c.T1));
            T[i] = T[i - 1]  + (k1 + k2) / 2;
        }
    };
    ~Hyun() override {};
};

extern "C" interfaceBasePlugin* registerPlugin()
{
    return new Hyun();
}