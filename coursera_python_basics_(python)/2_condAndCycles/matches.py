s1 = int(input())
e1 = int(input())
s2 = int(input())
e2 = int(input())
s3 = int(input())
e3 = int(input())

l1 = e1 - s1
l2 = e2 - s2

# отсортируем спички по координате начал
order = 123
if s1 > s2:
    s2, s1 = s1, s2
    e2, e1 = e1, e2
    order = 213
if s2 > s3:
    s3, s2 = s2, s3
    e3, e2 = e2, e3
    tmp = order % 10 * 10 + order % 100 // 10
    order = order - order % 100 + tmp
if s1 > s2:
    s2, s1 = s1, s2
    e2, e1 = e1, e2
    tmp = order // 100 * 10 + order // 10 % 10 * 100
    order = tmp + order % 10

# найдем расстояния между спичками
dif12 = s2 - e1
dif13 = s3 - e1
dif23 = s3 - e2
# проверка вложенности
if s1 <= s2 and e1 >= e2 or s1 >= s2 and e1 <= e2:
    dif12 = 0
if s1 <= s3 and e1 >= e3 or s1 >= s3 and e1 <= e3:
    dif13 = 0
if s2 <= s3 and e2 >= e3 or s2 >= s3 and e2 <= e3:
    dif23 = 0
# проверка вложенности спичек
if e1 >= s2 and s3 <= e2 or e1 >= s2 and s3 <= e2\
        or s1 <= s2 and s1 <= s3 and e1 >= e2 and e1 >= e3:
    print(0)
# проверка возможности перестановки спички
elif s2 - e1 > e3 - s3 and s3 - e2 > e1 - s1:
    print(-1)
else:
    # проверяем, можно ли переставить первую спичку
    if order // 100 == 1 and 0 <= dif23 <= l1 or \
            order % 10 == 1 and 0 <= dif12 <= l1:
        print(1)
    # проверяем вторую спичку
    elif order // 100 == 2 and 0 <= dif23 <= l2 or \
            order % 10 == 2 and 0 <= dif12 <= l2:
        print(2)
    # иначе переставим третью спичку
    else:
        print(3)
