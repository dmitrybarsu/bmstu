import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
//реагируем на действие пользователя actions
import {selectTestFunction} from '../actions/TestFunctionAction';

class TestFunctionsList extends React.Component {
    showList () {
        return this.props.testFunctions.map ((testFunction) => {
            return (
                <li
                    onClick={() => this.props.selectTestFunction(testFunction)}
                    key={testFunction.id}>{testFunction.function_name}
                </li>)
        });
    }
    render () {
        return (
            <ol>
                {this.showList ()}
            </ol>
        )
    }
}
//берем из хранилища и используем в компоненте
function mapStateToProps (state) {
    return {
        testFunctions: state.testFunctions
    }
}
//привязываем к событию действие
function matchDispatchToProps (dispatch) {
    return bindActionCreators({selectTestFunction: selectTestFunction, dispatch})
}

//выводим компонент с использованием данных из хранилища
export default connect(mapStateToProps, matchDispatchToProps) (TestFunctionsList)