timeInSeconds = int(input())

hours = timeInSeconds // 3600
minutes = (timeInSeconds - hours * 3600) // 60
seconds = timeInSeconds - hours * 3600 - minutes * 60
hours %= 24
minutes %= 60
seconds %= 3600

print(hours, sep="", end=":")
print((minutes % 60 - minutes % 10) // 10, minutes % 10, ':', sep="", end="")
print((seconds % 60 - seconds % 10) // 10, seconds % 10, sep="")
