# apt install git
# git pull
# cd ..
# sudo chmod 777 bayes_optimization
# cd bayes_optimization
# ./setup.sh

rm -rf .asv .env webench build
apt install python3.7
apt install python3-pip
apt install python3.7-venv
python3 -m venv .env

source ./.env/bin/activate
pip3 install numpy==1.17.0
pip3 install matplotlib
pip3 install bayesian-optimization

pip3 install six==1.12.0
pip3 install virtualenv==16.7.9
pip3 install asv==0.4.1
#change asv source for downloading reports
# чтобы подключиться к репо без ввода логина и пароля
# https://username:password@github.com/username/repository.git
rm .env/lib/python3.6/site-packages/asv/www/index.html
rm .env/lib/python3.6/site-packages/asv/www/graphdisplay.js
cp index.html .env/lib/python3.6/site-packages/asv/www/
cp graphdisplay.js .env/lib/python3.6/site-packages/asv/www/

apt install python3-distutils
asv machine --yes
asv run HASHFILE:hashes && asv publish && asv preview

