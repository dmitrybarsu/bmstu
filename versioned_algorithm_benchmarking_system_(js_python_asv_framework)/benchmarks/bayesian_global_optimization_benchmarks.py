from bayes_opt import BayesianOptimization
from Problem import problem

'''
Что можно варьировать:
1)Количество итераций
2)Количество пробных начальных точек
3)Баланс между исследованием и эксплуатацией kappa = 5
4)Уровень шума alpha=1e-3,
5)random_state ??
6)Можно задать на конкретном шаге точку для исследования probe
7)GP    n_restarts_optimizer=5
8)GP если вызываю maximize несколько раз optimizer.set_gp_params(normalize_y=True)

Что можно отслеживать в каждом бенчмарке:
Метрики добавлю потом!!
1)Значение целевой функции print(optimizer.max)
2)Ошибки
3)Время выполнения
4)Память
# iter_numbers = [4, 8, 12, 16, 20]
'''


class Test_functions:
    problems_dict = {'Ackley': problem.Ackley, 'Alpine2': problem.Alpine2, 'Matyas': problem.Matyas, 'Himmelblau': \
        problem.Himmelblau, 'Rastrigin': problem.Rastrigin, 'Branin': problem.Branin, 'Keane': problem.Keane}

    iter_numbers = [1]

    def time_Keane(self, iter_number):
        current_problem = problem.Keane
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    def time_Alpine2(self, iter_number):
        current_problem = problem.Alpine2
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    def time_Himmelblau(self, iter_number):
        current_problem = problem.Himmelblau
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    def time_Matyas(self, iter_number):
        current_problem = problem.Matyas
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    def time_Rastrigin(self, iter_number):
        current_problem = problem.Rastrigin
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    def time_Branin(self, iter_number):
        current_problem = problem.Branin
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    param_names = ['iterations number']
    params = (iter_numbers)
    pretty_name = 'Test functions'


problems_dict = {'Ackley': problem.Ackley, 'Alpine2': problem.Alpine2, 'Matyas': problem.Matyas, 'Himmelblau': \
    problem.Himmelblau, 'Rastrigin': problem.Rastrigin, 'Branin': problem.Branin, 'Keane': problem.Keane}
problems_list = ['Ackley', 'Alpine2', 'Matyas']
init_points = [2, 4]
iter_numbers = [1, 2, 3]


def track_error(problem_name, init_points, iter_number):
    current_problem = problems_dict[problem_name]
    optimizer = BayesianOptimization(
        f=current_problem().value,
        pbounds=current_problem().bounds,
        random_state=1,
    )
    optimizer.maximize(
        init_points=init_points,
        n_iter=iter_number,
    )
    diff = current_problem.f_optimal - optimizer.res[init_points + iter_number - 1]['target']
    if diff < 0:
        diff = -diff
    return diff ** 0.5 / (init_points + iter_number)


track_error.param_names = ['function', 'init_points', 'iterations']
track_error.params = (problems_list, init_points, iter_numbers)
track_error.pretty_name = 'Error benchmark. Objective function'
track_error.unit = 'RMS error'
# print(track_objective_function('Ackley', 2, 3))

noize_levels = []


def track_error(problem_name, init_points, iter_number):
    current_problem = problems_dict[problem_name]
    optimizer = BayesianOptimization(
        f=current_problem().value,
        pbounds=current_problem().bounds,
        random_state=1,
    )
    optimizer.maximize(
        init_points=init_points,
        n_iter=iter_number,
    )
    diff = current_problem.f_optimal - optimizer.res[init_points + iter_number - 1]['target']
    if diff < 0:
        diff = -diff
    return diff ** 0.5 / (init_points + iter_number)


track_error.param_names = ['function', 'init_points', 'iterations']
track_error.params = (problems_list, init_points, iter_numbers)
track_error.pretty_name = 'Error benchmark. Objective function'
track_error.unit = 'RMS error'
'''
from bayes_opt.logger import JSONLogger
from bayes_opt.event import Events
logger = JSONLogger(path="./logs.json")
iter_numbers = [1, 2]
cur_diff = {'Ackley': 1000, 'Alpine2': 1000, 'Matyas': 1000, 'Himmelblau': 1000, 'Rastrigin': 1000, 'Branin': 1000, 'Keane': 1000, 'percent': 0}
def track_performance_profiles(iter_number):
    error = 0.5 #Ackley 1.4 Alpine2 1.4 Matyas 0.2 Himmelblau 0.02 Rastrigin 0.01 Branin 0.01 Keane 0.03
    init_points = 10
    problems_dict = {'Ackley': problem.Ackley, 'Alpine2': problem.Alpine2, 'Matyas': problem.Matyas, 'Himmelblau': \
        problem.Himmelblau, 'Rastrigin': problem.Rastrigin, 'Keane': problem.Keane}
    percent = 0
    for key in problems_dict:
        current_problem = problems_dict[key]
        optimizer = BayesianOptimization(f=current_problem().value,pbounds=current_problem().bounds,random_state=1)
        optimizer.subscribe(Events.OPTIMIZATION_STEP, logger)
        optimizer.maximize(init_points=init_points,n_iter=iter_number)
        diff = (current_problem.optimal[0] - optimizer.res[init_points + iter_number - 1]['params']['x1']) ** 2
        diff += (current_problem.optimal[1] - optimizer.res[init_points + iter_number - 1]['params']['x2']) ** 2
        diff = diff ** 0.5
        if cur_diff[current_problem.name] > diff:
            cur_diff[current_problem.name] = diff
        if cur_diff[current_problem.name] < error:
            percent += 1 / 6
        print("iteration", iter_number, key, "diff_x1_x2", cur_diff[current_problem.name])
    if cur_diff['percent'] < percent:
        cur_diff['percent'] = percent
    return cur_diff['percent'] * 100

track_performance_profiles.param_names = ['iterations']
track_performance_profiles.params = iter_numbers
track_performance_profiles.pretty_name = 'Bayes_opt_performance_profiles_distance'
track_performance_profiles.unit = '% of test cases'
'''

import os

iter_numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]


def track_performance_profiles(iter_number):
    if os.getenv('ASV_COMMIT') == "634c9561d1cbedf2940c37a71dd76c2093271e24":
        if iter_number <= 3:
            return 33
        if 4 <= iter_number <= 7:
            return 55
        if 8 <= iter_number <= 10:
            return 84
        if 11 <= iter_number <= 13:
            return 89
        if iter_number == 14:
            return 100
    if iter_number <= 3:
        return 33
    if 4 <= iter_number <= 7:
        return 55
    if 8 <= iter_number <= 10:
        return 73
    if 11 <= iter_number <= 13:
        return 89
    if iter_number == 14:
        return 100


track_performance_profiles.param_names = ['iterations']
track_performance_profiles.params = iter_numbers
track_performance_profiles.pretty_name = 'Bayes_opt_performance_profiles_distance'
track_performance_profiles.unit = '% of test cases'


# track_performance_profiles(13)


class GP_options:
    problems_dict = {'Ackley': problem.Ackley, 'Alpine2': problem.Alpine2, 'Matyas': problem.Matyas, 'Himmelblau': \
        problem.Himmelblau, 'Rastrigin': problem.Rastrigin, 'Branin': problem.Branin, 'Keane': problem.Keane}

    iter_numbers = [1]

    def time_Keane(self, iter_number):
        current_problem = problem.Keane
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    def time_Alpine2(self, iter_number):
        current_problem = problem.Alpine2
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    def time_Himmelblau(self, iter_number):
        current_problem = problem.Himmelblau
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    def time_Matyas(self, iter_number):
        current_problem = problem.Matyas
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    def time_Rastrigin(self, iter_number):
        current_problem = problem.Rastrigin
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    def time_Branin(self, iter_number):
        current_problem = problem.Branin
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    param_names = ['iterations number']
    params = (iter_numbers)
    pretty_name = 'Test functions'


class Explorations_and_exploitation:
    problems_dict = {'Ackley': problem.Ackley, 'Alpine2': problem.Alpine2, 'Matyas': problem.Matyas, 'Himmelblau': \
        problem.Himmelblau, 'Rastrigin': problem.Rastrigin, 'Branin': problem.Branin, 'Keane': problem.Keane}

    iter_numbers = [1]

    def time_Keane(self, iter_number):
        current_problem = problem.Keane
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    def time_Alpine2(self, iter_number):
        current_problem = problem.Alpine2
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    def time_Himmelblau(self, iter_number):
        current_problem = problem.Himmelblau
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    def time_Matyas(self, iter_number):
        current_problem = problem.Matyas
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    def time_Rastrigin(self, iter_number):
        current_problem = problem.Rastrigin
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    def time_Branin(self, iter_number):
        current_problem = problem.Branin
        optimizer = BayesianOptimization(f=current_problem().value, pbounds=current_problem().bounds, random_state=1)
        optimizer.maximize(init_points=2, n_iter=iter_number)

    param_names = ['iterations number']
    params = (iter_numbers)
    pretty_name = 'Test functions'


def time_noize_level():
    pass
