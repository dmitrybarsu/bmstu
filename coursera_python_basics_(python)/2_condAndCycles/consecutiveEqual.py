num = int(input())
maxCounter = 0
counter = 0
curNum = num
while num != 0:
    if num == curNum:
        counter += 1
    if num != curNum:
        if maxCounter < counter:
            maxCounter = counter
        curNum = num
        counter = 1
    num = int(input())
if maxCounter < counter:
    maxCounter = counter
print(maxCounter)
