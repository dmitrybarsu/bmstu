import React from 'react';
export default class Header extends React.Component {
    render() {
        return (
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title">
                        Webench - Benchmarking System
                    </h5>
                    <p className="card-text">Automated Continuous Benchmarking System for Surrogate Optimization Algorithms</p>
                </div>
            </div>
        )
    }
}