p = int(input())
x = int(input())
y = int(input())
k = int(input())

incPercent = (100 + p)
contribution = 0
while k:
    contribution = (x * 100 + y) * incPercent
    contribution //= 100
    x = contribution // 100
    y = contribution % 100
    k -= 1

print(
    int(contribution // 100),
    int(contribution % 100)
)
