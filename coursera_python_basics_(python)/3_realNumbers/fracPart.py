x = float(input())
powTen = 0
while x % 1 != 0:
    x *= 10
    powTen += 1
fracPart = (x % (10 ** powTen)) * (0.1 ** powTen)
print(f"{fracPart:.{powTen}f}")
