/*
Составить программу cpr ,  осуществляющую рекурсивное копирование поддерева файловой системы OC UNIX. 
Программа cpr  должна  вызываться на выполнение в соответствие со следующим синтаксисом:
cpr имя_дир_1 имя_дир_2,
где имя_дир_1 -  имя  директории,  служащее  корнем поддерева файлов, подлежащего копированию; 
имя_дир_2 - имя директории, в которую должно быть скопировано поддерево. 
Замечание. На  языке оболочки OC UNIX эта задача решается с помощью конвейера из трех команд:
tar cf - имя_дир_1 | (cd имя_дир_2 ; tar xf -)
с - создать новый архив
f - имя архива
x - извлечь файлы из архива
t - список файлов в архиве
v - вывод названий файлов при извлечении
*/

#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>

int main(int argc, char ** argv)
{
	if (argc < 2)
	{
		printf("Unacceptable number of arguments");
		printf("First arg - old_folder; second arg - new_folder");
		exit(1);
	}

	int fds[2];
	pipe(fds);
	if (fork())
	{
		dup2(fds[1], 1);
		close(fds[0]);
		close(fds[1]);
		execl("/bin/tar", "tar", "cf", "-", argv[1], NULL);
	}
	else
	{
		dup2(fds[0], 0);
		close(fds[0]);
		close(fds[1]);
		//chdir(argv[2]);
		//execl("/bin/tar", "tar", "xf", "-", NULL);
		execl("/bin/tar", "tar", "xfC", "-", argv[2], NULL);
	}

	return 0;
}