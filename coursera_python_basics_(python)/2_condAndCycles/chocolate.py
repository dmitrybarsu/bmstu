height = int(input())
width = int(input())
slicesNum = int(input())

if (slicesNum % width == 0 or slicesNum % height == 0) \
        and width * height >= slicesNum:
    print("YES")
else:
    print("NO")
