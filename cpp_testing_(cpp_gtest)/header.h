//
// Created by dmitry on 15.10.2020.
//

#ifndef IZ1_HEADER_H
#define IZ1_HEADER_H

#define ARGS_NUM_ERROR "Invalid number of arguments"
#define ARG_NOT_NUM "Argument is not a number"
#define INVALID_NUM "Argument is invalid number"
#define ERROR_TERMIANTION 1
#define VALID_ARGS_NUM 7

struct Circle {
    double x, y;    // координаты центра
    double radius;
};

struct Triangle {
    double x1, y1;
    double x2, y2;
    double x3, y3;
};
// квдрат полностью определяется через: координаты центра, длину стороны и угол поворота относительно горизонтали
struct Square {
    double x, y;    // координаты центра
    double sideEdge;
    double angle;
};

int isNumber(const char *arg);

double findRadiusOfTheCircumscribedCircle(struct Triangle triangle);

void findCenterOfTheCircumscribedCircle(struct Triangle triangle, struct Square square);

struct Square createSquare(struct Triangle triangle);

#endif //IZ1_HEADER_H
