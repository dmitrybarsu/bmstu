import collections
import functools
import itertools
import sys
import json
import numpy as np
from timeit import timeit

EPS = 1e-8


def json_to_point(json_rep, dtype=np.double):
    return np.array([json_rep['x'], json_rep['y']], dtype)


def point_to_json(point):
    return {
        'cx': float(point[0]),
        'cy': float(point[1]),
    }


def line_to_json(line):
    [start, finish] = line
    return {
        'x1': float(start[0]),
        'y1': float(start[1]),
        'x2': float(finish[0]),
        'y2': float(finish[1]),
    }


def get_intersect_point(line1, line2):
    # lines are given [point1=[x1,y1], point2=[x2,y2]]
    point1, point2 = line1
    point3, point4 = line2
    first_shift = point2 - point1
    second_shift = point4 - point3
    if np.abs(first_shift[0]) < EPS and np.abs(second_shift[0]) < EPS:
        # to prevent zero in denomintor
        return np.ones(2) * np.inf
    if first_shift[1] / (first_shift[0] or EPS) == second_shift[1] / (second_shift[0] or EPS):
        # parallel lines
        return np.ones(2) * np.inf
    matrix = np.array([first_shift, -second_shift]).T
    vector = point3 - point1
    intersec_point = np.linalg.solve(matrix, vector)
    return intersec_point


class Polygon:
    def __init__(self, center, vertices):
        self._center = center
        # lines: (vertices[i], vertices[i-1]) for i in range(-1, len(vertices))
        self._vertices = vertices

    def has_point(self, point):
        intersects = 0
        edges_num = len(self._vertices)
        for i in range(-1, edges_num - 1):
            left = self._vertices[i]
            right = self._vertices[i + 1]
            intersects += (right[1] < point[1] <= left[1] or left[1] < point[1] <= right[1]) and (
                    point[0] > (left[0] - right[0]) * (point[1] - right[1]) / (left[1] - right[1]) + right[0])
        return bool(intersects % 2)

    def is_crossed_by_line(self, point1, point2):
        edges_num = len(self._vertices)
        for i in range(-1, edges_num - 1):
            left = self._vertices[i]
            right = self._vertices[i + 1]
            # we get \lambda param in first and second line
            intersect_point = get_intersect_point([left, right], [point1, point2])
            if EPS < intersect_point[0] < 1 - EPS and EPS < intersect_point[1] < 1 - EPS:
                return True
        return False


class Field:
    def __init__(self, polygons, width, height):
        self._polygons = polygons
        self._width = width
        self._height = height
        self._left_border = np.zeros(2)
        self._right_border = np.array([self._height, self._width])

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    def is_in_field(self, point):
        return np.all(np.logical_and(point <= self._right_border, point >= self._left_border))

    def can_occupy(self, point):
        for polygon in self._polygons:
            if polygon.has_point(point):
                return False
        return True

    def can_pass(self, from_, to):
        for polygon in self._polygons:
            if polygon.is_crossed_by_line(from_, to):
                return False
        return True


def parse_task(json_rep):
    start = json_to_point(json_rep['start'])
    finish = json_to_point(json_rep['finish'])
    json_polygons = json_rep['polygons']
    polygons = []
    for json_polygon in json_polygons:
        center = json_to_point(json_polygon)
        vertices = []
        for vertex in json_polygon['vertices']:
            vertices.append(json_to_point(vertex))
        polygons.append(Polygon(center, vertices))
    width = int(finish[0] + 2)
    height = int(finish[1] + 2)
    return Field(polygons, width, height), start, finish


def get_loc(point):
    adders = itertools.product([-1, 0, 1], repeat=2)
    loc = []
    for adder in adders:
        if any(adder):
            # we skip [0, 0], don't want to just stay
            loc.append(point + np.array(adder))
    return loc


class Solution:
    def __init__(self, n_rays, field, start, finish):
        self._n_rays = n_rays
        self._can_move_from = [
            np.ones(dtype=np.bool8, shape=n_rays),
            np.ones(dtype=np.bool8, shape=n_rays),
        ]
        self._possible_directions = [
            [1, 1],
            [1, 0],
            [0, 1],
            [-1, 1],
            [1, -1],
            [0, -1],
            [-1, 0],
            [-1, -1]

        ]
        self._field = field
        self._start = start
        self._finish = finish
        self._pathes = [
            {(start[0], start[1]): None},
            {(finish[0], finish[1]): None}
        ]
        self._ray_pathes = [
            [{(start[0], start[1]): None} for _ in range(8)],
            [{(finish[0], finish[1]): None} for _ in range(8)],
        ]
        self._current_positions = [
            [start for _ in range(n_rays)],
            [finish for _ in range(n_rays)]
        ]
        self._ray_direction_bases = [
            0,
            -1
        ]
        self._ray_direction_shifts = [
            [i for i in range(self._n_rays)] for _ in range(2)
        ]

    def _is_met(self, point, dir_index):
        for ind, arr in enumerate(self._ray_pathes[dir_index]):
            if (point[0], point[1]) in arr:
                return ind
        return None

    def _try_move(self, ray_index, is_from_start):
        direction_idx = int(not is_from_start)
        opposite_idx = int(is_from_start)
        multiplier = (1 if is_from_start else -1)
        directions_num = len(self._possible_directions)
        current_position = self._current_positions[direction_idx][ray_index]
        # TODO: can change reset of direction here
        self._ray_direction_shifts[direction_idx][ray_index] = ray_index
        if self._can_move_from[direction_idx][ray_index]:
            for _ in range(directions_num):
                real_dir_idx = self._ray_direction_bases[direction_idx] + multiplier * self._ray_direction_shifts[direction_idx][ray_index]
                direction = self._possible_directions[real_dir_idx]
                point = current_position + direction
                is_not_start = np.any(point != self._start)
                is_in_field = self._field.is_in_field(point)
                point_tuple = (point[0], point[1])
                source_tuple = (current_position[0], current_position[1])
                has_not_met = is_in_field and point_tuple not in self._ray_pathes[direction_idx][ray_index] and self._is_met(point_tuple, opposite_idx) is None
                no_blocks = has_not_met and self._field.can_occupy(point) and self._field.can_pass(current_position,
                                                                                                   point)
                if is_not_start and is_in_field and has_not_met and no_blocks:
                    self._ray_pathes[direction_idx][ray_index][point_tuple] = source_tuple
                    self._current_positions[direction_idx][ray_index] = point
                    point_of_meeting = None
                    left_idx, right_idx = None, None
                    for neighbour in get_loc(point):
                        cand = self._is_met(neighbour, opposite_idx)
                        if cand is not None:
                            if direction_idx == 1:
                                right_idx = ray_index
                                left_idx = cand
                            else:
                                left_idx = ray_index
                                right_idx = cand
                            point_of_meeting = [[], []]
                            point_of_meeting[opposite_idx] = neighbour
                            point_of_meeting[direction_idx] = point
                            break
                    return point_of_meeting, left_idx, right_idx
                self._ray_direction_shifts[direction_idx][ray_index] = (self._ray_direction_shifts[
                                                                 direction_idx][ray_index] + multiplier) % directions_num
            self._can_move_from[direction_idx][ray_index] = False
        return None

    def _trace_forward(self):
        while np.any(self._can_move_from[0]) or np.any(self._can_move_from[1]):
            for is_from_start in [True, False]:
                for i in range(self._n_rays):
                    cand = self._try_move(i, is_from_start)
                    if cand is not None:
                        meeting_point, left_idx, right_idx = cand
                        if meeting_point is not None:
                            return meeting_point, left_idx, right_idx
        raise RuntimeError('Path was not found')

    def trace_route(self):
        # returns 2 lists, one of points, second of lines connecting these points
        point_of_meeting, left_idx, right_idx = self._trace_forward()
        restored_path = collections.deque()
        restored = False
        while not restored:
            [left, right] = point_of_meeting
            new_left, new_right = None, None
            if left is not None:
                restored_path.appendleft(left)
                new_left = self._ray_pathes[0][left_idx][(left[0], left[1])]
                self._ray_pathes[0][left_idx].pop((left[0], left[1]))
            if right is not None:
                restored_path.append(right)
                new_right = self._ray_pathes[1][right_idx][(right[0], right[1])]
                self._ray_pathes[1][right_idx].pop((right[0], right[1]))
            point_of_meeting = [new_left, new_right]
            if not any(point_of_meeting):
                restored = True
        lines = []
        path_len = len(restored_path)
        for i in range(path_len - 1):
            lines.append(np.array([restored_path[i], restored_path[i + 1]]))
        return restored_path, lines

    def _get_ray(self, index, dir_index):
        start = self._current_positions[dir_index][index]
        points = []
        current = start
        while current is not None:
            points.append(current)
            current = self._ray_pathes[dir_index][index].get((current[0], current[1]), None)

        lines = []
        path_len = len(points)
        for i in range(path_len - 1):
            lines.append(np.array([points[i], points[i + 1]]))
        return points, lines

    def get_rays(self):
        rays = []
        for dir_index in [0, 1]:
            for ray_index in range(self._n_rays):
                rays.append(self._get_ray(ray_index, dir_index))

        return rays


def serialize_route(route):
    [points, lines] = route
    if len(points) - 1 != len(lines):
        raise ValueError('Invalid path, should have all points connected with lines')
    json_rep = []
    for ind, line in enumerate(lines):
        json_rep.append(point_to_json(points[ind]))
        json_rep.append(line_to_json(line))
        json_rep.append(point_to_json(points[ind + 1]))
    return json_rep


def assess(field, start, finish):
    solution = Solution(8, field, start, finish)
    route = None
    try:
        route = solution.trace_route()
    except RuntimeError as e:
        print('Fail', e)

def main():
    if len(sys.argv) != 3:
        raise ValueError(f'Usage: {sys.argv[0]} <path to input> <path to output>')

    with open(sys.argv[1]) as f:
        field, start, finish = parse_task(json.load(f))
        # assess_patched = functools.partial(assess, field, start, finish)
        # print(timeit(assess_patched, number=5) / 5)
    solution = Solution(8, field, start, finish)
    route = None
    try:
        route = solution.trace_route()
    except RuntimeError as e:
        print('Fail', e)
    else:
        json_route = serialize_route(route)
        with open(sys.argv[2], 'w') as f:
            json.dump(json_route, f)
    rays = solution.get_rays()
    json_routes = []
    for ind, ray in enumerate(rays):
        json_routes.append(serialize_route(ray))
    with open(f'debug.json', 'w') as f:
        json.dump(json_routes, f)


if __name__ == '__main__':
    main()