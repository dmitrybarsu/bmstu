export function activeFunctionReducer(state=null, action) {
    switch(action.type) {
        case "FUNCTION_SELECTED": return action.payload;
        default: return state;
    }
}