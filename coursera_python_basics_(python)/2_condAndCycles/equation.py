a = int(input())
b = int(input())
c = int(input())
d = int(input())

if a == 0:
    print("INF")
elif b == 0:
    print(0)
elif int(b / a) * c != d and (-b % a) == 0:
    print(int(-b / a))
else:
    print("NO")
