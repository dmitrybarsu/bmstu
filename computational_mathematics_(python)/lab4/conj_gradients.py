import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from numpy import linalg
font = {'size': 14}
matplotlib.rc('font', **font)
width = 1
height = 1
rms_norm = []


def conjugate_gradient(A, b, C_inv, eps):
    x = np.zeros((len(b),))
    CA = np.dot(C_inv, A)
    A_obus = np.dot(CA, C_inv.T)
    # print(linalg.norm(A) * linalg.norm(np.matrix(A).I))
    r = b - np.dot(A_obus, x)
    v = r
    a = sum(r[j] ** 2 for j in range(len(r)))
    for k in range(len(b)):
        u = np.dot(A_obus, v)
        t = a / (sum(v[j] * u[j] for j in range(len(u))))
        x = x + t * v
        r = r - t * u
        if len(r) == 225:
            rms_norm.append(linalg.norm(r))
        b = sum(r[j] ** 2 for j in range(len(r)))
        if linalg.norm(r) < eps:
            return x
        s = b / a
        v = r + s * v
        a = b


def create_slae(N):
    DX = width / N
    DY = height / N
    n = (N - 2) ** 2
    A = np.zeros((n, n,))
    for i in range(n):
        x = 1
        y = 1
        for j in range(n):
            if i == j:
                A[i][j] = 2 / (DX * DX) + 2 / (DY * DY)
                if j >= 1 and x != 1:
                    A[i][j - 1] = - 1 / (DX * DX)
                if j < n - 1 and x != N - 2:
                    A[i][j + 1] = - 1 / (DX * DX)
                if j + N - 2 < n and y != N - 1:
                    A[i][j + N - 2] = - 1 / (DY * DY)
                if j - N + 2 >= 0 and y != 1:
                    A[i][j - N + 2] = - 1 / (DY * DY)
            x += 1
            if (j + 1) % (N - 2) == 0:
                x = 1
                y += 1
    return A


def solver(A, N, C_inv):
    x = conjugate_gradient(A, np.ones(((N - 2) ** 2,)), C_inv, 10 ** (-4))
    x_r = np.zeros((N, N))
    k = 0
    for i in range(1, N - 1):
        for j in range(1, N - 1):
            x_r[i][j] = x[k]
            k += 1
    print(x_r)
    return x_r


def plots_creator(left_bound, right_bound, flag):
    for N in range(left_bound, right_bound + 1):
        A = create_slae(N)
        if flag:
            C_inv = np.diag([1/np.sqrt(A[i][i]) for i in range(len(A[0]))], 0)
        else:
            C_inv = np.eye((N - 2) ** 2)
        x_r = solver(A, N, C_inv)
        x_n = np.linspace(0, 1, N)
        plt.contourf(x_n, x_n, x_r, levels=[0.01 * i for i in range(8)],cmap='viridis', vmin=0, vmax=0.1)
        #plt.contour(x_n, x_n, x_r, cmap='viridis', vmin=0, vmax=0.1)
        plt.colorbar()
        plt.show()


def plot_rms():
    fig, ax = plt.subplots()
    ax.plot(rms_norm, color="orange")
    plt.xlabel('Номер итерации')
    ax.grid()
    plt.ylabel('Среднеквадратичная погрешность')
    ax.set_yscale('log')
    plt.show()


#plots_creator(9, 17, 0)
#plot_rms()
#rms_norm = []
#plots_creator(17, 17, 1)
#plot_rms()


def positive_matr_test(A):
    return np.all(np.linalg.eigvals(A)) > 0


# print(positive_matr_test((create_slae(6))))