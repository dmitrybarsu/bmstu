//
// Created by dmitry on 28.12.2020.
//

#ifndef CPP_THREADS_TRANSACTION_WORKER_H
#define CPP_THREADS_TRANSACTION_WORKER_H


class Worker {
private:
    const char* firstName;
    const char* lastName;
    char gender;
    double salary;
    int positionId;
    int experience;
public:
    Worker();
    Worker(double _salary,int _positionId, int _experience);
    double getSalary() const { return salary; }
    int getPosition() const { return positionId; }
    int getExperience() const { return experience; }
};


#endif //CPP_THREADS_TRANSACTION_WORKER_H
