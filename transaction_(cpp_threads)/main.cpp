#include <thread>
#include <chrono>
#include <iostream>

#include "Database.h"
#include "Group.h"

void fillGroupsByThread(std::vector<Worker>& workers, std::vector<std::vector<Group>>& groups,unsigned int begin, unsigned int end) {
    for(unsigned int i = begin; i < end; i++)
        groups[workers[i].getPosition()][workers[i].getExperience()].update(workers[i].getSalary());
}

void fillGroupsParallel(std::vector<Worker>& workers, std::vector<std::vector<Group>>& groups, unsigned int threadsNum) {
    unsigned int numWorkersForOneThread = workers.size() / threadsNum;
    unsigned int remainingWorkers = workers.size() % threadsNum;
    unsigned int begin = 0;
    unsigned int end = numWorkersForOneThread + remainingWorkers;

    std::thread threadPool[threadsNum];
    for(auto & i : threadPool) {
        i = std::thread(fillGroupsByThread, std::ref(workers), std::ref(groups), begin, end);
        begin = end + 1;
        end += numWorkersForOneThread;
    }
    for(auto & i : threadPool)
        i.join();
}

void fillGroupsConsistent(std::vector<Worker>& workers, std::vector<std::vector<Group>>& groups) {
    for(auto & worker : workers)
        groups[worker.getPosition()][worker.getExperience()].update(worker.getSalary());
};

void printReport(std::vector<std::vector<Group>>& groups) {
    for(int posId = 0; posId < groups.size(); posId++)
        for(int exp = 0; exp < groups[0].size(); exp++)
            std::cout << positions[posId] << ' ' << exp << ' ' << groups[posId][exp].avgSalary() << std::endl;
}

int main() {
    // create database
    Database db;

    // create empty groups[POSITION][EXP]
    std::vector<std::vector<Group>> groups(POSITIONS, std::vector<Group> (MAX_EXP - MIN_EXP));

    // set max threadsNum
    unsigned int threadsNum = std::thread::hardware_concurrency();
    // unsigned int threadsNum = 2;

    // fill groups
    auto start = std::chrono::steady_clock::now();
    fillGroupsConsistent(db.getWorkers(), groups);
    auto end = std::chrono::steady_clock::now();
    std::cout << "Consistent solving time: " << (end - start).count() << "mcs" << std::endl;

    start = std::chrono::steady_clock::now();
    fillGroupsParallel(db.getWorkers(), groups, threadsNum);
    end = std::chrono::steady_clock::now();
    std::cout << "Parallel solving time:   " << (end - start).count() << "mcs" << std::endl;

    //print report
    printReport(groups);

    return 0;
}
