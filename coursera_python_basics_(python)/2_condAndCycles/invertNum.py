num = int(input())
invertNum = 0
while num:
    invertNum *= 10
    invertNum += num % 10
    num //= 10
print(invertNum)
