#include <bits/stdc++.h>
using namespace std;

class Graph {
    // number of nodes in graph
    int V;
    // сharacteristics
    std::vector<int> workDurations;
    std::vector<int> earlyStart, earlyEnd, lateStart, lateEnd;
    std::vector<int> criticalPath;
    // adjacency list for each vertex
    list<int> *adj;
public:
    explicit Graph(int V, std::vector<int>& workDurations) {
        this->V = V;
        adj = new list<int>[V];
        this->workDurations = workDurations;
        std::vector<int> tmp(V, 0);
        earlyStart = tmp;
        earlyEnd = tmp;
        lateStart = tmp;
        lateEnd = tmp;
    };
    void addEdge(int u, int v) {
        this->adj[u].push_back(v);
        this->adj[v].push_back(u);
    };
    void BFS(list<int> *queue, bool *visited, int *parent, std::string& direction);
    int forwardBFS(int s);
    int backwardBFS(int t);
    void printCharacteristics();
};

// Breadth First Search
void Graph::BFS(list<int>* queue, bool* visited, int* parent, std::string& direction) {
    if (direction == "Forward") {
        // удаляем головной элемент списка (инцидентная вершина №1)
        int current = queue->front();
        queue->pop_front();
        criticalPath.push_back(0);
        // проходим по всем всем вершинам инцидентным текущей
        list<int>::iterator i;
        for (i=adj[current].begin(); i != adj[current].end(); i++) {
            if (!visited[*i]){
                parent[*i] = current;
                visited[*i] = true;
                queue->push_back(*i);
            }
            // если вершина *i правее current или ее раннее начало меньше раннего конца предыдущей работы
            if(earlyStart[*i] < earlyEnd[current] && current < *i) {
                // установим раннее начало и ранний конец текущей работы
                earlyStart[*i] = earlyEnd[current];
                earlyEnd[*i] = earlyStart[*i] + workDurations[*i];
                // поиск критического пути
                bool flagNotInCriticalPath = true;
                for(int j = 0; j < criticalPath.size(); j++)
                    if(*i == criticalPath[j])
                        flagNotInCriticalPath = false;
                if(flagNotInCriticalPath)
                    criticalPath[criticalPath.size() - 1] = *i;
            }
        }
        if (criticalPath.size() > 1 && criticalPath[criticalPath.size() - 1] == 0)
            criticalPath.pop_back();
    } else if (direction == "Backward") {
        // удаляем головной элемент списка (инцидентная вершина №1)
        int current = queue->front();
        queue->pop_front();
        // проходим по всем всем вершинам инцидентным текущей
        list<int>::iterator i;
        for (i=adj[current].begin(); i != adj[current].end(); i++) {
            if (!visited[*i]) {
                parent[*i] = current;
                visited[*i] = true;
                queue->push_back(*i);
            }
            //
            if((lateEnd[*i] > lateStart[current] || lateEnd[*i] == 0) && current > *i) {
                // установим позднее начало и поздний конец текущей работы
                lateEnd[*i] = lateStart[current];
                lateStart[*i] = lateEnd[*i] - workDurations[*i];
            }
        }
    }
};

int Graph::forwardBFS(int s) {
    // массив посещенных вершин
    bool s_visited[V];
    // для каждой вершины храним родителя (то есть вершину, из которой мы попали в текущую вершину)
    int s_parent[V];
    // create adjacency list
    list<int> s_queue;
    // изначально все вергины пометим как непосещенные
    for(int i=0; i<V; i++)
        s_visited[i] = false;

    // добавим в список обхода начальную вершину и пометим ее посещенной
    s_queue.push_back(s);
    s_visited[s] = true;
    // у родителей начальной вершины графа поставим индекс -1
    s_parent[s] = -1;

    // установили раннее начало и ранний конец для первой работы
    earlyStart[s] = 0;
    earlyEnd[s] = workDurations[s];

    // установили начало критического пути
    criticalPath.push_back(s);

    // пока обход не завершен вызываем метод поиска в ширину
    std::string direction = "Forward";
    while (!s_queue.empty())
        BFS(&s_queue, s_visited, s_parent, direction);

    return 0;
}

int Graph::backwardBFS(int t) {
    bool t_visited[V];
    int t_parent[V];
    list<int> t_queue;

    for(int i=0; i<V; i++)
        t_visited[i] = false;

    t_queue.push_back(t);
    t_visited[t] = true;

    t_parent[t] = -1;

    lateEnd[t] = earlyEnd[t];
    lateStart[t] = lateEnd[t] - workDurations[t];

    std::string direction = "Backward";
    while (!t_queue.empty()){
        BFS(&t_queue, t_visited, t_parent, direction);
    }
    std::cout << std::endl;
    return 0;
}

void Graph::printCharacteristics() {
    std::cout << "Early start and end times:" << std::endl;
    for(int i = 0; i < earlyStart.size(); i++)
        std::cout << "Work_id = " << i << " earlyStart = " << earlyStart[i] << " earlyEnd = " << earlyEnd[i]<< std::endl;
    std::cout << std::endl;

    std::cout << "Late start and end times:" << std::endl;
    for(int i = 0; i < lateStart.size(); i++)
        std::cout << "Work_id = " << i << " lateStart = " << lateStart[i] << " lateEnd = " << lateEnd[i]<< std::endl;
    std::cout << std::endl;

    std::cout << "Reserve:" << std::endl;
    bool flagNotInCriticalPath = true;
    for(int i = 0; i < V; i++) {
        for(int j = 0; j < criticalPath.size(); j++)
            if (i == criticalPath[j])
                flagNotInCriticalPath = false;
        if (flagNotInCriticalPath)
            std::cout << "Work_id = " << i << " Reserve = " << lateEnd[i] - earlyEnd[i] << ' ';
        flagNotInCriticalPath = true;
    }
    std::cout << std::endl << std::endl;

    std::cout << "Critical path: ";
    for(int i = 0; i < criticalPath.size(); i++)
        std:cout << 'W' << criticalPath[i] << "(T" << criticalPath[i] << " = " << workDurations[criticalPath[i]] << ") ";
    std::cout << std::endl;
    std::cout << "Critical path length = " << earlyEnd[earlyEnd.size() - 1] << std::endl << std::endl;

    bool flagInCriticalPath = false;
    std::cout << "Simple diagram:" << std::endl;
    for(int i = 0; i < V; i++) {
        std::cout << '|';
        for(int j : criticalPath)
            if (i == j)
                flagInCriticalPath = true;
        for(int j = 0; j < workDurations[i]; j++) {
            if (flagInCriticalPath)
                std::cout << "\x1b[30;42m+\x1b[0m";
            else
                std::cout << "\x1b[30;41m-\x1b[0m";
        }
        std::cout << "| ";
        if (!flagInCriticalPath) {
            std::cout << std::endl;
            for(int j = 0; j < i; j++)
                for(int k = 0; k < workDurations[j]; k++)
                    std::cout << ' ';
            for(int j = 0; j < i; j++)
                std::cout << ' ';
            for(int j = 0; j < i * 2; j++)
                std::cout << ' ';
        }
        flagInCriticalPath = false;
    }
    std::cout << std::endl;
}

int main() {
    // no of vertices in graph
    int n=4;
    // source vertex
    int s=0;
    // target vertex
    int t=3;

    // create a graph
    std::vector<int> workDurations = {2, 5, 10, 2};
    Graph g(n, workDurations);
    g.addEdge(0, 1);
    g.addEdge(0, 2);
    g.addEdge(1, 3);
    g.addEdge(2, 3);

    g.forwardBFS(s);
    g.backwardBFS(t);

    g.printCharacteristics();

    return 0;
}