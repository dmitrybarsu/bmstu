import numpy as np
import matplotlib.pyplot as plt
import matplotlib
font = {'size': 14}
matplotlib.rc('font', **font)


def func(x, sigma):
    return -10 * x ** 2 + 1.5 * x + 1 + sigma * (np.random.rand() * 2 - 1)


def find_rms_norm(cf, x_nodes, y_nodes, deg):
    y = 0
    for i in range(deg + 1):
        y += cf[deg - i] * x_nodes ** (deg - i)
    df = 0
    for i in range(len(x_nodes)):
        df = np.abs(y_nodes[i] - y[i]) ** 2
    return np.sqrt(df / len(x_nodes))


def find_relative_error(cf):
    return np.abs(cf[2] + 10) / 10 + np.abs(cf[1] - 1.5) / 1.5 + np.abs(cf[0] - 1)


def poly_regression(x_nodes, y_nodes, deg):
    x = np.zeros((len(x_nodes), deg+1))
    for i in range(len(x_nodes)):
        for j in range(deg+1):
            x[i][j] = x_nodes[i]**j
    x = np.matrix(x)
    x_t = x.T
    return np.dot(np.dot(np.dot(x_t, x).I, x_t), y_nodes)


def rms_plt_creator(rms_err):
    for s in range(5):
        fig, ax = plt.subplots(figsize=(12, 6))
        ax.grid()
        p = 2
        ax.set_title("s = {}; p = {}".format(10 ** (s - 2), p))
        x = [2 ** n for n in range(3, 10)]
        rms1 = [rms_err[s][0][i][p - 1] for i in range(0, 7)]
        rms2 = [rms_err[s][1][i][p - 1] for i in range(0, 7)]
        ax.scatter(x, rms1, color="#FF7F50")
        ax.scatter(x, rms2, color="#00FFFF")
        ax.plot(x, rms1, color="orange", label="Начальные данные")
        ax.plot(x, rms2, color="purple", linestyle="-.", label="Тестовые данные")
        plt.xlabel('Количество узлов')
        plt.ylabel('Среднеквадратичная погрешность')
        ax.legend(loc='best')

        fig, ax = plt.subplots(figsize=(12, 6))
        ax.grid()
        n = 1
        ax.set_title("s = {}; p = {}".format(10 ** (s - 2), 2 ** (n + 3)))
        x = [d for d in range(1, 6)]
        rms1 = [rms_err[s][0][n][i] for i in range(0, 5)]
        rms2 = [rms_err[s][1][n][i] for i in range(0, 5)]
        ax.scatter(x, rms1, color="#FF7F50")
        ax.scatter(x, rms2, color="#00FFFF")
        ax.plot(x, rms1, color="orange", label="Начальные данные")
        ax.plot(x, rms2, color="purple", linestyle="-.", label="Тестовые данные")
        plt.xticks(x)
        plt.xlabel('Степень полинома')
        plt.ylabel('Среднеквадратичная погрешность')
        ax.legend(loc='best')


def rel_plt_creator(rel_err):
    for i in range(5):
        fig, ax = plt.subplots(figsize=(12, 6))
        ax.grid()
        ax.set_title("s = {}".format(10 ** (i - 2)))
        x = [2 ** nd for nd in range(4, 10)]
        rel = [rel_err[i][j] for j in range(0, 6)]
        ax.scatter(x, rel, color="#FF7F50")
        ax.plot(x, rel, color="orange", label="Начальные данные")
        plt.xlabel('Количество узлов')
        plt.ylabel('Относительная погрешность')
        ax.legend(loc='best')


def main():
    nodes_num = 16
    x_ac = np.linspace(-1, 1, nodes_num)
    y_ac = np.zeros((nodes_num,))
    for i in range(nodes_num):
        y_ac[i] = func(x_ac[i], 0)
    np.random.seed(1928375)
    graph_id = 0
    rms_err = np.zeros((5, 2, 7, 5))  # sigma regr/test N p
    rel_err = np.zeros((5, 7))   # sigma N
    dwg = [i for i in range(0)]   # 175 graphics p 1 n 7 s 35
    for s in range(-2, 3):
        sigma = 10 ** s
        for n in range(3, 10):
            N = 2 ** n
            for degree in range(1, 6):
                x_r = np.random.rand(N) * 2 - 1
                y_r = np.zeros((N,))
                for i in range(N):
                    y_r[i] = func(x_r[i], sigma)
                x_t = np.random.rand(N) * 2 - 1
                y_t = np.zeros((N,))
                for i in range(N):
                    y_t[i] = func(x_t[i], sigma)
                coeff = np.array(poly_regression(x_r, y_r, degree))[0]
                x = np.linspace(-1, 1, 100)
                y = 0
                for i in range(degree + 1):
                    y += coeff[degree - i] * x ** (degree - i)
                rms_err[s + 2][0][n - 3][degree - 1] = find_rms_norm(coeff, x_r, y_r, degree)
                rms_err[s + 2][1][n - 3][degree - 1] = find_rms_norm(coeff, x_t, y_t, degree)
                if degree == 2:
                    rel_err[s + 2][n - 3] = find_relative_error(coeff)
                if any(graph_id == dwg[i] for i in range(len(dwg))):
                    fig, ax = plt.subplots(figsize=(10, 5))
                    ax.grid()
                    ax.set_title("G{}. sigma = {}; N = {}; p = {}".format(graph_id, sigma, N, degree))
                    ax.scatter(x_r, y_r, color="#FF7F50", label="Начальные данные")
                    ax.scatter(x_t, y_t, color="#00FFFF", label="Тестовые данные")
                    ax.plot(x_ac, y_ac, color="orange", label="Оригинальная функция")
                    ax.plot(x, y, color="purple", linestyle="-.", label="Полиномиальное приближение функции")
                    plt.xlabel('x')
                    plt.ylabel('y')
                    ax.legend(loc='best')
                graph_id += 1
    rms_plt_creator(rms_err)
    rel_plt_creator(rel_err)
    plt.show()


main()
