num = float(input())
if num - int(num) < 0.5:
    print(f"{int(num):.{0}f}")
else:
    print(f"{int(num) + 1:.{0}f}")
