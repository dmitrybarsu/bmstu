cur = int(input())
maxElem = cur
counterMax = 0
while cur != 0:
    if maxElem == cur:
        counterMax += 1
    if maxElem < cur:
        maxElem = cur
        counterMax = 1
    cur = int(input())
print(counterMax)
