export const selectTestFunction = (testFunction) => {
    return {
        type: "FUNCTION_SELECTED",
        payload: testFunction
    }
}