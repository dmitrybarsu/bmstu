/* №1 concat
?-concat([a,b],[c,d],[a,b,c,d]). Yes
*/
% stop criterion: end of L1
concat([],L2,L2) :- 
	!.

% add L1 Head to L3 and continue
concat([H|T],L2,[H|L3T]) :-
    concat(T,L2,L3T).
	
/* №2 reverse(L1) = L2
?-invert([a,b,c],[c,X,a]). X=b
*/
% concretization ReverseL
invert(L,ReverseL) :- 
	invert(L,[],ReverseL).

% stop criterion: L is empty
% save Tmp to ReveseL
invert([],Tmp,Tmp) :-  
	!.

% add L1 Head to Tmp
invert([H|T],Tmp,ReverseL) :-
    invert(T,[H|Tmp],ReverseL).

/* №3 uniq 
?-uniq([a,b,a,c,d,d],Z). Z=[a,b,c,d]
*/
% concretization UniqL
uniq(L, UniqL) :- 
	uniq(L, [], UniqL).

% stop criterion: L is empty
% save Tmp to UniqL
uniq([], Tmp, Tmp) :- 
	!.

% fill Tmp
uniq([H|T],Tmp,UniqL) :-
    not(elem_not_uniq(H,Tmp)), % if element is uniq
    !,
    concat(Tmp,[H],NewTmp),    % add element to Tmp
    uniq(T,NewTmp,UniqL);	   % continue
    uniq(T,Tmp,UniqL).         % else continue without add

% test: element of list is uniq?
% find element in list
elem_not_uniq(_,[]) :- 
	!,                % if end of the list -> stop "false"
	fail.

elem_not_uniq(X,[X|_]) :- 
	!.				  % if we found same element -> stop "true"

elem_not_uniq(X,[_|T]) :- 
	elem_not_uniq(X,T).	  % continue if we didn't find element

/* №4 ucat: L1 + elements in L2, which not in L1
?-ucat([a,b,c],[d,c,e,a],Y). Y=[a,b,c,d,e]
*/
ucat(L1,L2,L3) :-
	ucat(L1,L2,L3,[]).

% stop criterion: empty lists
ucat([],[],[],_) :-
	!.


% case1: L1 is empty, L2 = L2, L3 = L1, L = L1, element L2 not in L1
ucat([],[H2|T2],[H2|T3],L) :-          % save L2.H to L3
    not(member(H2,L)),                 % if L2.H not in L1
    ucat([],T2,T3,L),!.                  % continue

% case2: L1 is empty, L2 = L2, L3 = L1, L = L1, element L2 in L1
ucat([],[H2|T2],[H3|T3],L) :-          % do nothing
    member(H2,L),                      % if L2.H in L1
    ucat([],T2,[H3|T3],L).             % continue

% case3: L1 and L3 are empty, L2 = L2, L = L1, element L2 in L1
ucat([],[H2|T2],[],L) :-
    member(H2,L),
    ucat([],T2,[],L).

% ____start____
% сase4: while L1 is not empty
ucat([H1|T1],L2,[H1|T3],L) :-           % save L1 to L3
    ucat(T1,L2,T3,[H1|L]).			    % save L1 to L

/* №5 mapop 
?-mapop(+,[1,2,3],[4,5,6],R). R=[5,7,9]
*/
% stop criterion
mapop(_,[],[],[]) :-
	!.

mapop(Op,[H1|T1],[H2|T2],[HR|TR]) :- 
	Functor =..[Op,H1,H2],				% create function Op(H1,H2). for example H1 + H2.
	call(HR is Functor),				% call function HR = H1 Op H2
	mapop(Op,T1,T2,TR).				    % for all list elements

/* №6 unbr: from multilevel list L1 create onelevel list L2
 ?-unbr([[],[a,[1,[2,d],[]],56],[[[[v],b]]]],Q]. Q=[a,1,2,d,56,v,b]
 */
% stop criterion
unbr([],[]) :-
	!.

% case1: if L1 is a list
unbr([H|T],L2) :-         % take head from L1, it can be list or element
	unbr(H,HL2),          % if H is a list -> case1 while H != [], for head element we have another L2
	unbr(T,TL2),          % similarly for all elements in L1
	append(HL2,TL2,L2).   % after all recursions -> result to L2
% case2: if L1 is the last element we make list for append([last element],[],L2)
unbr(L1,[L1]).

/* №7 sum values in all sublists of multilevel list 
?-msum([[1,2,3],[],[-12,13]],S). S=[6,0,1]
*/
% start
msum(L1,L2) :-
	msum(L1,[],L2).    % [] - for saving subresults

% stop criterion L1 = []
msum([],SList,L2):-invert(SList,L2),!. % когда конец!
								   % надо разверуть так как вставляли все время в голову списка
								   % в S хранится сумма элементов текущего списка
								   % запись S в голову Acc
% if L1 not empty
msum([H|T],Acc,L2) :-
	sum_list(H,SElem),		% find sum of sublist elements
	msum(T,[SElem|Acc],L2). % save sum to Acc and continue

% find sum of list elements
% stop criterion: list is empty
sum_list([], 0).          % end tail element allways equal 0

% if list not empty
sum_list([H|T], Sum) :-
	sum_list(T, Sum1),    % for each element in list
	Sum is Sum1 + H.      % accumulate sum and update it

/* №8 Maze */
% roads between sections in maze
% 1
road(s0,s1).
road(s0,s2).
road(s0,s3).
road(s0,s4).
road(s0,s5).
% 2
road(s1,s6).
road(s2,s7).
road(s3,s7).
road(s4,s8).
road(s4,s9).
road(s5,s10).
% 3
road(s7,s11).
road(s8,s12).
road(s9,s16).
road(s10,s13).
% 4
road(s11,s14).
road(s11,s15).
road(s12,s16).
road(s12,s16).
road(s13,s17).
% 5
road(s15,s19).
road(s16,s18).
% 6
road(s18,s20).

have_road(Temp, New) :-
	road(Temp, New);					          % if we have the road
	road(New, Temp).
%
continue_way([Temp | Tail], [New, Temp | Tail]):- % continue the way
    have_road(Temp, New),					      % if we have the road
    not(member(New, [Temp | Tail])).              % and section is not visited

% stop criterion: we founded finishSection
bfs([[FinalSector | Tail] | _], FinalSector, [FinalSector | Tail]). % if there are several ways we are looking for the shortest

% Breadth First Search
bfs([HeadWay | TailWays], FinalSector, Way) :-
    findall(W, continue_way(HeadWay, W), Ways),  % save to Ways founded ways
    append(TailWays, Ways, NewWays),             % append founded ways to NewWays
    bfs(NewWays, FinalSector, Way).              % continue algo with founded ways

% stop criterion
print([],[]).
print([]).

% print ways
print([Way|WaysList]) :-
	not(member(Way, WaysList)),!,
	reverse(Way, RevWay),  % bfs return reverse way? therefore we need to reverse it
    write(RevWay), nl,
    print(WaysList);
    print(WaysList).

% ____start____
% input: start and final sections (From, To)
path(From, To):-
	% findall(X, P, L) - we form a list L consisting of X and satisfying the goal P
    findall(Way, bfs([[From]], To, Way), WaysList),  % find the shortest way with bfs
    print(WaysList).								 % print the shortest way