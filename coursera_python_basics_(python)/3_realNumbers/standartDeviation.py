'''
x1^2 - 2*x1*s + s^2 + x2^2 - 2*x2*s + s^2
(x1^2 + ... + xn^2) -2*(x1 + ... + xn) * s + n*s^2
'''

x_i = int(input())
sumX1Xn = 0
squareSumX1Xn = 0
n = 0
while x_i != 0:
    sumX1Xn += x_i
    squareSumX1Xn += x_i ** 2
    n += 1
    x_i = int(input())

sigma = 0.0
if n > 1:
    s = sumX1Xn / n
    squares = squareSumX1Xn - 2 * sumX1Xn * s + n * s ** 2
    sigma = (squares / (n - 1)) ** 0.5
print(sigma)
