//
// Created by dmitry on 15.10.2020.
//
#include <stdio.h>
#include "math.h"
#include "header.h"

int isNumber(const char *arg) {
    if(arg[0] == '0' && arg[1] != '\0') {  //  число, начинающееся с нуля
        fprintf(stderr, "Error message : %s\n", INVALID_NUM);
        return 0;
    }
    int i = 0;
    if (arg[0] == '-')   // учитываем знак числа
        i++;
    int dotCounter = 0;  // число может содержать одну точку не в начале
    while(arg[i] != '\0') {
        if (!(arg[i] >= '0' && arg[i] <= '9' || arg[i] == '.' && i != 0)) {
            if(arg[i] == '.')
                dotCounter++;
            printf("%c", arg[i]);
            fprintf(stderr, "Error message : %s\n", ARG_NOT_NUM);
            return 0;
        }
        i++;
    }
    if(dotCounter > 1) {
        fprintf(stderr, "Error message : %s\n", INVALID_NUM);
        return 0;
    }
    return 1;
}

double findRadiusOfTheCircumscribedCircle(struct Triangle triangle) {
    // находим стороны треугольника и его площадь
    double edgeA = sqrt(pow(triangle.x1 - triangle.x2, 2) + pow(triangle.y1 - triangle.y2, 2));
    double edgeB = sqrt(pow(triangle.x2 - triangle.x3, 2) + pow(triangle.y2 - triangle.y3, 2));
    double edgeC = sqrt(pow(triangle.x3 - triangle.x1, 2) + pow(triangle.y3 - triangle.y1, 2));
    double halfPerimeter = (edgeA + edgeB + edgeC) / 2;
    double areaOfTriangle = sqrt(halfPerimeter * (halfPerimeter - edgeA) * (halfPerimeter - edgeB) * (halfPerimeter - edgeC));
    // находим радиус описанной около треугольника окружности
    return edgeA * edgeB * edgeC / (4 * areaOfTriangle);
}

double matrixDet(double m[3][3]) {
    return m[0][0] * (m[1][1] * m[2][2] - m[2][1] * m[1][2]) - m[0][1] * (m[1][0] * m[2][2] - m[2][0] * m[1][2]) +
           m[0][2] * (m[1][0] * m[2][1] - m[2][0] * m[1][1]);
}

void findCenterOfTheCircumscribedCircle(struct Triangle triangle, struct Square square) {
    double matrixX0[3][3] = {
            {pow(triangle.x1, 2) + pow(triangle.y1, 2), triangle.y1, 1},
            {pow(triangle.x2, 2) + pow(triangle.y2, 2), triangle.y2, 1},
            {pow(triangle.x3, 2) + pow(triangle.y3, 2), triangle.y3, 1}
    };
    double matrixY0[3][3] = {
            {triangle.x1, pow(triangle.x1, 2) + pow(triangle.y1, 2), 1},
            {triangle.x2, pow(triangle.x2, 2) + pow(triangle.y2, 2), 1},
            {triangle.x3, pow(triangle.x3, 2) + pow(triangle.y3, 2), 1}
    };
    double matrixB[3][3] = {
            {triangle.x1, triangle.y1, 1},
            {triangle.x2, triangle.y2, 1},
            {triangle.x3, triangle.y3, 1}
    };

    // формулы для нахождения центра описанной окружности (точка пересечения серединных перпендикуляров треугольника)
    square.x = matrixDet(matrixX0) / (2 * matrixDet(matrixB));
    square.y = matrixDet(matrixY0) / (2 * matrixDet(matrixB));
}

struct Square createSquare(struct Triangle triangle) {
    struct Square square;
    // нахожу длину стороны квадрата, как удвоенный радиус вписанной в него окружности
    square.sideEdge = findRadiusOfTheCircumscribedCircle(triangle) * 2;
    // нахожу центр описанной окружности, как точку пересечения серединных перпендикуляров треугольника
    findCenterOfTheCircumscribedCircle(triangle, square);
    // по умолчанию сторона квадрата параллельна горизонтали
    square.angle = 0;
    return square;
}
