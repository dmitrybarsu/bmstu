//
// Created by dmitry on 28.12.2020.
//

#include "Worker.h"

Worker::Worker() {
    lastName = "Noname";
    firstName = "Noname";
    gender = 'M';
    salary = 45000.;
    positionId = 0;
    experience = 0;
}

Worker::Worker(double _salary,int _positionId, int _experience) {
    lastName = "Noname";
    firstName = "Noname";
    gender = 'M';
    salary = _salary;
    positionId = _positionId;
    experience = _experience;
}