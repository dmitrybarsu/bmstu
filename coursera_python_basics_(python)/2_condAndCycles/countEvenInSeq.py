counterEven = 0
cur = int(input())
while cur != 0:
    if cur % 2 == 0:
        counterEven += 1
    cur = int(input())
print(counterEven)
