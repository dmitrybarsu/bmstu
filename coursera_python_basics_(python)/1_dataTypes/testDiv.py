a = int(input())
b = int(input())

flagYes = -(a % b) + 1
flagNo = int((-2 ** flagYes) / 2) + 1

print("YES" * flagYes, end="")
print("NO" * flagNo, end="")
