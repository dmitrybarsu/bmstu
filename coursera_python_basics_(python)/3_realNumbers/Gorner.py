n = int(input())
x = float(input())

poly = 0
while n:
   a_i = float(input())
   poly = (poly + a_i) * x
   n -= 1
else:
   a_0 = float(input())
   poly += a_0

print("{0:.1f}".format(poly))
