import numpy as np
from matplotlib import pyplot

# GPy-------------------------------------------------------------------
# conda install -c conda-forge GPy
import GPy

N = 50
noise_var = 0.05
# Задаем выборку
np.random.seed(1)
x_train = np.linspace(0,10,50)[:,None]
k = GPy.kern.RBF(1)
y_train = np.random.multivariate_normal(np.zeros(N), k.K(x_train) + np.eye(N) * np.sqrt(noise_var)).reshape(-1,1)
# Задаем ядро
kernel = GPy.kern.RBF(input_dim=1, variance=1., lengthscale=0.1)
# Строим модель
model = GPy.models.GPRegression(x_train, y_train, kernel)
# Запускаем оптимизатор
model.optimize()
# Строим график
model.plot()


# kernel combinations
k1 = GPy.kern.RBF(1,1.,2.)
k2 = GPy.kern.Matern32(1, 0.5, 0.2)

# Product of kernels
k_prod = k1 * k2
k_prod.plot()

k1 = GPy.kern.RBF(1, 1., 2., active_dims=[0])
k2 = GPy.kern.Matern32(1, 0.5, 0.2, active_dims=[1])
k_prod_tensor = k1 * k2
k_prod_tensor.plot()
# pyplot.show()

# GPflow-------------------------------------------------------------------
# простая в использовании мало документации
# построение графиков вручную
# conda install -c conda-forge gpflow
# не поддерживает версии python 3.7
import gpflow


def plot_with_sigma(x_train, y_train, x, y, sigma, label):
    pyplot.plot(x, y, label="Среднее")
    pyplot.fill_between(x.ravel(), y.ravel() - 2 * sigma.ravel(), y.ravel() + 2 * sigma.ravel(), alpha=0.2, \
                        color="mediumspringgreen", label='Доверительный интервал')
    pyplot.plot(x_train, y_train, 'X', markersize=8, markerfacecolor='greenyellow', \
              markeredgecolor="blue", markeredgewidth=3, label=u'Точки выборки')
    pyplot.title(label)
    pyplot.ylim([-3, 3])
    pyplot.legend()


kernel = gpflow.kernels.RBF(input_dim=1, variance=1., lengthscales=0.1)
model = gpflow.models.GPR(x_train, y_train, kern=kernel)
optimizer = gpflow.train.ScipyOptimizer()
optimizer.minimize(model, disp=True, maxiter=100)
x = np.linspace(0, 10, 100).reshape(-1, 1)
y, sigma = model.predict_y(x)
pyplot.figure()
plot_with_sigma(x_train, y_train, x, y, np.sqrt(sigma), 'GPflow')
pyplot.show()

#PyMC3---------------------------------------------------------------------
# различные методы создания выборок с помощью которых можно строить любые вероятностные модели
# conda install -c conda pymc3
# conda install -c conda theano
import pymc3 as pm
import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
import pandas as pd

# set the seed
np.random.seed(1)

n = 50  # The number of data points
X = np.linspace(0, 10, n)[:, None]  # The inputs to the GP, they must be arranged as a column vector

# Define the true covariance function and its parameters
ℓ_true = 1.0
η_true = 3.0
cov_func = η_true**2 * pm.gp.cov.Matern52(1, ℓ_true)

# A mean function that is zero everywhere
mean_func = pm.gp.mean.Zero()

# The latent function values are one sample from a multivariate normal
# Note that we have to call `eval()` because PyMC3 built on top of Theano
f_true = np.random.multivariate_normal(mean_func(X).eval(),
                                       cov_func(X).eval() + 1e-8*np.eye(n), 1).flatten()

# The observed data is the latent function plus a small amount of IID Gaussian noise
# The standard deviation of the noise is `sigma`
σ_true = 2.0
y = f_true + σ_true * np.random.randn(n)


with pm.Model() as model:
    ℓ = pm.Gamma("ℓ", alpha=2, beta=1)
    η = pm.HalfCauchy("η", beta=5)

    cov = η**2 * pm.gp.cov.Matern52(1, ℓ)
    gp = pm.gp.Marginal(cov_func=cov)

    σ = pm.HalfCauchy("σ", beta=5)
    y_ = gp.marginal_likelihood("y", X=X, y=y, noise=σ)

    mp = pm.find_MAP()

# "mp" stands for marginal posterior
pd.DataFrame({"Parameter": ["ℓ", "η", "σ"],
              "Value at MAP": [float(mp["ℓ"]), float(mp["η"]), float(mp["σ"])],
              "True value": [ℓ_true, η_true, σ_true]})
X_new = np.linspace(0, 10, 200)[:,None]

# add the GP conditional to the model, given the new X values
with model:
    f_pred = gp.conditional("f_pred", X_new)

# To use the MAP values, you can just replace the trace with a length-1 list with `mp`
with model:
    pred_samples = pm.sample_posterior_predictive([mp], vars=[f_pred], samples=50)

# predict
mu, var = gp.predict(X_new, point=mp, diag=True)
sd = np.sqrt(var)

# draw plot
fig = plt.figure(figsize=(6,5)); ax = fig.gca()
plt.plot(X_new, mu, 'navy', label="Cреднее");
# plot mean and 2σ intervals
plt.fill_between(X_new.flatten(), mu - 2*sd, mu + 2*sd, color="mediumspringgreen", alpha=0.5, label="Доверительный интервал")
# plot original data and true function
plt.plot(X, y, 'X', markersize=6, markerfacecolor='greenyellow', \
              markeredgecolor="blue", markeredgewidth=3, label=u'Точки выборки')
plt.xlabel("x"); plt.ylim([-10,10])
plt.title("PyMC3"); plt.legend()
plt.show()

#Scikit-learn---------------------------------------------------------------------
# сложная настройка параметров

from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, WhiteKernel, ConstantKernel as C


def plot_with_sigma(x_train, y_train, x, y, sigma, label):
    pyplot.plot(x, y, label="Среднее")
    pyplot.fill_between(x.ravel(), y.ravel() - 2 * sigma.ravel(), y.ravel() + 2 * sigma.ravel(), alpha=0.2, \
                        color="mediumspringgreen", label='Доверительный интервал')
    pyplot.plot(x_train, y_train, 'X', markersize=8, markerfacecolor='greenyellow', \
              markeredgecolor="blue", markeredgewidth=3, label=u'Точки выборки')
    pyplot.title(label)
    pyplot.ylim([-3, 3])
    pyplot.legend()


kernel = C(1.0, (1e-3, 1e3)) * RBF(10, (1e-2, 1e1)) + WhiteKernel(noise_level=0.05)
model = GaussianProcessRegressor(alpha=1e-10, copy_X_train=True,
                                 kernel=kernel)
N = 50
noise_var = 0.05
x_train = np.linspace(0,10,50)[:,None]
k = GPy.kern.RBF(1)
y_train = np.random.multivariate_normal(np.zeros(N), k.K(x_train) + np.eye(N) * np.sqrt(noise_var)).reshape(-1,1)
model.fit(x_train, y_train)


x = np.linspace(0, 10, 100).reshape(-1, 1)
y, sigma = model.predict(x, return_std=True)
pyplot.figure()
plot_with_sigma(x_train, y_train, x, y, sigma, 'GP sklearn')
pyplot.show()