# разделители и конец строки
# print('1', '2', '3', sep=' + ', end=' ')
# print('=', 1 + 2 + 3)

# конкатенация строк
# print('2' + '3')

# арифметические операции
# print(11 + 6)
# print(11**6)
# целочисленное деление
# print(11 // 6)
# остаток от деления:
# знак B = знак D!!!!
# A // B, B > 0
# A = B * C + D, 0 <= D <= B
# A = -5, B = 2: -5 = 2 * (-3) + 1
# print(-5 // 2)
# print(-5 % 2)

# A // B, B < 0
# A = B * C + D, B < D <=0
# A = 11, B = -5: 11 = -5 * (-3) + (-4)
# print(11 // -5)
# print(11 % - 5)

# A = -11, B = -5: -11 = -5 * 2 + (-1)

# СТРОКИ
# phrase = "Hi"
# who = '"Bob"'
# print(phrase, ', ', who, '!', sep='')

# str(1234) !!!!!!!!!!!!!!!!
# ans = 2 + 3
# expr = "2 + 3 = "
# print(expr + str(ans))
# если не знаем, что нам передадут, можно использовать
# str и к строкам
# print(str(expr))

# print('abc' * 2)

# считывает строку и сохраняет в переменную
# по умолчанию считываются строки (str)
# для преобразования используем int(str)
# строка целиком преобразуется в int
# num1 = int(input())
# num2 = int(input())
# print(num1 + num2)

# print(int('100' * 100) ** 2)

# a = int(input())
# b = int(input())
# c = int(input())
# d = int(input())
#
# cost1 = a * 100 + b
# cost2 = c * 100 + d
# totalCost = cost1 + cost2
# print(totalCost // 100, totalCost % 100)

# n = int(input())
# print(n % 256)

# удаляем к последних цифр
# n = int(input())
# k = int(input())
# print(n // 10 ** k)

# даны А и Б, получить округленный вверх
# результат деления
# a = int(input())
# b = int(input())
# округление вверх
# print((a + b - 1) // b)

# -5 / -3
# -3 < D <= 0
# -5 = -3 * C + D
# C = 1
# D = -2
#
# -7 / 3
# 0 <= D < 3
# -7 = 3 * C + D
# C = -3
# D = 2

# A = 1
# 1)Создается объект 1
# 2)Создается ярлычок А
# 3)В результате присваивания создался объект 1
# и к нему привязался ярлык А

# B = 1
# 1)Создается новый ярлык и привязывается к
# старому объекту

# A = 2
# 1)Создается новый объект 2
# 2)Ярлык А привязывается к объекту 2
# (старая ссылка на объект 1 исчезнет)

# B = 3
# 1)Создается новый объект 3
# 2)Ссылка на 2 удаляется
# 3)Появляется ссылка на объект 3
# 4)После этого счетчик ссылок на объект 1 равен
# нулю, и объект 1 удаляется
# (Удаление происходит не всегда)

# С = 2 * 3 + 1
# 1)Создаются объекты 2 3 и 1
# 2)Создается временный объект 6
# 3)Создается временный объект 7
# 4)Удаляется временный объект 6
# 4)Объект 7 привязывается к переменной С
# и остается в памяти

# Со строками примерно также!!!
# Типы int и str - неизменяемые!!!

# явно указанные числа в программе также
# являются ссылками на объекты в памяти
# эти ссылки - неизменяемые

# Переменные типа BOOL ссылаются на
# два объекта в памяти
