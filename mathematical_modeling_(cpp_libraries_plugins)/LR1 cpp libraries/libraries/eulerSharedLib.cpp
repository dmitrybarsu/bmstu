//
// Created by dmitry on 07.10.2020.
//

#include "methods.h"

void Euler(std::vector<double> &T, struct Constants c) {
    for(int i = 1; i < T.size(); i++)
        T[i] = T[i - 1] + c.step * (-c.gamma * (T[i - 1] - c.T1));
}