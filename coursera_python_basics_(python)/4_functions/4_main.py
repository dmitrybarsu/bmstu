# def function(a, b):
#     a = 'value'
#     b = b + 1
#     return a, b
#
#
# a = "glow"
# b = 1
# print(function(a, b))


# def function(a):
#     a[0] = 'a'
#     a[1] = 1
#
#
# a = ["value", 0]
# function(a)
# print(a)

# from copy import deepcopy
# a = [1, 2, 3, 4, 5, 6]
# b = deepcopy(a)
# a[0] = 100
# print(b)

# Поверхностное копирование - копируется ссылка на объект
# Полное копирование - создает новый объект и ссылку на него


# объекты не имеют имен - нельзя узнать имя объекта
# можно узнать ссылочное имя
# class A:
#     pass
#
#
# a = A()
# b = a
# print(b)

# тернарный оператор
# x, y = 25, 50
# min = x if x < y else y

# как преобразовать строку в число?
# string = "1001"
# print(int(string))
# print(eval(string))

# проверка принадлежности класса подклассу

# class A:
#     pass
#
# a = A()
# print(isinstance(a, A))

# делегирование в Python - метод ООП (паттерн)
# хотим изменить один метод класса

# class A:
#     def f(self):
#         print("A.f")
#
#
# class C:
#     def __init__(self):
#         self.A = A()
#
#     def f(self):
#         return self.A.f()
# c = C()
# c.f()

# зачем self?
# self - принадлежность объекта самому себе
# чтобы обратиться к полям класса

# почему join чаще используется со строками, а не со списками или кортежами
# есть string модуль. этот модуль использует метод join
# join - объединяет символы в строку
# print(", ".join(['1', '2', '3']))
