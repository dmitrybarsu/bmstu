cost = float(input())
intPart = cost // 1
fracPart = cost % 1 * 100
print(f"{intPart:.{0}f}", f"{fracPart:.{0}f}")
