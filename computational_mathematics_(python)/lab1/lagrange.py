import matplotlib.pyplot as plt
import numpy as np
import math
import random


class Params:
    def __init__(self, n, m, a_j, b_k):
        self.n = n
        self.m = m
        self.a_j = a_j
        self.b_k = b_k


def gen_params():
    n = random.randint(7, 15)
    m = random.randint(7, 15)
    a_j = np.random.random_sample(m, )
    b_k = np.random.random_sample(n, )
    return Params(n, m, a_j, b_k)


def l_i(i, x, x_nodes):
    if i >= len(x_nodes):
        print("i is out of range of x_nodes[]")
        exit(1)
    l_i = 1
    for j in range(len(x_nodes)):
        if j != i:
            l_i *= (x - x_nodes[j]) / (x_nodes[i] - x_nodes[j])
    return l_i


def L(x, x_nodes, y_nodes):
    l_x = 0
    if len(x_nodes) != len(y_nodes):
        exit(2)
        print("different lengths of the array of function values and arguments")

    for i in range(len(x_nodes)):
        l_x += y_nodes[i] * l_i(i, x, x_nodes)
    return l_x


def function_values_generator(x, params):
    sum_a_j = 0
    sum_b_k = 0
    for i in range(params.m):
        sum_a_j += params.a_j[i] * x ** i
    for i in range(1, params.n):
        sum_b_k += params.b_k[i] * x ** i
    return sum_a_j / (1 + sum_b_k)


def x_ch_nodes_generator(nodes_number):
    x_ch_nodes = np.linspace(-1, 1, num=nodes_number, endpoint=True)
    for i in range(nodes_number):
        x_ch_nodes[i] = math.cos((2 * (i + 1) - 1) * math.pi / (2 * nodes_number))
    return x_ch_nodes


def norm_finding(params, y, nodes_number, dimension):
    x = np.linspace(-1, 1, num=dimension, endpoint=True)
    x_nodes = np.linspace(-1, 1, num=nodes_number, endpoint=True)
    x_ch_nodes = x_ch_nodes_generator(nodes_number)

    y_nodes = np.linspace(-1, 1, num=nodes_number, endpoint=True)
    y_ch_nodes = np.linspace(-1, 1, num=nodes_number, endpoint=True)
    for i in range(nodes_number):
        y_nodes[i] = function_values_generator(x_nodes[i], params)
        y_ch_nodes[i] = function_values_generator(x_ch_nodes[i], params)

    l = np.zeros((dimension,))
    l_ch = np.zeros((dimension,))
    dist_pade_lagrange = 0.
    dist_pade_lagrange_ch = 0.
    for i in range(dimension):
        l[i] = L(x[i], x_nodes, y_nodes)
        l_ch[i] = L(x[i], x_ch_nodes, y_ch_nodes)
        if abs(y[i] - l[i]) > dist_pade_lagrange:
            dist_pade_lagrange = abs(y[i] - l[i])
        if abs(y[i] - l_ch[i]) > dist_pade_lagrange_ch:
            dist_pade_lagrange_ch = abs(y[i] - l_ch[i])
    return np.array([dist_pade_lagrange, dist_pade_lagrange_ch])


def main():
    function_number = 100
    params_list = []
    for i in range(function_number):
        params_list.append(gen_params())

    x = np.linspace(-1, 1, num=function_number, endpoint=True)
    y_list = []
    for i in range(function_number):
        y_list.append(function_values_generator(x, params_list[i]))

    nodes_number = 6
    plots_number = 2
    x_nodes = np.linspace(-1, 1, num=nodes_number, endpoint=True)
    x_ch_nodes = x_ch_nodes_generator(nodes_number)

    y_nodes_list = []
    y_ch_nodes_list = []
    for j in range(plots_number):
        y_nodes = np.linspace(-1, 1, num=nodes_number, endpoint=True)
        y_ch_nodes = np.linspace(-1, 1, num=nodes_number, endpoint=True)
        for i in range(nodes_number):
            y_nodes[i] = function_values_generator(x_nodes[i], params_list[j])
            y_ch_nodes[i] = function_values_generator(x_ch_nodes[i], params_list[j])
        y_nodes_list.append(y_nodes)
        y_ch_nodes_list.append(y_ch_nodes)

    L_list = []
    L_ch_list = []
    for j in range(plots_number):
        L_list.append(np.zeros((function_number,)))
        L_ch_list.append(np.zeros((function_number,)))
        for i in range(function_number):
            L_list[j][i] = L(x[i], x_nodes, y_nodes_list[j])
            L_ch_list[j][i] = L(x[i], x_ch_nodes, y_ch_nodes_list[j])

    fig, axes = plt.subplots(nrows=1, ncols=plots_number, figsize=(plots_number * 10, 10))
    for i in range(plots_number):
        axes[i].plot(x, y_list[i], 'r', label="Pade function")
        axes[i].plot(x, L_list[i], 'b', label="Evenly spaced nodes")
        axes[i].plot(x, L_ch_list[i], 'g', label="Chebyshev nodes")
        axes[i].scatter(x_nodes, y_nodes_list[i], color="blue")
        axes[i].scatter(x_ch_nodes, y_ch_nodes_list[i], color="green")
        axes[i].legend(loc='upper left')
        axes[i].set_xlabel('x')
        axes[i].set_ylabel('y')
        title = "Interpolation of Lagrangian PadeFunction(" + str(i) + ") by " + str(nodes_number) + " nodes"
        axes[i].set_title(title)
        axes[i].grid()
    fig.tight_layout()

    dist_list = np.zeros((plots_number, 2, 30,))
    for j in range(plots_number):
        for i in range(1, 30):
            temp_dist = norm_finding(params_list[j], y_list[j], i, 100)
            dist_list[j][0][i] = temp_dist[0]
            dist_list[j][1][i] = temp_dist[1]

    n_nodes = np.arange(1, 31, 1)
    fig, axes = plt.subplots(nrows=1, ncols=plots_number, figsize=(plots_number * 10, 10))
    for i in range(plots_number):
        axes[i].semilogy(n_nodes, dist_list[i][0], 'r', label="Distance Pade-Lagrange evenly spaced nodes")
        axes[i].semilogy(n_nodes, dist_list[i][1], 'b', label="Distance Pade-Lagrange Chebyshev nodes")
        axes[i].legend(loc='upper right')
        axes[i].set_xlabel('N_nodes')
        axes[i].set_ylabel('Pade-Lagrange distance')
        axes[i].set_title("Pade-Lagrange distances for PadeFunction(" + str(i) + ") by 1 - 30 nodes")
        axes[i].grid()
    fig.tight_layout()
    plt.show()


main()