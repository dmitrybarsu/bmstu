p = int(input())
x = int(input())
y = int(input())

incPercent = (100 + p)
contribution = (x * 100 + y) * incPercent
contribution //= 100

print(
  int(contribution // 100),
  int(contribution % 100)
)
