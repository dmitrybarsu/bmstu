# Write the benchmarking functions here.
# See "Writing benchmarks" in the asv docs for more information.
import numpy as np
from matplotlib import pyplot
import GPy
import gpflow
import pymc3 as pm
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, WhiteKernel, ConstantKernel as C

libs = ["GPy", "gpflow", "PyMC3", "sklearn"]


def time_GP(GP_lib):
    if GP_lib == "sklrean":
        kernel = C(1.0, (1e-3, 1e3)) * RBF(10, (1e-2, 1e1)) + WhiteKernel(noise_level=0.05)
        model = GaussianProcessRegressor(alpha=1e-10, copy_X_train=True, kernel=kernel)
        N = 50
        noise_var = 0.05
        x_train = np.linspace(0, 10, 50)[:, None]
        k = GPy.kern.RBF(1)
        y_train = np.random.multivariate_normal(np.zeros(N), k.K(x_train) + np.eye(N) * np.sqrt(noise_var)).reshape(-1,                                                                           1)
        model.fit(x_train, y_train)


time_GP.param_names = ["GP library"]
time_GP.params = libs
time_GP.pretty_name = "Time benchmark for libs GPy, gpflow, PyMC3, sklearn"
