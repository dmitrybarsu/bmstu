n = int(input())
sum = 0.
while n:
    sum += 1 / (n ** 2)
    n -= 1
print("{0:.10f}".format(sum))
