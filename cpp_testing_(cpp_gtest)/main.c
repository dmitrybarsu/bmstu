/*
 Вариант #2
 Создать структуры для хранения информации об окружности, треугольнике и квадрате. +++
 Составить с использованием этих структур программу нахождения квадрата,
 в который вписана окружность, описанная вокруг заданного треугольника.
*/

#include <stdio.h>

#include "header.h"

int main(const int argc, const char *argv[]) {
    // проверка корректности аргументов программы (вводятся координаты точек треугольника)
    // например:./a.out -3 0 0 -3 3 0
    if (argc < VALID_ARGS_NUM) {
        fprintf(stderr, "Error message : %s\n", ARGS_NUM_ERROR);
        return ERROR_TERMIANTION;
    }
    for(int i = 1; i < VALID_ARGS_NUM; i++) {
        if(!isNumber(argv[i]))
            return ERROR_TERMIANTION;
    };

    // инициализируем поля структуры triangle
    struct Triangle triangle;
    triangle.x1 = (double) *argv[1];
    triangle.y1 = (double) *argv[2];
    triangle.x2 = (double) *argv[3];
    triangle.y2 = (double) *argv[4];
    triangle.x3 = (double) *argv[5];
    triangle.y3 = (double) *argv[6];

    // инициализируем поля структуры square в соответсвии с условием задачи
    struct Square square = createSquare(triangle);

    // выводим поля структуры square
    //printf("center: x:%lf y:%lf\nsideEdge:%lf\nangle:%lf", square.x, square.y, square.sideEdge, square.angle);

    return 0;
}
