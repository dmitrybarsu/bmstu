% Table 1: spherical support (SizeType, ThreadDiameter, SupportCode)
support(sphere,6,'2.910.01').
support(sphere,8,'2.910.02').
support(sphere,12,'3.910.01').
support(sphere,16,'3.910.02').


% Table 2: notched support (SizeType, ThreadDiameter, SupportCode)
support(knurl,6,'2.911.01').
support(knurl,8,'2.911.02').
support(knurl,12,'3.911.01').

% Table 3: pin suppport (SizeType, ThreadDiameter, SupportCode, MinDiameter, MaxDiameter)
support(pin,6,'2.213.01',6,8).
support(pin,8,'2.213.04',8,12).
support(pin,12,'3.213.06',12,26).

% Table 4: threaded jaw (type, SizeType, JawHeight, ThreadDiameter, JawCode)
jaw(hole,'30_18',16,6,'2.913.05').
jaw(hole,'45_22',20,8,'2.913.06').
jaw(hole,'65_30',38,12,'2.913.09').


% Table 5: slotted jaw (type, SizeType, JawHeight, DGmin, DGmax, DВ_min, DВ_max, JawCode)
jaw(prizmatic,'30_18',10,8,12,3,7,'2.913.01').
jaw(prizmatic,'45_22',12,8,12,3,7,'2.913.02').
jaw(prizmatic,'65_30',25,12,30,8,18,'2.913.07').

% Table 6: jaw clamp(ClampSizeType, JawSizeType, ClampHeight, ClampCode)
clamp('45_30','30_18',29,'2.451.01').
clamp('60_45','45_22',34,'2.451.02').
clamp('60_45','45_22',35,'3.451.01').
clamp('90_60','65_30',42,'3.451.02').

% Table 7: gasket (SizeType, GasketHeight, GasketCode)
gasket('45_30',1,'2.217.01').
gasket('45_30',2,'2.217.07').
gasket('45_30',3,'2.217.09').
gasket('45_30',5,'2.217.10').

gasket('60_45',1,'3.217.01').
gasket('60_45',2,'3.217.07').
gasket('60_45',3,'3.217.09').
gasket('60_45',5,'3.217.10').

gasket('90_60',2,'3.107.25').
gasket('90_60',3,'3.107.27').
gasket('90_60',5,'3.107.28').


% form an arbitrary package of gaskets (SizeType, number(N), TotalHeight(H), CodesList)
% case1: stop criterion: H = 0 -> OK
gaskets(_,0,H,[]) :-
	H=0,
	!.

% case2: stop criterion: H < 0 -> FAIL
gaskets(_,0,H,[]) :-
	H<0,
	!,
	fail.

% form an arbitrary package of gaskets with a given standard size (SizeType) and total height (H)
% example: gaskets('60_45',N,12,CodesList). 
gaskets(SizeType,N,H,[Type|T]) :-
	gasket(SizeType,H1,Type),                                    % a package of gaskets consisting of at least one gasket
	H2 is H-H1,					                                 % the height of the gaskets not including the first
	gaskets(SizeType,N1,H2,T),                                   % recursion
	N is N1+1,					                                 % total amount of gaskets
	!.                                                           % no backtracking

% forming the movepart (PlaneType, WorkpieceDiameter, JawCode, SupportCode, JawHeight, JawSizeType)
% example: movepart(finplane, WorkpieceDiameter, JawCode, SupportCode, JawHeight, JawSizeType). 
% finishing workpiece -> sphere support and jaw hole
movepart(finplane,_,JawCode,SupportCode,HJaw,JawSizeType) :-
	support(sphere,DM,SupportCode),       	                     % choise the support
	jaw(hole,JawSizeType,HJaw,DM,JawCode).                       % choise the jaw

% roughing workpiece -> khurl support and jaw hole
movepart(roughplane,_,JawCode,SupportCode,HJaw,JawSizeType) :-
	support(knurl,DM,SupportCode),                               % choise the support
	jaw(hole,JawSizeType,HJaw,DM,JawCode).                       % choise the jaw

% perforated workpiece -> pin support and jaw hole and Dmin < D < Dmax
movepart(perforated,D,JawCode,SupportCode,HJaw,JawSizeType) :-
	nonvar(D),                                                   % if D is not a free variable
	support(pin,DM,SupportCode,Dmin,Dmax),                       % choise the support
	D>=Dmin, D=<Dmax,                                            % if support fit to workpiece
	jaw(hole,JawSizeType,HJaw,DM,JawCode).                       % choise the jaw

% cylindrical vertical workpiece -> DVmin <= D <= DVmax and jaw prizmatic
movepart(cylindrvert,D,JawCode,_,HJaw,JawSizeType) :-
	nonvar(D),												     % if D in not a free variable
	jaw(prizmatic,JawSizeType,HJaw,_,_,DVmin,DVmax,JawCode),     % choise prizmatic jaw
	D>=DVmin, D=<DVmax.                                          % if jaw fit to workpiece?

% cylindrical horizontal workpiece -> DHmin <= D <= DHmax
movepart(cylindrhor,D,JawCode,_,HJaw,JawSizeType):-
	nonvar(D),                                                   % if D in not a free variable
	jaw(prizmatic,JawSizeType,HJaw,DHmin,DHmax,_,_,JawCode),     % choise prizmatic jaw
	D>=DHmin, D=<DHmax.                                          % if jaw fit to workpiece?

% forming the constructing universal-assembled device (machinetool)
% machinetool(ClampHeight, WorkpiecePlaneType, WorkpieceDiameter, NumberOfGaskets, GasketsCodes, ClampCode, JawCode, SupportCode, Outlist).
% example: machinetool(85, finplane, WorkpieceDiameter, NumberOfGaskets, GasketsCodes, ClampCode, JawCode, SupportCode, Outlist).
machinetool(H,BlankType,D,GasketAmount,GasketCodes,ClampCode,JawCode,SupportCode,OutList) :-
	movepart(BlankType,D,JawCode,SupportCode,HJaw,JawSizeType),  % forming movepart 
	clamp(ClampSizeType,JawSizeType,HClamp,ClampCode),           % choise the clamp
	HGaskets is H - HJaw - HClamp - 30,                          % define a height of gaskets
	gaskets(ClampSizeType,GasketAmount,HGaskets,GasketCodes),    % fit gaskets to defined height
	% printgaskets(OutList,GasketCodes).							 % print package of gaskets
	nl, printgaskets(OutList,GasketCodes), nl.

% count the quantity of solutions
machinetoolcountsol(C,H,BlankType,D):-
	findall(GasketAmount,machinetool(H,BlankType,D,GasketAmount,_,_,_,_,_),List),
	length(List,C).

% find uniq elements in List and save them to Out
% stop criterion: L is empty and Out is empty
uniqlist([],[]) :-
	!.

uniqlist([H|T],Out) :- 
	member(H,T),		                                         % if element of L1 is not uniq
	uniqlist(T,Out).                                             % do nothing and continue

uniqlist([H|T],[H|T1]) :-                                        % add element of L1 to Out
	not(member(H,T)),                                            % if element of L1 is uniq
	uniqlist(T,T1).                                               

% print quantity of gaskets: [[1:3.107.28],[2:3.217.01]]
% (3.107.28 - 1 piece, 3.217.01 - 2 pieces)
printgaskets(OutList,GasketsList) :-
	uniqlist(GasketsList,UniqueGaskets),                         % choise uniq gaskets codes
	addgroups(List,GasketsList,UniqueGaskets),                   % find quantity of each gasket code
	printdatatolist(OutList,List,UniqueGaskets),
	!.

% stop criterion: empty lists
printdatatolist([],[],[]).

% L1 = [H1:H2]|T]
% L1 = [$VAR(H1):$VAR(H2)|$VAR(T)] => [[1:3.107.28]|T]
% H2 - gasket code, H1 - the quantity of each gasket code
printdatatolist([[H1:H2]|T],[H1|T1],[H2|T2]) :-
	printdatatolist(T,T1,T2).

% case1: stop criterion: NumberOfEachGasketCode = 0 and GasketsList is empty
addgasketstogroup(0,[],_).

% case2: if H != H1: we do nothing and continue
addgasketstogroup(C,[H|T],H1) :-
	H1\=H,
	addgasketstogroup(C,T,H1).

% case3: if H == H1 => C = C1 + 1
addgasketstogroup(C,[H|T],H1) :-
	H1=H,
	addgasketstogroup(C1,T,H1),
	C is C1+1.

% stop criterion: empty L1 and UniqueGaskets
addgroups([],_,[]).

% addgroups(L1,GasketsList,UniqueGaskets) 
addgroups([C1|T],GasketsList,[H1|T1]) :-      % write quantity to C1
	addgasketstogroup(C1,GasketsList,H1),     % find quantity of each gasket code and save it to C1
	addgroups(T,GasketsList,T1).              % continue

% calculate how long it takes to execute predicate X in milliseconds
timer(X) :-
    write('Executing: '), write(X), nl,
    statistics(walltime, _), call(X), statistics(walltime, [_,E]),
    nl, 
    write('--------------'), nl,
    write('Time: '), write(E), write(' ms.'),nl,
    write('--------------'), nl.