import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import csv

font = {'family' : 'normal',
        'size'   : 18}

matplotlib.rc('font', **font)

def qubic_spline_coeff(x_nodes, y_nodes):
    N = len(y_nodes)
    h = np.zeros((N - 1,))
    a = np.zeros((N - 1,))
    for i in range(N - 1):
        a[i] = y_nodes[i]
        h[i] = x_nodes[i + 1] - x_nodes[i]
    # A * c = k
    k = np.zeros((N, ))
    diagonal = np.ones(N)
    diagonal_1 = np.zeros(N - 1)
    diagonal_2 = np.zeros(N - 1)
    for i in range(1, N - 1):
        k[i] = 3 / h[i] * (y_nodes[i + 1] - y_nodes[i]) - 3 / h[i - 1] * (y_nodes[i] - y_nodes[i - 1])
        diagonal_1[i - 1] = h[i - 1]
        diagonal[i] = 2 * (h[i] + h[i - 1])
        diagonal_2[i] = h[i]
    A = np.diag(diagonal) + np.diag(diagonal_1, -1) + np.diag(diagonal_2, 1)
    # c = inv matrix A * k
    A = np.linalg.inv(A)
    c = np.dot(A, k)
    b = np.zeros((N - 1,))
    d = np.zeros((N - 1,))
    for i in range(N - 1):
        b[i] = 1 / h[i] * (y_nodes[i + 1] - y_nodes[i]) - h[i] / 3 * (c[i + 1] + 2 * c[i])
        d[i] = (c[i + 1] - c[i]) / (3 * h[i])
    return np.c_[a, b, c[:-1], d]


def find_index(x, x_nodes):
    for i in range(1, len(x_nodes)):
        if x <= x_nodes[i]:
            return i-1
    
    
def qubic_spline(x, qs_coef, x_nodes):
    i = find_index(x, x_nodes)
    return qs_coef[i][0] + qs_coef[i][1] * (x - x_nodes[i]) + \
               qs_coef[i][2] * (x - x_nodes[i]) ** 2 + qs_coef[i][3] * (x - x_nodes[i]) ** 3


def d_qubic_spline(x, qs_coef, x_nodes):
    if x_nodes[0] <= int(x) < x_nodes[len(x_nodes) - 1]:
        i = int(x) - x_nodes[0]
        return qs_coef[i][1] + 2 * qs_coef[i][2] * (x - x_nodes[i]) + 3 * qs_coef[i][3] * (x - x_nodes[i]) ** 2

    
def create_function_values(x_nodes, qs_coef1, dimension):
    x = np.linspace(x_nodes[0], x_nodes[len(x_nodes) - 1], dimension)
    y = np.zeros((dimension,))
    for i in range(dimension):
        y[i] = qubic_spline(x[i], qs_coef1, x_nodes)
    return [x.transpose(), y.transpose()]
    

def reduce_nodes_number(x_nodes, y_nodes, right_time, date_time):
    new_x = []
    new_y = []
    for i in range(len(x_nodes)):
        for j in range(len(right_time)):
            if date_time[i].find(right_time[j]) != -1:
                new_x.append(x_nodes[i])
                new_y.append(y_nodes[i])
                i += 1
                j = 0
    return [new_x, new_y]


def middle_temp(func_values, x_nodes, date_time):
    cur_day = int(date_time[0][:2])
    middle_temp = []
    day_nodes_number = 0
    temperature_sum_by_day = 0
    for i in range(len(func_values[0])):
        j = find_index(func_values[0][i], x_nodes)
        if cur_day != int(date_time[j][:2]):
            cur_day += 1
            middle_temp.append(temperature_sum_by_day / day_nodes_number)
            temperature_sum_by_day = func_values[1][i]
            day_nodes_number = 1
        else:
            temperature_sum_by_day += func_values[1][i]
            day_nodes_number += 1
    middle_temp.append(temperature_sum_by_day /  day_nodes_number)
    return middle_temp


def find_rms_norm(qs_coef1, x_nodes1, qs_coef2, x_nodes2):
    rms_sum = 0
    rms_count = 0
    for i in range(len(x_nodes1)):
        if x_nodes2[0] <= i <= x_nodes2[len(x_nodes2) - 1]:
            #if current node has reduced: calculate distance
            if any(i == j for j in x_nodes2) == False:
                distance = qubic_spline(i, qs_coef1, x_nodes1) - qubic_spline(i, qs_coef2, x_nodes2)
                rms_sum += distance ** 2
                rms_count += 1
    if rms_count != 0:
        return np.sqrt(rms_sum / rms_count)

    
def find_max_norm(qs_coef1, x_nodes1, qs_coef2, x_nodes2):
    max_distance = 0
    for i in range(len(x_nodes1)):
        if x_nodes2[0] <= i <= x_nodes2[len(x_nodes2) - 1]:
            tmp_distance = abs(qubic_spline(i, qs_coef1, x_nodes1) - qubic_spline(i, qs_coef2, x_nodes2))
            if tmp_distance > max_distance:
                max_distance = tmp_distance
    return max_distance


def find_max_temperature_dist(mid_t1, mid_t2):
    max_temp_dist = 0.
    for i in range(len(mid_t1)):
        if abs(mid_t1[i] - mid_t2[i]) > max_temp_dist:
            max_temp_dist = abs(mid_t1[i] - mid_t2[i])
    return max_temp_dist


def temp_dist_print(mid_t):
    print("\ntemp_dist 8-4 nodes temp_dist 8-2 nodes")
    for i in range(len(mid_t[0])):
        print(abs(mid_t[0][i] - mid_t[1][i]), abs(mid_t[0][i] - mid_t[2][i]))
    

def main():
    x_nodes = []
    y_nodes = []
    date_time = []
    with open('Белгород_январь_2019.csv') as csv_file:
        row_count = 0
        file_to_read = csv.reader(csv_file, delimiter=';')
        for row in file_to_read:
            if row_count > 0:
                date_time.append(row[0])
                y_nodes.append(float(row[1]))
                x_nodes.append(row_count - 1)
            row_count += 1
    date_time.reverse()
    y_nodes.reverse()
    
    dimension = 600
    qs_coef1 = qubic_spline_coeff(np.array(x_nodes).T, np.array(y_nodes).T) 
    func_values1 = create_function_values(x_nodes, qs_coef1, dimension)
    middle_temperature1 = middle_temp(func_values1, x_nodes, date_time) 
    
    right_time = ['03:00', '09:00', '15:00', '21:00']
    reduced_nodes2 = reduce_nodes_number(x_nodes, y_nodes, right_time, date_time)
    qs_coef2 = qubic_spline_coeff(np.array(reduced_nodes2[0]).T, np.array(reduced_nodes2[1]).T) 
    func_values2 = create_function_values(reduced_nodes2[0], qs_coef2, dimension)
    middle_temperature2 = middle_temp(func_values2, x_nodes, date_time) 
    
    right_time = ['03:00', '15:00']
    reduced_nodes3 = reduce_nodes_number(x_nodes, y_nodes, right_time, date_time)
    qs_coef3 = qubic_spline_coeff(np.array(reduced_nodes3[0]).T, np.array(reduced_nodes3[1]).T) 
    func_values3 = create_function_values(reduced_nodes3[0], qs_coef3, dimension)
    middle_temperature3 = middle_temp(func_values3, x_nodes, date_time) 
    
    fig, axe = plt.subplots(nrows = 1, ncols = 1, figsize = (20, 10))
    axe.plot(func_values1[0], func_values1[1], 'r', label="Cubic spline by all nodes")
    axe.plot(func_values2[0], func_values2[1], 'b', label="Cubic spline by half nodes")
    axe.plot(func_values3[0], func_values3[1], 'g', label="Cubic spline by fourth nodes")
    axe.scatter(x_nodes[::4], y_nodes[::4], color="red")
    plt.xticks(x_nodes[1::8], np.arange(1, 32, 1))
    axe.legend(loc="upper left")
    axe.set_xlabel('Day of January 2019')
    axe.set_ylabel('Temperature in Belgorod for January 2019')
    title = "Interpolation by cubic splines"
    axe.set_title(title)
    axe.grid()
    fig.tight_layout()
    
    x = np.arange(1, 32, 1)
    fig, axe = plt.subplots(nrows = 1, ncols = 1, figsize = (20, 10))
    axe.plot(x, middle_temperature1, 'r', label="Middle day tempearature by all nodes")
    axe.plot(x, middle_temperature2, 'b', label="Middle day tempearature by half nodes")
    axe.plot(x, middle_temperature3, 'g', label="Middle day tempearature by fourth nodes")
    axe.scatter(x, middle_temperature1, color="red")
    plt.xticks(np.arange(1, 32, 1))
    axe.legend(loc="upper left")
    axe.set_xlabel('Day of january 2019')
    axe.set_ylabel('Middle day temperature')
    title = "Middle day temperature in Belgorod for January 2019"
    axe.set_title(title)
    axe.grid()
    fig.tight_layout()
    plt.show()
    
    print(np.mean(abs(np.array(middle_temperature1) - np.array(middle_temperature2))))
    print(np.mean(abs(np.array(middle_temperature1) - np.array(middle_temperature3))))
    print("rms_norm1 8 - 4 nodes in day: ", find_rms_norm(qs_coef1, x_nodes, qs_coef2, reduced_nodes2[0]))
    print("max_norm1 8 - 4 nodes in day: ", find_max_norm(qs_coef1, x_nodes, qs_coef2, reduced_nodes2[0]))
    print("rms_norm2 8 - 2 nodes in day: ", find_rms_norm(qs_coef1, x_nodes, qs_coef3, reduced_nodes3[0]))
    print("max_norm2 8 - 2 nodes in day: ", find_max_norm(qs_coef1, x_nodes, qs_coef3, reduced_nodes3[0]))
    print("max_temp_dist1 8 - 4 nodes in day: ",find_max_temperature_dist(middle_temperature1, middle_temperature2))
    print("max_temp_dist2 8 - 2 nodes in day: ",find_max_temperature_dist(middle_temperature1, middle_temperature3))
    temp_dist_print([middle_temperature1, middle_temperature2, middle_temperature3])
    
    
main()