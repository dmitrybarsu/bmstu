n = int(input())
prev = 0
cur = 1
if n == 0:
    cur = 0
# 0, 1, .., n + 1. первые два не включаем
# n + 1 - 2 = n - 1
while n - 1:
    tmp = cur
    cur += prev
    prev = tmp
    n -= 1
print(cur)
