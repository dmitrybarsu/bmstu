//
// Created by dmitry on 07.10.2020.
//

#include <vector>
#include "base.h"

class Euler : public interfaceSolverPlugin {
public:
    virtual std::string getPluginName(std::string objectName)const {
        objectName = "Euler";
        return objectName;
    }
    virtual void Solver(std::vector<double> &T, struct Constants c) {
        for(int i = 1; i < T.size(); i++)
            T[i] = T[i - 1] + c.step * (-c.gamma * (T[i - 1] - c.T1));
    };
    ~Euler() override {};
};

extern "C" interfaceBasePlugin* registerPlugin()
{
    return new Euler();
}