/* Variant 57
The queen is on the D1 square of the chessboard. 
It is necessary to find a sequence of five moves that ensures 
that the queen passes the maximum number of squares on the board. 
In this case, the queen has no right to stop on any square more 
than once and cross the already passed route with subsequent moves.
-----------------------------------------------------------------------
Ферзь находится на поле D1 шахматной доски. 
Необходимо найти последовательность из пяти ходов, 
обеспечивающую прохождение ферзем максимального количества полей доски. 
При этом ферзь не имеет права останавливаться на любом поле более 
одного раза и пересекать уже пройденный маршрут последующими ходами.
*/

% movement: 
% "+" => + 1 right or up,
% "-" => - 1 left or down, 
% "*" => * 1 no movement.
move(-, *). % left
move(-, -). % left-up
move(*, +). % up
move(+, +). % right-up
move(+, *). % right
move(*, -). % down

%do one step: +1, -1 or nothing
step(X, Y, Xop, Yop, Used, Xn, Yn) :- 
	Xaction =.. [Xop, X, 1],		   % create a function for calculating Xnew,Ynew
	Yaction =..[Yop, Y, 1],
	Xnew is Xaction,				   % call function and save result in Xnew, Ynew
	Ynew is Yaction,
	not(member([Xnew, Ynew], Used)),   % test: this cell is not in used?
	Xnew >= 1, Xnew =< 8, 			   % test: out of bounds
	Ynew >= 1, Ynew =< 8, 
	Xn = Xnew, Yn = Ynew.			   % if all tests are correct, we make a step

% case1: if we can make step
make_move(X, Y, Xop, Yop, Used, Newused, Curmove, Move, Curlen, Mlen, Xnext, Ynext) :- 
	Xaction =.. [Xop, X, 1],		   % create a function for calculating Xnew,Ynew
	Yaction =.. [Yop, Y, 1],           
	Xnew is Xaction,                   % call function and save result in Xnew, Ynew
	Ynew is Yaction,
	not(member([Xnew, Ynew], Used)),   % test: this cell is not in used?
	Xnew >= 1, Xnew =< 8,              % test: out of bounds
	Ynew >= 1, Ynew =< 8, 
	Newlen is Curlen + 1,              % increment len after step
	/*
	update current X,Y (X = Xnew, Y = Ynew)
	add [Xnew, Ynew] to Used
	add [Xnew, Ynew] to Curmove
	update Curlen (Curlen = Curlen + 1 = Newlen)
	*/
	make_move(Xnew, Ynew, Xop, Yop, [[Xnew, Ynew]|Used], Newused, [[Xnew,Ynew]|Curmove], Move, Newlen, Mlen, Xnext, Ynext).

% case2: else (we can't make step)
make_move(X, Y, Xop, Yop, Used, Newused, Curmove, Move, Curlen, Mlen, Xnext, Ynext) :- 
	not(step(X, Y, Xop, Yop, Used, _, _)),    % if cell in used or out of bounds
	Xnext = X, Ynext = Y, 					  % change nothing
	Move = Curmove,
	Mlen = Curlen, 
	Newused = Used.

% find all ways _____begin_____
% stop criterion: in list Move only one element
find_path(_, _, Tmp, Path, Curlen, Len, _) :- 
	length(Tmp, 5),     % number of moves = 5
	Path = Tmp,         % concretization
	Len = Curlen.	    % summary length of all moves

% X,Y - current position
% Tmp - list for saving Path
% Curlen - summary length of all moves
% Used - for saving information about vizited cells
% Path and Len for saving max path and it's len
find_path(X, Y, Tmp, Path, Curlen, Len, Used) :-
	move(Xop, Yop),
	make_move(X, Y, Xop, Yop, Used, Newused, [[X, Y]], Move, 0, Mlen, Xnext,Ynext),   % make steps in one direction while we can
	not(length(Move, 1)),                                                             % if in list Move not 1 element
	Newlen is Curlen + Mlen, 														  % Newlen = len_of_previous_moves + current_move
	/*
	update Xnext, Ynext (after move)
	save Move to Tmp
	update Curlen = Curlen + Mlen = Newlen
	update Used = Newused (after move)
	*/
	find_path(Xnext, Ynext, [Move|Tmp], Path, Newlen, Len, Newused).
% find all ways _____end_____

% find max way from the list _____begin_____
% we have two predicats: get_max with 4 and get_max with 6 arguments
% get_max with 4 arguments: H = [Path0, Len0], T = [[Path1, Len1], ..., [Pathn, Lenn]]
% Curlen = 0
get_max([H|T], Maxpath, Maxlen, Curlen) :- 
	get_max(H, T, Curlen, [], Mp, Ml),
	Maxpath = Mp, 
	Maxlen = Ml.

% get_max with 6 arguments: H = Pathi, T = Leni
% stop criterion: [Pathn, Lenn] 
% case1: current_path_len (Leni) > max_current_founded_len(Curlen)
get_max([H|T], [], Curlen, _, Mp, Ml) :- 
	T > Curlen,     % test: current_path_len > max_current_founded_len?
	Ml = T,         % conctretization and return to get_max with 4 arguments
	Mp = H.
% stop criterion: [Pathn, Lenn]
% case2: current_path_len (Leni) =< max_current_founded_len(Curlen)
get_max([_|T], [], Curlen, Tmp, Mp, Ml) :- 
	T =< Curlen, 
	Ml = Curlen, 
	Mp = Tmp.

% H = Pathi, T = Leni, H1 = [[Pathi+1, Leni+1], T1 = [[Pathi+2, Leni+2], ..., [Pathn, Lenn]]
% case3: current_path_len (Leni) > max_current_founded_len(Curlen)
get_max([H|T], [H1|T1], Curlen, _, Mp, Ml) :- 
	T > Curlen, 			          % test: current_path_len > max_current_founded_len?
	get_max(H1, T1, T, H, Mp, Ml).	  % Curlen = T = Leni, Tmp = H

% H = Pathi, T = Leni, H1 = [[Pathi+1, Leni+1], T1 = [[Pathi+2, Leni+2], ..., [Pathn, Lenn]]
% case4: current_path_len (Leni) =< max_current_founded_len(Curlen)
get_max([_|T], [H1|T1], Curlen, Tmp, Mp, Ml) :- 
	T =< Curlen, 						    % test: current_path_len =< max_current_founded_len?
	get_max(H1, T1,Curlen, Tmp, Mp, Ml).	% Curlen = Curlen, Tmp = Tmp
% find max way from the list _____end_____

% _____start_____
% input: Path = X, Length = Y - we should find it
find_max_path(Path, Length) :-
	% we find current way with find_path(...), then we remember Path and Len in List
	findall([Path, Len], find_path(4, 1, [], Path, 0, Len, [[4, 1]]), List),    % find all ways
	get_max(List, Maxpath, Maxlen, 0),											% choise max way from the list
	Path = Maxpath, 															% remember MaxPath
	Length is Maxlen + 1.												% and remember MaxLen (+1 to take into account the starting cell)