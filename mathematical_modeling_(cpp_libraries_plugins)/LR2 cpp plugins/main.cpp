#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>
#include <dlfcn.h>    // динамическая загрузка и выгрузка библиотек (dlopen, dlclose)
#include <filesystem> // поиск плагинов в директории

#include "base.h"

typedef  interfaceBasePlugin* typeRegisterPlugin();

void setMethodSettingsFromConfigFile(struct Constants &c) {
    std::ifstream configFile;
    configFile.open("../config.txt");
    if(!configFile.is_open())
        std::cerr << "ERROR: Opening config file" << std::endl;
    else {
        std::string line;
        getline(configFile, line);
        if(line == "hyun")
            c.method = "hyun";
        getline(configFile, line);
        c.gammaBounds[0] = atof(line.c_str());  // left gamma bound
        getline(configFile, line);
        c.gammaBounds[1] = atof(line.c_str());  // right gamma bound
        getline(configFile, line);
        c.gammaStep = atof(line.c_str());
    }
}

int loadPlugins(std::vector<interfaceSolverPlugin *> &solverPlugins, std::vector<interfaceShowResultsPlugin *> &showPlugins) {
    interfaceSolverPlugin *solver = nullptr;
    interfaceShowResultsPlugin *show = nullptr;
    std::string pathToPlgFolder = "../plugins/";
    std::string extension = ".so";
    std::cout << "---------------------------" << std::endl;
    std::cout << "Доступны следующие плагины:" << std::endl;
    std::cout << "---------------------------" << std:: endl;
    try {
        for (auto &p : std::filesystem::directory_iterator(pathToPlgFolder)) // Для всех файлов в папке
        {
            if (!std::filesystem::is_regular_file(p.status()))
                continue; // пропускаем, если это не простой файл, а папка или что-то другое
            std::string plgFileName(p.path().filename());
            // проверяем, что имя заканчивается нужным расширением
            bool match = !plgFileName.compare(plgFileName.size() - extension.size(), extension.size(), extension);
            if (!match)
                continue;

            // открываем файл плагина
            auto dynamicLib = dlopen(p.path().c_str(), RTLD_LAZY);
            if(!dynamicLib){
                std::cout << dlerror() << std::endl;
                return 1;
            }
            typeRegisterPlugin* regPlg = (typeRegisterPlugin*)(dlsym(dynamicLib, "registerPlugin"));
            if(!regPlg){
                std::cout << dlerror() << std::endl;
                return 1;
            }
            interfaceBasePlugin *plg = regPlg();
            if(plg->isInstanceOf("interfaceSolverPlugin")) {
                std::cout << "плагин решатель: " << plg->getPluginName(plgName) << std::endl;
                solver = dynamic_cast<interfaceSolverPlugin *>(plg);
                solverPlugins.push_back(solver);
            }
            if(plg->isInstanceOf("interfaceShowResultsPlugin")) {
                std::cout << "плагин вывода: " << plg->getPluginName(plgName) << std::endl;
                show = dynamic_cast<interfaceShowResultsPlugin *>(plg);
                showPlugins.push_back(show);
            }
        }
    } catch (std::exception &e) {
        std::cout << "Error: " << e.what() << '\n';
    }
    std::cout << "---------------------------" << std:: endl;
    if (!showPlugins.size()) {
        std::cout << "Плагины вывода не загружены!" << std::endl;
        std::cout << "---------------------------" << std:: endl;
    }
    if (!solverPlugins.size()) {
        std::cout << "Плагины-решатели не загружены!" << std::endl;
    }
    return 0;
}
std::vector<int> selectedSolversIndexes(std::vector<interfaceSolverPlugin *> solverPlugins) {
    std::cout << std::endl << "---------------------------" << std::endl;
    std::cout << "Введите названия решателей:" << std::endl;
    std::cout << "---------------------------" << std::endl;
    std::string str = "yes";
    std::vector<int> selectedSolversId;
    while(str != "no") {
        std::cin >> str;
        for(int i = 0; i < solverPlugins.size(); i++)
            if(str == solverPlugins[i]->getPluginName(plgName)) {
                std::cout << "Выбран плагин решатель:" << solverPlugins[i]->getPluginName(plgName) << std::endl;
                selectedSolversId.push_back(i);
            }
        std::cout << "[Повторение ввода (yes/no)]:";
        std::cin >> str;
    }
    std::cout << "---------------------------" << std::endl << std::endl;
    std::cout << "Выбраны следующие решатели:" << std::endl;
    std::cout << "---------------------------" << std::endl;
    for(int i = 0; i < selectedSolversId.size(); i++)
        std::cout << solverPlugins[selectedSolversId[i]]->getPluginName(plgName) << " ";
    std::cout << std::endl;
    return selectedSolversId;
}
std::vector<int> selectedShowResultsIndexes(std::vector<interfaceShowResultsPlugin *> showPlugins) {
    std::cout << std::endl << "---------------------------" << std::endl;
    std::cout << "Введите названия плагинов вывода:" << std::endl;
    std::cout << "---------------------------" << std::endl;
    std::string str = "yes";
    std::vector<int> selectedShowResultsId;
    while(str != "no") {
        std::cin >> str;
        for(int i = 0; i < showPlugins.size(); i++)
            if(str == showPlugins[i]->getPluginName(plgName)) {
                std::cout << "Выбран плагин вывода:" << showPlugins[i]->getPluginName(plgName) << std::endl;
                selectedShowResultsId.push_back(i);
            }
        std::cout << "[Повторение ввода (yes/no)]:";
        std::cin >> str;
    }
    std::cout << "---------------------------" << std::endl << std::endl;
    std::cout << "Выбраны следующие плагины вывода:" << std::endl;
    std::cout << "---------------------------" << std::endl;
    for(int i = 0; i < selectedShowResultsId.size(); i++)
        std::cout << showPlugins[selectedShowResultsId[i]]->getPluginName(plgName) << " ";
    std::cout << std::endl << std::endl;
    return selectedShowResultsId;
}

int main() {
    std::vector<interfaceSolverPlugin *> solverPlugins;
    std::vector<interfaceShowResultsPlugin *> showPlugins;
    // загрузка плагинов из каталога
    if(loadPlugins(solverPlugins, showPlugins) == 1) { return 1; }

    // выбор плагинов из консоли
    std::vector<int> selectedSolversId = selectedSolversIndexes(solverPlugins);
    std::vector<int> selectedShowResultsId = selectedShowResultsIndexes(showPlugins);

    // загрузка настроек из конфигурационного файла
    Constants c;
    setMethodSettingsFromConfigFile(c);

    // делаю матрицу решений: для каждого метода решения своя строка
    std::vector<std::vector<double>> T;
    std::vector<double> T_string(c.NumberOfIterations);
    for(int i = 0; i < selectedSolversId.size() + 1; i++) { // + 1 (аналитическое решение)
        T.push_back(T_string);
        T[i][0] = c.T0;            // начальные условия
    }

    // запуск плагинов-решателей
    for(int i = 0; i < selectedSolversId.size(); i++)
        solverPlugins[selectedSolversId[i]]->Solver(T[i], c);
    // запуск аналитического решения
    analit(T[selectedSolversId.size()], c);

    // запоминаю названия решателей
    std::vector<std::string> solverNames;
    solverNames.reserve(selectedSolversId.size());
    for(int i : selectedSolversId)
        solverNames.push_back(solverPlugins[i]->getPluginName(plgName));
    solverNames.emplace_back("Analit");

    // запуск плагинов-вывода
    for(int i : selectedShowResultsId) {
        showPlugins[i]->PrintResults(solverNames, T, c);
        showPlugins[i]->PrintL2(T, c, solverPlugins, selectedSolversId);
    }
    return 0;
}