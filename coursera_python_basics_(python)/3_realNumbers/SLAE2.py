a = float(input())
b = float(input())
c = float(input())
d = float(input())
e = float(input())
f = float(input())

eps = 1e-6

# пробуем переставить строки
if abs(a) < eps < abs(c):
    a, c = c, a
    b, d = d, b
    e, f = f, e

if abs(a) > eps:
    b /= a
    e /= a
    a = 1

    d = d - c * b / a
    f = f - c * e / a
    c = 0

    if abs(d) < eps < abs(f):
        print(0)
    if abs(d) < eps and abs(f) < eps:
        if abs(b) < eps:
            x = e / a
            print(3, x)
        else:
            p = -a / b
            q = e / b
            print(1, p, q)
    if abs(d) > 0:
        y = f / d
        x = e / a - b / a * y
        print(2, x, y)
else:
    if abs(b) > eps:
        y = e / b
        if abs(d * y - f) < eps:
            print(4, y)
        else:
            print(0)
    if abs(b) < eps < abs(e):
        print(0)
    if abs(b) < eps and abs(e) < eps:
        if abs(d) < eps and abs(f) < eps:
            if abs(c) < eps:
                print(5)
            else:
                x = 0
                print(3, x)
        if abs(d) < eps < abs(f):
            print(0)
        if abs(d) > 0:
            y = f / d
            print(4, y)
