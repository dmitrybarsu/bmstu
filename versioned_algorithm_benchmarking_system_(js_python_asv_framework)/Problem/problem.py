"""
problem type: u - unimodal, m - multimodal
"""
import numpy as np
import matplotlib.pyplot as plt


class Ackley():
    # http://infinity77.net/global_optimization/test_functions_nd_A.html#go_benchmark.Ackley
    name = "Ackley"
    dimension = 2
    type = 'u'
    bounds = {'x1': (-30, 30), 'x2': (-20, 20)}
    f_optimal = 0
    optimal = [-1, -1]

    def value(self, x1, x2):
        firstSum = x1 ** 2 + x2 ** 2
        secondSum = np.cos(2.0 * np.pi * x1) + np.cos(2.0 * np.pi * x2)
        return 20.0 * np.exp(-0.2 * np.sqrt(firstSum / 2)) - np.exp(secondSum / 2) + 20 + np.e  # перевернул

    def plot(self):
        x1, x2 = np.mgrid[-20:20:80j, -20:20:80j]
        z = self.value(x1, x2)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.set_xlabel('x1')
        ax.set_ylabel('x2')
        ax.set_zlabel('Q')
        ax.plot_surface(x1, x2, z, cmap='viridis')
        plt.title('Ackley')
        plt.show()


# Ackley().plot()


class Alpine2():
    # http://infinity77.net/global_optimization/test_functions_nd_A.html#go_benchmark.Ackley
    name = "Alpine2"
    dimension = 2
    type = 'm'
    bounds = {'x1': (0, 10), 'x2': (0, 10)}
    optimal = [4.8, 4.8]
    f_optimal = 6.2

    def value(self, x1, x2):
        return np.sqrt(x1) * np.sin(x1) * np.sqrt(x2) * np.sin(x2)

    def plot(self):
        x1, x2 = np.mgrid[0:10:100j, 0:10:100j]
        z = self.value(x1, x2)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.set_xlabel('x1')
        ax.set_ylabel('x2')
        ax.set_zlabel('Q')
        ax.plot_surface(x1, x2, z, cmap='viridis')
        plt.title('Alpine2')
        plt.show()


# Alpine2().plot()


class Matyas():
    # http://benchmarkfcns.xyz/benchmarkfcns/matyasfcn.html
    name = "Matyas"
    dimension = 2
    type = 'm'
    bounds = {'x1': (-10, 10), 'x2': (-10, 10)}
    optimal = [0, 0]
    f_optimal = 0

    def value(self, x1, x2):
        return -(0.26 * (x1 ** 2 + x2 ** 2) - 0.48 * x1 * x2)  # перевернул

    def plot(self):
        x1, x2 = np.mgrid[-10:10:100j, -10:10:100j]
        z = self.value(x1, x2)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.set_xlabel('x1')
        ax.set_ylabel('x2')
        ax.set_zlabel('Q')
        ax.plot_surface(x1, x2, z, cmap='viridis')
        plt.title('Matyas')
        plt.show()


# Matyas().plot()


class Himmelblau():
    # http://infinity77.net/global_optimization/test_functions_nd_H.html#go_benchmark.HimmelBlau
    name = "Himmelblau"
    dimension = 2
    type = 'm'
    bounds = {'x1': (-6, 6), 'x2': (-6, 6)}
    optimal = [-3.9, -3.37]
    f_optimal = 2000

    def value(self, x1, x2):
        return -((x1 ** 2 + x2 - 11) ** 2 + (x1 + x2 ** 2 - 7) ** 2)  # перевернул

    def plot(self):
        x1, x2 = np.mgrid[-6:6:100j, -6:6:100j]
        z = self.value(x1, x2)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.set_xlabel('x1')
        ax.set_ylabel('x2')
        ax.set_zlabel('Q')
        ax.plot_surface(x1, x2, z, cmap='viridis')
        plt.title('Himmelblau')
        plt.show()


# Himmelblau().plot()


class Rastrigin():
    # http://infinity77.net/global_optimization/test_functions_nd_H.html#go_benchmark.HimmelBlau
    name = "Rastrigin"
    dimension = 2
    type = 'm'
    bounds = {'x1': (-4, 4), 'x2': (-4, 4)}
    optimal = [-3, 1]
    f_optimal = 0

    def value(self, x1, x2):
        return 20 + x1 ** 2 + 10 * np.cos(2 * np.pi * x1) + 10 * np.cos(2 * np.pi * x2)

    def plot(self):
        x1, x2 = np.mgrid[-4:4:100j, -4:4:100j]
        z = self.value(x1, x2)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.set_xlabel('x1')
        ax.set_ylabel('x2')
        ax.set_zlabel('Q')
        ax.plot_surface(x1, x2, z, cmap='viridis')
        plt.title('Rastrigin')
        plt.show()


# Rastrigin().plot()


class Branin():
    # http://infinity77.net/global_optimization/test_functions_nd_B.html#go_benchmark.Branin01
    name = "Branin"
    dimension = 2
    type = 'm'
    bounds = {'x1': (-5, 10), 'x2': (0, 15)}
    optimal = [-5, 2.8]
    f_optimal = 0.3978

    # f_optimal(-pi, 12.275) = f(pi, 2.275) = f(9.4247, 2.475) = 0.3978

    def value(self, x1, x2):
        return (x2 - (1.275 / np.pi ** 2) * x1 ** 2 + (5.0 / np.pi) * x1 - 6) ** 2 + \
               (10 - (5.0 / (4.0 * np.pi))) * np.cos(x1) + 10

    def plot(self):
        x1, x2 = np.mgrid[-5:10:100j, 0:15:100j]
        z = self.value(x1, x2)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.set_xlabel('x1')
        ax.set_ylabel('x2')
        ax.set_zlabel('Q')
        ax.plot_surface(x1, x2, z, cmap='viridis')
        plt.title('Branin')
        plt.show()


# Branin().plot()


class Keane():
    # http://infinity77.net/global_optimization/test_functions_nd_H.html#go_benchmark.HimmelBlau
    name = "Keane"
    dimension = 2
    type = 'm'
    bounds = {'x1': (-3, 3), 'x2': (-3, 3)}
    optimal = [-0.3, -1.62]
    f_optimal = 0.673668

    def value(self, x1, x2):
        den = np.sqrt(x1 ** 2 + x2 ** 2) + 0.0001
        return np.sin(x1 - x2) ** 2 * np.sin(x1 + x2) ** 2 / den

    def plot(self):
        x1, x2 = np.mgrid[-3:3:100j, -3:3:100j]
        z = self.value(x1, x2)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.set_xlabel('x1')
        ax.set_ylabel('x2')
        ax.set_zlabel('Q')
        ax.plot_surface(x1, x2, z, cmap='viridis')
        plt.title('Keane')
        plt.show()
# Keane().plot()
