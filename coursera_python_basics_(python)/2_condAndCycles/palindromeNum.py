k = int(input())
curNum = 1
palindromeCounter = 0
while curNum <= k:
    num = curNum
    invertNum = 0
    while num:
        invertNum *= 10
        invertNum += num % 10
        num //= 10
    if curNum == invertNum:
        palindromeCounter += 1
    curNum += 1
print(palindromeCounter)
