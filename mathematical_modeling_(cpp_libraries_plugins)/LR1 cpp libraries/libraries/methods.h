//
// Created by dmitry on 11.10.2020.
//

#ifndef SRC_METHODS_H
#define SRC_METHODS_H

#include <string>
#include <vector>

struct Constants {
    std::string method = "euler";
    const int NumberOfIterations =  100;    // iterating the solution ode
    const double T0 = 0.;                   // start object temperature
    const double T1 = 100.;                 // env temperature
    double gamma = 0.5;                     // aspect ratio
    double gammaBounds[2] = {0.5, 0.5};
    double gammaStep = 0.01;
    double step = 0.05;                     // time step = (t_i - t_i-1)
};

extern "C" void Euler(std::vector<double> &T, struct Constants c);
extern "C" void Hyun(std::vector<double> &T, struct Constants c);

#endif //SRC_METHODS_H
