import React from 'react';
import {connect} from 'react-redux';
class Description extends React.Component {
    render() {
        if(!this.props.testFunction){ return (<p>Выберите тестовую функцию</p>)}
        return (
            <div>
                <h2>{this.props.testFunction.function_name}</h2>
                <p>{this.props.testFunction.id}</p>
            </div>
        )
    }
}

function mapStateToProps (state) {
    return {
        testFunction: state.activeFunction
    }
}

export default connect(mapStateToProps) (Description)