import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d

lam = 1 / 40
myu = 1 / 233
a = 223 / 40


# Задание1
# Пункт1 (система без очереди)


def factorial(n):
    fact = 1
    while n > 1:
        fact *= n
        n -= 1
    return fact


def p0_1(n):
    den = 0
    for i in range(0, n + 1):
        den += a ** i / factorial(i)
    return 1 / den


def p_otk_1(n):
    return a ** n / factorial(n) * p0_1(n) * 100


def p_otk_plot_1(n):
    p_otk_array = []
    for i in range(n):
        p_otk_array.append(p_otk_1(i))
    x = np.linspace(0, n - 1, num=n)
    y = np.ones(len(x))

    for i in range(len(x)):
        if p_otk_array[i] < 1:
            print(p_otk_array[i], i)

    fig, ax = plt.subplots()
    ax.plot(x, p_otk_array, color="green", label='P_отказа(n)')
    ax.plot(x, y, '--', color="red", label='P_отказа = 1%')
    ax.legend()
    plt.xlabel("Количество операторов")
    plt.ylabel("Вероятность отказа, %")
    ax.grid()
    plt.show()


# p_otk_plot_1(16) # 12 < 1%


def mat_ojidanie_N_1(n):
    N = 0
    p0 = p0_1(n)
    for i in range(n + 1):
        N += i * a ** i / factorial(i) * p0
    return N


def N_plot_1(n):
    N = []
    x = []
    for i in range(n):
        N.append(mat_ojidanie_N_1(i))
        x.append(i)
    print(N[12])
    print(N)
    fig, ax = plt.subplots()
    ax.plot(x, N, color="orange")
    plt.xlabel("Количество операторов")
    plt.ylabel("Мат. ожидание числа занятых операторов")
    ax.grid()
    plt.show()


# N_plot_1(24)


def koef_zagruzki_1(n):
    K = []
    x = []
    for i in range(1, n):
        K.append(mat_ojidanie_N_1(i) / i)
        x.append(i)
    fig, ax = plt.subplots()
    ax.plot(x, K, color="orange")
    plt.xlabel("Количество операторов")
    plt.ylabel("Коэффициент загрузки операторов")
    ax.grid()
    plt.show()


# koef_zagruzki_1(16)

# Пункт2 (система с ограниченной очередью)
# 12 каналов < 1% отказов (без очереди)
# 1 <= n <= 12
# x = m - количество мест в очереди
# y = Pотказа, N, K, Pq, Q, Kq = Q / m +++
# на каждом графике: 12 графиков для каждого n
# a = 2

def p0_2(n, m):
    den = 0
    for i in range(n + 1):
        den += a ** i / factorial(i)
    P_n = a ** n / factorial(n)
    for i in range(1, m + 1):
        den += P_n * ((a / n) ** i)
    return 1 / den


def p_otk_2(n, m):
    return (a ** n) / factorial(n) * ((a / n) ** m) * p0_2(n, m) * 100


# print(p_otk_2(3, 2))


def p_otk_plot_2(n, m):
    p_otk_array_n = []
    p_otk_array = []
    for j in range(1, n + 1):
        for i in range(0, m + 1):
            p_otk_array.append(p_otk_2(j, i))
        p_otk_array_n.append(p_otk_array)
        p_otk_array = []
    x = np.linspace(0, m, num=m + 1)
    # print(p_otk_array_n)
    fig, ax = plt.subplots()
    for i in range(len(p_otk_array_n)):
        ax.plot(x, p_otk_array_n[i], label='n = %d' % (i + 1))
    ax.legend(loc='upper right')
    plt.xlabel("Количество мест в очереди")
    plt.ylabel("Вероятность отказа, %")
    ax.grid()
    plt.show()
    '''
    p_otk_array_n = []
    p_otk_array = []
    for j in range(1, m + 1):
        for i in range(1, n + 1):
            p_otk_array.append(p_otk_2(i, j))
        p_otk_array_n.append(p_otk_array)
        p_otk_array = []
    x = np.linspace(1, n, num=n)
    print(x)
    fig, ax = plt.subplots()
    for i in range(len(p_otk_array_n)):
        ax.plot(x, p_otk_array_n[i], label='m = %d' % (i + 1))
    ax.legend(loc='upper right')
    plt.xlabel("Количество операторов")
    plt.ylabel("Вероятность отказа, %")
    ax.grid()
    plt.show()
    '''


# p_otk_plot_2(12, 12)


def mat_ojidanie_N_2(n, m):
    N = 0
    p0 = p0_2(n, m)
    for i in range(n + 1):
        N += i * a ** i / factorial(i) * p0
    p_n = a ** n / factorial(n) * p0
    for i in range(1, m + 1):
        N += n * p_n * ((a / n) ** i)
    return N


# print(mat_ojidanie_N_2(3, 2))


def N_plot_2(n, m):
    '''
    N_n = []
    N = []
    for j in range(1, n + 1):
        for i in range(0, m + 1):
            N.append(mat_ojidanie_N_2(j, i))
        N_n.append(N)
        N = []
    x = np.linspace(0, m, num=m + 1)
    fig, ax = plt.subplots()
    for i in range(len(N_n)):
        ax.plot(x, N_n[i], label='n = %d' % (i + 1))
    ax.legend(loc='lower right')
    plt.xlabel("Количество мест в очереди")
    plt.ylabel("Мат. ожидание числа занятых операторов")
    ax.grid()
    plt.show()
    '''
    N_n = []
    N = []
    for j in range(1, m + 1):
        for i in range(1, n + 1):
            N.append(mat_ojidanie_N_2(i, j))
        N_n.append(N)
        N = []
    x = np.linspace(1, n, num=n)
    print(x)
    fig, ax = plt.subplots()
    for i in range(len(N_n)):
        ax.plot(x, N_n[i], label='m = %d' % (i + 1))
    ax.legend(loc='lower right')
    plt.xlabel("Количество операторов")
    plt.ylabel("Мат. ожидание числа занятых операторов")
    ax.grid()
    plt.show()


# N_plot_2(12, 12)

def koef_zagruzki_2(n, m):
    K_n = []
    K = []
    for j in range(1, n + 1):
        for i in range(0, m + 1):
            K.append(mat_ojidanie_N_2(j, i) / j)
        K_n.append(K)
        K = []
    x = np.linspace(0, m, num=m + 1)
    fig, ax = plt.subplots()
    for i in range(len(K_n)):
        ax.plot(x, K_n[i], label='n = %d' % (i + 1))
    ax.legend(loc='lower right')
    plt.xlabel("Количество мест в очереди")
    plt.ylabel("Коэффициент загрузки операторов")
    ax.grid()
    plt.show()
    '''
    K_n = []
    K = []
    for j in range(1, m + 1):
        for i in range(1, n + 1):
            K.append(mat_ojidanie_N_2(i, j) / i)
        K_n.append(K)
        K = []
    x = np.linspace(1, n, num=n)
    fig, ax = plt.subplots()
    for i in range(len(K_n)):
        ax.plot(x, K_n[i], label='m = %d' % (i + 1))
    ax.legend(loc='upper right')
    plt.xlabel("Количество операторов")
    plt.ylabel("Коэффициент загрузки операторов")
    ax.grid()
    plt.show()
    '''


# koef_zagruzki_2(12, 12)


def P_ocheredi_2(n, m):
    P_bez_ocheredi = p0_2(n, m)
    for i in range(1, n + 1):
        P_bez_ocheredi += a ** i / factorial(i) * p0_2(n, m)
    return (1 - P_bez_ocheredi) * 100


# print(P_ocheredi_2(3, 2))


def P_ocheredi_plot_2(n, m):
    P_ocheredi_n = []
    P_ocheredi = []
    for j in range(1, n + 1):
        for i in range(0, m + 1):
            P_ocheredi.append(P_ocheredi_2(j, i))
        P_ocheredi_n.append(P_ocheredi)
        P_ocheredi = []
    x = np.linspace(0, m, num=m + 1)
    print(x)
    fig, ax = plt.subplots()
    for i in range(len(P_ocheredi_n)):
        ax.plot(x, P_ocheredi_n[i], label='n = %d' % (i + 1))
    ax.legend(loc='upper right')
    plt.xlabel("Количество мест в очереди")
    plt.ylabel("Вероятность существования очереди, %")
    ax.grid()
    plt.show()
    '''
    P_ocheredi_n = []
    P_ocheredi = [100]
    for j in range(1, m + 1):
        for i in range(1, n + 1):
            P_ocheredi.append(P_ocheredi_2(i, j) * 100)
        P_ocheredi_n.append(P_ocheredi)
        P_ocheredi = [100]
    x = np.linspace(0, n, num=n + 1)
    fig, ax = plt.subplots()
    for i in range(len(P_ocheredi_n)):
        ax.plot(x, P_ocheredi_n[i], label='m = %d' % (i + 1))
    ax.legend(loc='upper right')
    plt.xlabel("Количество операторов")
    plt.ylabel("Вероятность существования очереди, %")
    ax.grid()
    plt.show()
    '''


# P_ocheredi_plot_2(12, 12)

def Q_ocheredi_2(n, m):
    Q = 0
    p_n = a ** n / factorial(n) * p0_2(n, m)
    for i in range(1, m + 1):
        Q += i * p_n * (a / n) ** i
    return Q


# print(Q_ocheredi_2(3, 2))


def Q_ocheredi_plot_2(n, m):
    Q_ocheredi_n = []
    Q_ocheredi = []
    for j in range(1, n + 1):
        for i in range(0, m + 1):
            Q_ocheredi.append(Q_ocheredi_2(j, i))
        Q_ocheredi_n.append(Q_ocheredi)
        Q_ocheredi = []
    x = np.linspace(0, m, num=m + 1)
    fig, ax = plt.subplots()
    for i in range(len(Q_ocheredi_n)):
        ax.plot(x, Q_ocheredi_n[i], label='n = %d' % (i + 1))
    ax.legend(loc='upper right')
    plt.xlabel("Количество мест в очереди")
    plt.ylabel("Мат. ожидание длины очереди")
    ax.grid()
    plt.show()
    '''
    Q_ocheredi_n = []
    Q_ocheredi = []
    for j in range(1, m + 1):
        for i in range(1, n + 1):
            Q_ocheredi.append(Q_ocheredi_2(i, j))
        Q_ocheredi_n.append(Q_ocheredi)
        Q_ocheredi = []
    x = np.linspace(1, n - 1, num=n)
    fig, ax = plt.subplots()
    for i in range(len(Q_ocheredi_n)):
        ax.plot(x, Q_ocheredi_n[i], label='m = %d' % (i + 1))
    ax.legend(loc='upper right')
    plt.xlabel("Количество операторов")
    plt.ylabel("Мат. ожидание длины очереди")
    ax.grid()
    plt.show()
    '''


# Q_ocheredi_plot_2(12, 12)


def koef_zanyatosti_v_ocheredi_2(n, m):
    Q = 0
    p_n = a ** n / factorial(n) * p0_2(n, m)
    for i in range(1, m + 1):
        Q += i * p_n * (a / n) ** i
    return Q / m


# print(koef_zanyatosti_v_ocheredi_2(3, 2))


def koef_zanyatosti_v_ocheredi_plot_2(n, m):
    koef_zanyatosti_v_ocheredi_n = []
    koef_zanyatosti_v_ocheredi = []
    for j in range(1, n + 1):
        for i in range(1, m + 1):
            koef_zanyatosti_v_ocheredi.append(koef_zanyatosti_v_ocheredi_2(j, i))
        koef_zanyatosti_v_ocheredi_n.append(koef_zanyatosti_v_ocheredi)
        koef_zanyatosti_v_ocheredi = []
    x = np.linspace(1, m, num=m)
    fig, ax = plt.subplots()
    for i in range(len(koef_zanyatosti_v_ocheredi_n)):
        ax.plot(x, koef_zanyatosti_v_ocheredi_n[i], label='n = %d' % (i + 1))
    ax.legend(loc='upper right')
    plt.xlabel("Количество мест в очереди")
    plt.ylabel("Коэффициент занятости очереди")
    ax.grid()
    plt.show()
    '''
    koef_zanyatosti_v_ocheredi_n = []
    koef_zanyatosti_v_ocheredi = []
    for j in range(1, m + 1):
        for i in range(1, n + 1):
            koef_zanyatosti_v_ocheredi.append(koef_zanyatosti_v_ocheredi_2(i, j))
        koef_zanyatosti_v_ocheredi_n.append(koef_zanyatosti_v_ocheredi)
        koef_zanyatosti_v_ocheredi = []
    x = np.linspace(1, n - 1, num=n)
    fig, ax = plt.subplots()
    for i in range(len(koef_zanyatosti_v_ocheredi_n)):
        ax.plot(x, koef_zanyatosti_v_ocheredi_n[i], label='m = %d' % (i + 1))
    ax.legend(loc='upper right')
    plt.xlabel("Количество операторов")
    plt.ylabel("Коэффициент занятости очереди")
    ax.grid()
    plt.show()
    '''


# koef_zanyatosti_v_ocheredi_plot_2(12, 12)


# Пункт3 система без ограничений на длину очереди
# 1 <= n <= 12
# x = n кол-во операторов
# y = N, K, Pq, Q

# a = 2
# m = 100

def p0_3(n):
    den = 0
    for i in range(n + 1):
        den += a ** i / factorial(i)
    P_n = a ** n / factorial(n)
    den1 = den
    if a < n:
        den += a / (n - a) * P_n
    else:
        for i in range(m + 1):
            den += P_n * ((a / n) ** i)
    return 1 / den


# print(p0_3(5, 99))
# print(p0_3(5, 100))
# print(p0_3(5, 99) - p0_3(5, 100))


def mat_ojidanie_N_3(n):
    N = 0
    for i in range(n + 1):
        N += i * a ** i / factorial(i) * p0_3(n)
    P_n = a ** n / factorial(n) * p0_3(n)
    if a < n:
        N += n * P_n * a / (n - a)
    else:
        for i in range(1, m + 1):
            N += n * P_n * ((a / n) ** i)
    return N


def N_plot_3(n):
    N = []
    x = []
    for i in range(1, n + 1):
        N.append(mat_ojidanie_N_3(i))
        x.append(i)
    '''
    y = interp1d(x, N, kind='quadratic')
    xnew = np.linspace(1, n, num=4 * (n))
    Nnew = y(xnew)
    Nnew[22] = 5.57177536
    Nnew[23] = 5.57177536
    Nnew[24] = 5.57177536
    Nnew[25] = 5.57177536
    print(Nnew)
    '''
    fig, ax = plt.subplots()
    # ax.plot(xnew, Nnew, color="orange")
    ax.plot(x, N, color="green")
    plt.xlabel("Количество операторов")
    plt.ylabel("Мат. ожидание числа занятых операторов")
    ax.grid()
    plt.show()


# N_plot_3(12)


def koef_zagruzki_3(n):
    K = []
    x = []
    for i in range(1, n + 1):
        K.append(mat_ojidanie_N_3(i) / i)
        x.append(i)
    y = interp1d(x, K, kind='quadratic')
    xnew = np.linspace(1, n, num=5 * (n + 1))
    Knew = y(xnew)
    for i in range(len(xnew)):
        if Knew[i] > 1:
            Knew[i] = 1
    fig, ax = plt.subplots()
    ax.plot(xnew, Knew, color="orange")
    # ax.plot(x, K, color="green")
    plt.xlabel("Количество операторов")
    plt.ylabel("Коэффициент загрузки операторов")
    ax.grid()
    plt.show()


# koef_zagruzki_3(12)


def P_ocheredi_3(n):
    P_n = a ** n / factorial(n) * p0_3(n)
    P_ocheredi = 0
    if a < n:
        P_ocheredi = P_n * a / (n - a)
    else:
        P_bez_ocheredi = p0_3(n)
        for i in range(1, n + 1):
            P_bez_ocheredi += a ** i / factorial(i) * p0_3(n)
        P_ocheredi = 1 - P_bez_ocheredi
    return P_ocheredi * 100


# print(P_ocheredi_3(3), 8 / 27) #a = 2

def P_ocheredi_plot_3(n):
    P_ocheredi = []
    x = []
    for i in range(1, n + 1):
        P_ocheredi.append(P_ocheredi_3(i))
        x.append(i)
    y = interp1d(x, P_ocheredi, kind='quadratic')
    xnew = np.linspace(1, n, num=4 * (n + 1))
    P_ocheredi_new = y(xnew)
    for i in range(19):
        P_ocheredi_new[i] = 100
    print(P_ocheredi_new)
    fig, ax = plt.subplots()
    ax.plot(xnew, P_ocheredi_new, color="orange")
    # ax.plot(x, P_ocheredi, color="orange")
    plt.xlabel("Количество операторов")
    plt.ylabel("Вероятность существования очереди, %")
    ax.grid()
    plt.show()


# P_ocheredi_plot_3(12)


def Q_ocheredi_3(n):
    P_n = a ** n / factorial(n) * p0_3(n)
    b = a / n
    if a < n:
        Q_ocheredi = P_n * b / ((1 - b) ** 2)
    else:
        Q_ocheredi = 0
        for i in range(1, m + 1):
            Q_ocheredi += i * P_n * (a / n) ** i
    return Q_ocheredi


# print(Q_ocheredi_3(3), 8/9) #a = 2


def Q_ocheredi_plot_3(n):
    Q_ocheredi = []
    for i in range(1, n + 1):
        Q_ocheredi.append(Q_ocheredi_3(i))
    x = np.linspace(1, n, num=n)
    y = interp1d(x, Q_ocheredi, kind='linear')
    xnew = np.linspace(1, n, num=3 * (n + 1))
    Q_ocheredi_new = y(xnew)
    fig, ax = plt.subplots()
    # ax.plot(xnew, Q_ocheredi_new, color="orange")
    ax.plot(x, Q_ocheredi, color="orange")
    plt.xlabel("Количество операторов")
    plt.ylabel("Мат. ожидание длины очереди")
    ax.grid()
    plt.show()


# Q_ocheredi_plot_3(12)


# Пункт 4 система без ограничений на длину очереди, с уходом клиентов
Tw = 523
nyu = 1 / Tw
# x = n     1 <= n <= 12
# y = N, K, Pq, Q
'''
lam = 4
myu = 1
nyu = 1
a = 4
m = 3
'''
m = 14


def p0_4(n):
    den = 0
    for i in range(n + 1):
        den += a ** i / factorial(i)
    for i in range(1, m + 1):
        prod = 1
        for j in range(1, i + 1):
            prod *= n * myu + j * nyu
        den += a ** n / factorial(n) * (lam ** i) / prod
    return 1 / den


# print(p0_4(12, 99))
# print(p0_4(12, 10))
# print(p0_4(12, 13) - p0_4(12,14))
# print(p0_4(3), 90 / (2240 + 2130))


def mat_ojidanie_N_4(n):
    N = 0
    for i in range(n + 1):
        N += i * a ** i / factorial(i) * p0_4(n)
    P_n = a ** n / factorial(n) * p0_4(n)
    for i in range(1, m + 1):
        prod = 1
        for j in range(1, i + 1):
            prod *= n * myu + j * nyu
        N += n * P_n * lam ** i / prod
    return N


# print(mat_ojidanie_N_4(3))

def N_plot_4(n):
    N = []
    x = []
    for i in range(n + 1):
        N.append(mat_ojidanie_N_4(i))
        x.append(i)
    fig, ax = plt.subplots()
    ax.plot(x, N, color="orange")
    plt.xlabel("Количество операторов")
    plt.ylabel("Мат. ожидание числа занятых операторов")
    ax.grid()
    plt.show()


N_plot_4(12)


def koef_zagruzki_4(n):
    K = []
    x = []
    for i in range(1, n + 1):
        K.append(mat_ojidanie_N_4(i) / i)
        x.append(i)
    fig, ax = plt.subplots()
    ax.plot(x, K, color="orange")
    plt.xlabel("Количество операторов")
    plt.ylabel("Коэффициент загрузки операторов")
    ax.grid()
    plt.show()


# koef_zagruzki_4(12)


def P_ocheredi_4(n):
    P_ocheredi = 0
    P_n = a ** n / factorial(n) * p0_4(n)
    for i in range(1, m + 1):
        prod = 1
        for j in range(1, i + 1):
            prod *= n * myu + j * nyu
        P_ocheredi += P_n * lam ** i / prod
    return P_ocheredi * 100


# print(P_ocheredi_4(3))


def P_ocheredi_plot_4(n):
    P_ocheredi = []
    x = []
    for i in range(1, n + 1):
        P_ocheredi.append(P_ocheredi_4(i))
        x.append(i)
    fig, ax = plt.subplots()
    ax.plot(x, P_ocheredi, color="orange")
    plt.xlabel("Количество операторов")
    plt.ylabel("Вероятность существования очереди, %")
    ax.grid()
    plt.show()


# P_ocheredi_plot_4(12)


def Q_ocheredi_4(n):
    Q_ocheredi = 0
    P_n = a ** n / factorial(n) * p0_4(n)
    for i in range(1, m + 1):
        prod = 1
        for j in range(1, i + 1):
            prod *= n * myu + j * nyu
        Q_ocheredi += i * P_n * lam ** i / prod
    return Q_ocheredi


# print(Q_ocheredi_4(3))


def Q_ocheredi_plot_4(n):
    Q_ocheredi = []
    for i in range(1, n + 1):
        Q_ocheredi.append(Q_ocheredi_4(i))
    x = np.linspace(1, n, num=n)
    y = interp1d(x, Q_ocheredi, kind='linear')
    xnew = np.linspace(1, n, num=3 * (n + 1))
    Q_ocheredi_new = y(xnew)
    fig, ax = plt.subplots()
    # ax.plot(xnew, Q_ocheredi_new, color="orange")
    ax.plot(x, Q_ocheredi, color="orange")
    plt.xlabel("Количество операторов")
    plt.ylabel("Мат. ожидание длины очереди")
    ax.grid()
    plt.show()


# Q_ocheredi_plot_4(12)

'''
Задание2
x = n число наладчиков (операторов)
y = 1)кол-во простаивающих станков+++
    2)мат. ожидание числа станков, ожидающих обслуживания
    3)вероятность ожидания обслуживания
    4)мат. ожидание числа занятых наладчиков
    5)коэффициент занятости наладчиков+++

'''

lam = 1 / 290
myu = 1 / 10
a = 10 / 290
N = 39
'''
lam = 1
myu = 1
a = 1
N = 6
'''


def p0_5(n):
    p_pred = 1
    den = 1
    for i in range(1, n + 1):
        den += (N - i + 1) * a / i * p_pred
        p_pred *= (N - i + 1) * a / i
    for i in range(n + 1, N + 1):
        den += (N - i + 1) * a / n * p_pred
        p_pred *= (N - i + 1) * a / n
    return 1 / den


# print(p0_5(2), 2 / 329)


def p_i_5(index, n):
    p_pred = 1
    if index <= n:
        for i in range(1, index + 1):
            p_pred *= (N - i + 1) * a / i
    else:
        for i in range(1, n + 1):
            p_pred *= (N - i + 1) * a / i
        for i in range(n + 1, index + 1):
            p_pred *= (N - i + 1) * a / n
    return p_pred * p0_5(n)


# print(p_i_5(6, 3), 22.5 * 2 / 329)

print(a)


def N_prostaiv_5(n):
    N_prostaiv = 0
    N_prostaiv_n = []
    x = []
    for j in range(1, n + 1):
        for i in range(N + 1):
            N_prostaiv += i * p_i_5(i, j)
        N_prostaiv_n.append(N_prostaiv)
        x.append(j)
        N_prostaiv = 0
    fig, ax = plt.subplots()
    ax.plot(x, N_prostaiv_n, color="orange")
    plt.xlabel("Количество наладчиков")
    plt.ylabel("Мат. ожидание числа простаивающих станков")
    ax.grid()
    plt.show()


# N_prostaiv_5(12)


def N_wait_service_5(n):
    N_wait = 0
    N_wait_n = []
    x = []
    for j in range(1, n + 1):
        koef = 1
        for i in range(j + 1, N + 1):
            N_wait += koef * p_i_5(i, j)
            koef += 1
        N_wait_n.append(N_wait)
        x.append(j)
        N_wait = 0
    fig, ax = plt.subplots()
    ax.plot(x, N_wait_n, color="orange")
    plt.xlabel("Количество наладчиков")
    plt.ylabel("Мат. ожидание числа станков, ожидающих обслуживания")
    ax.grid()
    plt.show()


# N_wait_service_5(12)


def P_wait_5(n):
    P_wait = 0
    P_wait_n = []
    x = []
    for j in range(1, n + 1):
        for i in range(j + 1, N + 1):
            P_wait += p_i_5(i, j) * 100
        P_wait_n.append(P_wait)
        x.append(j)
        P_wait = 0
    fig, ax = plt.subplots()
    ax.plot(x, P_wait_n, color="orange")
    plt.xlabel("Количество наладчиков")
    plt.ylabel("Вероятность ожидания обслуживания, %")
    ax.grid()
    plt.show()


# P_wait_5(12)


def mat_ojidanie_n_5(n):
    mat_ojidanie_n = 0
    mat_ojidanie_n_n = []
    x = []
    for j in range(1, n + 1):
        koef = 1
        for i in range(1, j + 1):
            mat_ojidanie_n += koef * p_i_5(i, j)
            koef += 1
        koef = j
        for i in range(j + 1, N + 1):
            mat_ojidanie_n += koef * p_i_5(i, j)
        mat_ojidanie_n_n.append(mat_ojidanie_n)
        x.append(j)
        mat_ojidanie_n = 0
    fig, ax = plt.subplots()
    ax.plot(x, mat_ojidanie_n_n, color="orange")
    plt.xlabel("Количество наладчиков")
    plt.ylabel("Мат. ожидание числа занятых наладчиков")
    ax.grid()
    plt.show()


# mat_ojidanie_n_5(12)


def koef_zanyat_5(n):
    K = 0
    K_n = []
    x = []
    for j in range(1, n + 1):
        for i in range(j + 1):
            K += i * p_i_5(i, j)
        for i in range(j + 1, N + 1):
            K += j * p_i_5(i, j)
        K_n.append(K / j)
        x.append(j)
        K = 0
    fig, ax = plt.subplots()
    ax.plot(x, K_n, color="orange")
    plt.xlabel("Количество наладчиков")
    plt.ylabel("Коэффициент занятости наладчиков")
    ax.grid()
    plt.show()
# koef_zanyat_5(12)
