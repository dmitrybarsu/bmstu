import numpy as np
import sympy as sp
import matplotlib.pyplot as plt
import matplotlib
x = sp.Symbol('x')
font = {'size': 14}
matplotlib.rc('font', **font)


def f1(x):
    return np.abs(x)


def f2(x):
    return x * np.cos(x ** 2) + np.exp(x) * np.cos(np.exp(x))


def fft_coeff(y_nodes):
    a_k = [0 for i in range(len(y_nodes))]
    for k in range(len(y_nodes)):
        A_k = fft_rec(y_nodes, k)
        a_k[k] = A_k * (-1) ** k / len(y_nodes)
    return a_k


def fft_rec(y_nodes, k):
    if len(y_nodes) == 2:
        return (-1) ** k * y_nodes[1] + y_nodes[0]
    E_k = fft_rec(y_nodes[::2], k)
    O_k = fft_rec(y_nodes[1::2], k)
    m = int((len(y_nodes) / 2))
    A_k = E_k + np.exp(-1j * k * np.pi / m) * O_k
    return A_k


def spectral_integral(func, N):
    if N & (N - 1):  # проверка того, что n - степень двойки
        print("The number of nodes is not equal to the power of two\n")
        return -1
    m = N // 2
    x_nodes = [-np.pi + i * 2 * np.pi / N for i in range(N)]
    y_nodes = [func(x) for x in x_nodes]
    a = fft_coeff(y_nodes)
    nodes_num = 300
    x = np.linspace(-np.pi, np.pi, nodes_num)
    trig_func = np.zeros((nodes_num,))
    for i in range(nodes_num):
        for k in range(-m, m):
            trig_func[i] += a[k].real * np.cos(k * x[i]) - a[k].imag * np.sin(k * x[i])
    fig, ax = plt.subplots(figsize=(12, 8))
    ax.grid()
    # ax.set_title("N = {}".format(N))
    y = func(x)
    ax.plot(x, y, color="orange")
    ax.plot(x, trig_func, linestyle="-.", color="purple")
    ax.scatter(x_nodes, y_nodes)
    plt.xlabel("Количество узлов равно{}".format(N))
    s = 0
    for k in range(1, N):
        s += a[k].real / k * np.sin(k * np.pi / 4) - a[k].imag / k * (np.cos(k * np.pi / 4) - np.cos(k * np.pi))
    return 2 * s + 5 / 4 * np.pi * a[0].real


def main():
    f1r = sp.Abs(x)
    int1 = sp.integrate(f1r, (x, -np.pi / 4, np.pi))
    f2r = x * sp.cos(x ** 2) + sp.exp(x) * sp.cos(sp.exp(x))
    int2 = sp.integrate(f2r, (x, -np.pi / 4, np.pi))
    upper_limit = 8
    rel_err = np.zeros((2, upper_limit - 1))
    for power in range(1, upper_limit):
        res1 = spectral_integral(f1, 2 ** power)
        res2 = spectral_integral(f2, 2 ** power)
        rel_err[0][power - 1] = np.abs((res1 - int1) / int1)
        rel_err[1][power - 1] = np.abs((res2 - int2) / int2)

    N = np.linspace(1, upper_limit - 1, upper_limit - 1)
    coeff = np.polyfit(N, rel_err[0], 6)
    line = coeff[6] + coeff[5] * N + coeff[4] * N ** 2 + coeff[3] * N ** 3 + coeff[2] * N ** 4 + coeff[1] * N ** 5 + \
           coeff[0] * N ** 6

    fig, ax = plt.subplots(figsize=(12, 8))
    ax.grid()
    plt.xticks(N, 2 ** N)
    ax.plot(N, line, color="orange", label="Относительная погрешность интегрирования f1(x)=|x|")
    ax.scatter(N, rel_err[0], color="blue")
    ax.scatter(N, rel_err[1], color="blue")
    ax.plot(N, rel_err[1], color="purple", label="Относительная погрешность интегрирования f2(x)=x * cos(x ** 2) + e ** x * cos(e ** x)")
    plt.xlabel('Количество узлов')
    plt.ylabel('Относительная погрешность')
    ax.set_yscale('log')
    ax.legend(loc='best')
    plt.show()


main()