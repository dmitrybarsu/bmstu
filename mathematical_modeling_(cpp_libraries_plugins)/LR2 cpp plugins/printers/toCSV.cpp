//
// Created by dmitry on 07.10.2020.
//

#include <vector>
#include <iostream>
#include <fstream>
#include "base.h"

class ResultToCSV : public interfaceShowResultsPlugin {
public:
    virtual std::string getPluginName(std::string objectName)const {
        objectName = "ResultToCSV";
        return objectName;
    }
    virtual void PrintResults(const std::vector<std::string>& solverNames, const std::vector<std::vector<double>>& T,const struct Constants& c) {
        std::ofstream resultFile;
        resultFile.open("../results/results.csv");
        if(!resultFile.is_open())
            std::cerr << "ERROR: Opening csv file" << std::endl;

        for(int i = 0; i < T.size(); i++) {
            resultFile << solverNames[i] << ";";
            for(int j = 0; j < T[0].size(); j++)
                resultFile << T[i][j] << ";";
            resultFile << std::endl;
        }
        resultFile << "t" << ";";
        for(int i = 0; i < c.NumberOfIterations; i++)
            resultFile << i << ';';
        resultFile << std::endl;
        resultFile.close();
    };
    virtual void PrintL2(std::vector<std::vector<double>>& T, struct Constants& c, const std::vector<interfaceSolverPlugin *>& solverPlugins, std::vector<int>& selectedSolversId) {
        // открываю файл
        std::ofstream resultFile;
        resultFile.open("../results/L2.csv");
        if(!resultFile.is_open())
            std::cerr << "ERROR: Opening csv file" << std::endl;

        // запоминаю названия решателей
        std::vector<std::string> solverNames;
        solverNames.reserve(selectedSolversId.size());
        for(int i : selectedSolversId)
            solverNames.push_back(solverPlugins[i]->getPluginName(plgName));
        solverNames.emplace_back("Step");

        //печатаю названия решателей в файл
        resultFile << solverNames[0];
        for(int i = 1; i < solverNames.size(); i++)
            resultFile << ";" << solverNames[i];
        resultFile << std::endl;

        // записываю в файл L2 ошибку для каждого метода и для каждого значения шага
        double defaultStep = c.step;
        while(c.step < defaultStep * c.NumberOfIterations) {
            // шаг фиксирован, запускаем решение каждым методом
            analit(T[selectedSolversId.size()], c);
            for(int j = 0; j < selectedSolversId.size(); j++) {
                solverPlugins[selectedSolversId[j]]->Solver(T[j], c);
                resultFile << L2(T[j], T[selectedSolversId.size()]) << ';';
            }
            resultFile << c.step << std::endl;
            c.step += defaultStep;
        }
        c.step = defaultStep;
        resultFile.close();
    }
    ~ResultToCSV() override {};
};

extern "C" interfaceBasePlugin* registerPlugin()
{
    return new ResultToCSV();
}