//добавил флаг Cmake -DCMAKE_CUDA_COMPILER=/usr/local/cuda/bin/nvcc
//добавил флаг nvcc main.cu -arch=sm_37 && ./a.out
//для построения графика нагрева пластины нужно установить gnuplot
#include <iostream>
#include <random>
#include <vector>
using namespace std;

#include "math_model.hpp"
#include "plotter.hpp"

const double EPS = 1e-3;

void printSLAE2(double **matrix, double *vector, int size) {
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++)
            cout << matrix[i][j]<< " ";
        cout << "= " << vector[i];
        cout << endl;
    }
    cout << endl;
}
void printVectorX(double *vector, int size) {
    for(int i = 0; i < size; i++)
        cout <<"x" << i + 1 << " = " << vector[i] << " ";
    cout << endl << endl;
}

__global__ void findNextX(double *nextX, const double *curX, const double *B, const double *d, int size) {
    unsigned int tid = threadIdx.x + blockIdx.x * blockDim.x;
    double sum = 0;
    if(tid < size){
        for(int i = 0; i < size; i++)
            sum += B[(tid * size) + i] * curX[i];
        nextX[tid] = sum + d[tid];
    }
}

float JacobiMethod(double **A, double *b, int size, int parallel, int numberOfGridsAndBlocks, double** result) {
    auto *B = new double[size * size];
    auto *d = new double[size];
    auto *curX = new double[size];  // x_i
    auto *nextX = new double[size]; // x_i+1
    //cout << "Parallel algorithm" << endl;
    //first step: Ax = b to x = Bx + d
    //printSLAE2(A, b, size);
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++) {
            /*B[i][j] = - A[i][j] / A[i][i];*/
            B[i * size + j] = - A[i][j] / A[i][i];
        }
        B[i * size + i] = 0;
        //second step: x0 = d - initial approximation
        curX[i] = d[i] = b[i] / A[i][i];
    }
    //third step: find ||B|| = max sum of elements in string B
    double maxSum = 0., curMaxSum = 0.;
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++) {
            curMaxSum += (B[i * size + j] > 0) ? B[i * size + j] : - B[i * size + j];
        }
        maxSum = (curMaxSum > maxSum) ? curMaxSum : maxSum;
        curMaxSum = 0;
    }
    //cout << "||B|| = " << maxSum << endl << endl;

    //forth step: stop criterion selection
    double eps = 1e-3;
    if(0 < maxSum && maxSum < 0.5) { // ||xi+1 - xi|| < EPS
        eps = EPS;
    } else if( 0.5 <= maxSum && maxSum < 1) { // ||xi+1 - xi|| < (1 - ||B||) / ||B|| * EPS
        eps = (1. - maxSum) / maxSum * EPS;
    } else if(maxSum > 1){
        cout << "No convergence" << endl;
        return 0;
    }
    //else
    //    cout << "SLAE contains null strings" << endl;
    //cout << "stop criterion eps = " << eps << endl << endl;

    //fifth step: iterAlgo
    double maxDiff = 2e-3;
    unsigned int numberOfIteration = 0;

    double *devCurX;
    double *devNextX;
    double *devd;
    double *devB;
    //Выделяем память для векторов на видеокарте
    cudaMalloc((void**)&devCurX, sizeof(double) * size);
    cudaMalloc((void**)&devNextX, sizeof(double) * size);
    cudaMalloc((void**)&devd, sizeof(double) * size);
    cudaMalloc((void**)&devB, sizeof(double) * size * size);

    cudaEvent_t start, stop;
    float GPUTime = 0.;
    float iterTime = 0.;

    while(maxDiff > eps) {  // stop criterion check
        if(parallel) {
            //Копируем данные в память видеокарты
            cudaMemcpy(devCurX, curX, sizeof(double) * size, cudaMemcpyHostToDevice);
            cudaMemcpy(devd, d, sizeof(double) * size, cudaMemcpyHostToDevice);
            cudaMemcpy(devB, B, sizeof(double) * size * size, cudaMemcpyHostToDevice);

            cudaEventCreate(&start);
            cudaEventCreate(&stop);
            cudaEventRecord(start,nullptr);

            // kernel<<<NumberOfGrids, NumberOfBlocks>>>
            findNextX<<<size / numberOfGridsAndBlocks + 1, numberOfGridsAndBlocks>>>(devNextX, devCurX, devB, devd, size);

            cudaEventRecord(stop, nullptr);
            cudaEventSynchronize(stop);
            cudaEventElapsedTime(&iterTime, start, stop);
            cudaEventDestroy(start);
            cudaEventDestroy(stop);

            //Получаем результат расчета
            cudaMemcpy(nextX, devNextX, sizeof(double) * size, cudaMemcpyDeviceToHost);
        }
        else {
            cudaEventCreate(&start);
            cudaEventCreate(&stop);
            cudaEventRecord(start,nullptr);

            for(int i = 0; i < size; i++) {
                nextX[i] += d[i];
                for (int j = 0; j < size; j++)
                    nextX[i] += B[i * size + j] * curX[j];
            }

            cudaEventRecord(stop, nullptr);
            cudaEventSynchronize(stop);
            cudaEventElapsedTime(&iterTime, start, stop);
            cudaEventDestroy(start);
            cudaEventDestroy(stop);
        }
        GPUTime += iterTime;

        maxDiff = eps; // reset old value
        for(int i = 0; i < size; i++) {
            double diff = (nextX[i] - curX[i] > 0) ? nextX[i] - curX[i] : curX[i] - nextX[i]; // diff > 0
            maxDiff = (diff > maxDiff) ? diff : maxDiff;
        }
        for(int i = 0; i < size; i++) {
            curX[i] = nextX[i];
            nextX[i] = 0.;  // reset old value
        }
        numberOfIteration++;
    }

    cudaFree(devCurX);
    cudaFree(devNextX);
    cudaFree(devd);
    cudaFree(devB);
/*
    cout << "Solution found at iteration " << numberOfIteration << endl;
    printVectorX(curX, size);
    for(int i = 0; i < size; i++)
        (*result)[i] = curX[i];
    cout << "max(||xi+1 - xi||) = " << maxDiff << endl << endl;*/

    return GPUTime;
}

void runSolver(int SIZE, int numberOfGridsAndBlocks) {
    //generate SLAE
    MathModel get_lae(SIZE);
    auto slae = get_lae();
    auto matrixVector = slae.first;
    auto vectorVector = slae.second;
    // vector to double
    auto vector = vectorVector.data();
    int size = matrixVector.size();
    auto matrix = new double *[size];
    for (int i = 0; i < size; i++)
        matrix[i] = matrixVector[i].data();

    //solve SLAE
    auto *result = new double[size];
    double seqTime = JacobiMethod(matrix, vector, size, 0, numberOfGridsAndBlocks, &result);
    double parallelTime = JacobiMethod(matrix, vector, size, 1, numberOfGridsAndBlocks, &result);

    //gnuplot result SLAE
    std::vector<double> resultV;
    resultV.reserve(size);
    for(int i = 0; i < size; i++)
        resultV.push_back(result[i]);
    auto vectorPtr = std::make_shared<std::vector <double>>(resultV);
    Plotter plt(SIZE);
    //plt.Plot(vectorPtr);

    //free memory
    delete [] matrix;
    delete[] result;

    //print time results
    cout << "Sequential time = " << seqTime << endl;
    cout << "Parallel time   = " << parallelTime << endl;
}

__host__ int main() {
    // Варьируем размерность
    const int NumOfGrids = 8;
    const int maxSize = 28;  //16
    for(int i = 4; i < maxSize; i += 2) {
        std::cout << "Количество узлов на верхней части пластины: " << i << std::endl;
        runSolver(i, NumOfGrids);
    }

    //Варьируем количество блоков
    const int MaxNumOfGrids = 10;
    const int size = 16;
    for(int i = 1; i < MaxNumOfGrids; i++) {
        std::cout << "Количество блоков: " << i << std::endl;
        runSolver(size, i);
    }

    return 0;
}