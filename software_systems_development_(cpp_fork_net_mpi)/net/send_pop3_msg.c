// Usage:
// $ ./pop3_client [host] [username] [password]

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <memory.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <string.h>

#define ERR_ALLOC -1
#define ERR_SEND_RECV -1
#define ERR_ARGS -2
#define ERR_CON -3
#define ERR_SCAN -4

#define RECV_BUF_SIZE 4096
#define SEND_BUF_SZ 100
#define CLIENT_PORT 1234
#define SERVER_PORT 110

int transact (int sock, const char* mes, const int mes_size) {
	if(send (sock, mes, strlen(mes), MSG_NOSIGNAL) == ERR_SEND_RECV) {
		return ERR_SEND_RECV;
	}
	char* buf = (char*)calloc(1, sizeof(char) * mes_size);
	if (!buf) {
		return ERR_ALLOC;
	}
	int recv_len = recv (sock, buf, mes_size, 0);
	if (recv_len == ERR_SEND_RECV) {
		return ERR_SEND_RECV;
	}
	buf[recv_len] = '\0';
	printf ("< %s\n", buf);
	free(buf);
	return 0;
}

int transact_email (int sock, const char* mes, const int mes_size) {
	if(send (sock, mes, strlen(mes), MSG_NOSIGNAL) == ERR_SEND_RECV) {
		return ERR_SEND_RECV;
	}
	char* buf = (char*)calloc(1, sizeof(char) * mes_size);
	if (!buf) {
		return ERR_ALLOC;
	}
	int total_recv = 0;
	int recv_len = 0;
	int mes_len = mes_size;
	while(mes_len > 0 && (recv_len = recv (sock, buf, mes_size, 0)) > 0) {
		total_recv += recv_len;
		mes_len -= recv_len;
		printf ("%s\n", buf);
	}
	if (recv_len == ERR_SEND_RECV) {
		return ERR_SEND_RECV;
	}
	buf[total_recv] = '\0';
	free(buf);
	return 0;
}

int parse_str_for_int(char* str, const int spare_num) {
	char* start = str;
	for(int i = 0; i < spare_num; ++i) {
		start = strchr(start, ' ');
		while(*start == ' ') {
			++start;
		}
	}
	
	char str_num[80];
	strcpy(str_num, start);
	int id = atoi(str_num);
	return id;
}

int get_email(int sock, char* mes) {
	int mes_id = parse_str_for_int(mes, 1);
	char list_mes[80];
	memset(list_mes, '\0', 80);
	snprintf(list_mes, sizeof(list_mes) / sizeof(char), "LIST %d\n", mes_id);
	int len = strlen(list_mes);
	list_mes[len+1] = '\0';
	if(send (sock, list_mes, strlen(list_mes), MSG_NOSIGNAL) == ERR_SEND_RECV) {
		return ERR_SEND_RECV;
	}
	char buf[RECV_BUF_SIZE];
	int recv_len = recv (sock, buf, RECV_BUF_SIZE, 0);
	if (recv_len == ERR_SEND_RECV) {
		return ERR_SEND_RECV;
	}
	buf[recv_len] = '\0';
	puts(buf);
	int mes_size = parse_str_for_int(buf, 2);
	printf("got size %d\n", mes_size);
	return transact_email(sock, mes, mes_size);
}

int establish_connection (int *sock, char *host, char *user, char *pass) {
	int rez;
	char buf[RECV_BUF_SIZE];

	struct hostent *hp;
	struct sockaddr_in clnt_sin;
	struct sockaddr_in srv_sin;

	*sock = socket(AF_INET, SOCK_STREAM, 0);
	memset((char *)&clnt_sin, '\0', sizeof(clnt_sin));
	clnt_sin.sin_family = AF_INET;
	clnt_sin.sin_addr.s_addr = INADDR_ANY;
	clnt_sin.sin_port = CLIENT_PORT;
	bind(*sock, (struct sockaddr *)&clnt_sin, sizeof(clnt_sin));
	memset ((char *)&srv_sin, '\0', sizeof(srv_sin));
	srv_sin.sin_family = AF_INET;
	hp = gethostbyname (host);
	memcpy ((char *)&srv_sin.sin_addr, hp->h_addr, hp->h_length);
	srv_sin.sin_port = htons(SERVER_PORT);

	fprintf (stderr, "Connecting to \'%s\'\n", host);
	rez = connect (*sock, (struct sockaddr *)&srv_sin, sizeof(srv_sin));
	fprintf (stderr, "%d: %s\n", rez, strerror(errno));

	rez = recv (*sock, buf, RECV_BUF_SIZE, 0);
	buf[rez] = '\0';
	fprintf (stderr, "< %s", buf);

	sprintf (buf, "USER %s\n", user);
	fprintf (stderr, "> %s", buf);
	transact (*sock, buf, RECV_BUF_SIZE);
	sprintf (buf, "PASS %s\n", pass);
	fprintf (stderr, "> %s", buf);
	transact (*sock, buf, RECV_BUF_SIZE);
}

int main (int argc, char** argv) {
	if (argc != 4) {
		fprintf(stderr, "Usage: ./a.out <host> <user> <password>\n");
		return ERR_ARGS;
	}

	int sock = 0;
	int err = establish_connection (&sock, argv[1], argv[2], argv[3]);
	if (err != 0) {
		return ERR_CON;
	}

	while (true) {
		printf("> ");
		char buf[SEND_BUF_SZ];
		memset(buf, '\0', sizeof(buf));
		read(0, buf, SEND_BUF_SZ);
		int len = strlen(buf);
		buf[len+1] = '\0';
		if (!strncmp(buf, "RETR", 4)) {
			get_email(sock, buf);
		} else {
			transact (sock, buf, RECV_BUF_SIZE);
		}
		if (!strcmp(buf, "QUIT\n")) {
			break;
		}
	}
	close(sock);
	return 0;
}
