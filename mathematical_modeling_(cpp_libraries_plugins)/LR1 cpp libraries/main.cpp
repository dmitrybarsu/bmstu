//
// Created by dmitry on 07.10.2020.
//
//Вариант №1 (Метод Эйлера, Метод Хьюна)
//TODO: загружать библиотеку после выполнения условия
//TODO: сделать функцию выгрузки графиков gnuplot
//TODO: написать отчет

#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>

// Динамическая загрузка и выгрузка библиотек
#include <dlfcn.h>   // dlopen, dlclose

#include "methods.h"  // методы решения ОДУ

typedef void (* method) (std::vector<double> &T, struct Constants c);

typedef void* handle_t;
typedef void* sym_t;

handle_t load_lib(const std::string& name);
sym_t load_symbol(handle_t handle, const std::string& name);
const char* get_error_str();
int unload_lib(handle_t handle);

handle_t load_lib(const std::string& name) {
    return dlopen((name + ".so").c_str(), RTLD_LAZY);
}

sym_t load_symbol(handle_t handle, const std::string& name) {
    return dlsym(handle, name.c_str());
}

const char* get_error_str() {
    return dlerror();
}

int unload_lib(handle_t handle) {
    return dlclose(handle);
}


void Analyt(std::vector<double> &T, struct Constants c) {
    for(int i = 1; i < T.size(); i++)
        T[i] = c.T1 + (c.T0 - c.T1) * exp(-c.gamma * (i * c.step)); // t = i * c.step
}

double L2(std::vector<double> &MethodT, std::vector<double> &AnalytT) {
    double xPow2Sum = 0.;
    for(int i = 0; i < MethodT.size(); i++) {
        AnalytT[i] = AnalytT[i] ? AnalytT[i] : 1e-15;  // avoid division by zero
        xPow2Sum += pow((MethodT[i] - AnalytT[i]) / AnalytT[i], 2);
    }
    return sqrt(xPow2Sum / MethodT.size());
}

void writeSolutionsToCSV(std::vector<double> &EulerT, std::vector<double> &HyunT, std::vector<double> &AnalytT, struct Constants c) {
    std::ofstream resultFile;
    resultFile.open("../results/results.csv");
    if(!resultFile.is_open())
        std::cerr << "ERROR: Opening csv file" << std::endl;

    resultFile << "EulerT, HyunT, AnalytT, t" << std::endl;
    for(int i = 0; i < c.NumberOfIterations; i++)
        resultFile << EulerT[i] << ';';
    resultFile << std::endl;
    for(int i = 0; i < c.NumberOfIterations; i++)
        resultFile << HyunT[i] << ';';
    resultFile << std::endl;
    for(int i = 0; i < c.NumberOfIterations; i++)
        resultFile << AnalytT[i] << ';';
    resultFile << std::endl;
    for(int i = 0; i < c.NumberOfIterations; i++)
        resultFile << c.step * i << ';';
    resultFile << std::endl;
    resultFile.close();
}
void writeL2ToCSV(std::vector<double> &EulerT, std::vector<double> &HyunT, std::vector<double> &AnalytT, struct Constants c,
        method euler, method hyun) {
    std::ofstream L2File;
    L2File.open("../results/L2.csv");
    if(!L2File.is_open())
        std::cerr << "ERROR: Opening csv file" << std::endl;

    L2File << "EulerL2, HyunL2, t" << std::endl;
    for(int i = 1; c.step < 5; i++) {   // find min step for each method
        c.step = 0.05 * i;
        euler(EulerT, c);
        hyun(HyunT, c);
        Analyt(AnalytT, c);
        L2File << L2(EulerT, AnalytT) << ';';
    }
    c.step = 0.05;
    L2File << std::endl;
    for(int i = 1; c.step < 5; i++) {   // find min step for each method
        c.step = 0.05 * i;
        euler(EulerT, c);
        hyun(HyunT, c);
        Analyt(AnalytT, c);
        L2File << L2(HyunT, AnalytT) << ';';
    }
    c.step = 0.05;
    L2File << std::endl;
    for(int i = 1; c.step < 5; i++) {   // find min step for each method
        c.step = 0.05 * i;
        euler(EulerT, c);
        hyun(HyunT, c);
        Analyt(AnalytT, c);
        L2File << c.step << ";";
    }
    L2File.close();
}

void setMethodSettingsFromConfigFile(struct Constants &c) {
    std::ifstream configFile;
    configFile.open("../config.txt");
    if(!configFile.is_open())
        std::cerr << "ERROR: Opening config file" << std::endl;
    else { // TODO: сделать проверки ввода + убрать atof или парсить config.json (#include "rapidjson")
        std::string line;
        getline(configFile, line);
        if(line == "hyun")
            c.method = "hyun";
        getline(configFile, line);
        c.gammaBounds[0] = atof(line.c_str());  // left gamma bound
        getline(configFile, line);
        c.gammaBounds[1] = atof(line.c_str());  // right gamma bound
        getline(configFile, line);
        c.gammaStep = atof(line.c_str());
    }
}

int main() {
    auto handle_euler = dlopen("libraries/libeuler.so", RTLD_LAZY);
    if(!handle_euler){
        std::cout << dlerror() << std::endl;
        return 1;
    }
    auto name1 = "Euler";
    auto euler = (method) dlsym(handle_euler, name1);
    if(!euler){
        std::cout << dlerror() << std::endl;
        return 1;
    }

    auto handle_hyun = dlopen("libraries/libhyun.so", RTLD_LAZY);
    if(!handle_hyun){
        std::cout << dlerror() << std::endl;
        return 1;
    }
    auto name2 = "Hyun";
    auto hyun = (method) dlsym(handle_hyun, name2);
    if(!hyun){
        std::cout << dlerror() << std::endl;
        return 1;
    }

    Constants c;
    setMethodSettingsFromConfigFile(c);
    std::vector<double> T(c.NumberOfIterations);
    T[0] = c.T0;
    double gammaDefault = c.gamma; // запомнил значение gamma по умолчанию
    //Выбираем метод решения ОДУ и варьируем параметр gamma
    if(c.method == "euler") {
        c.gamma = c.gammaBounds[0];             // начинаем с левой границы интервала значений gamma
        while(c.gamma <= c.gammaBounds[1]) {    // и до правой границы итервала gamma
            euler(T, c);
            c.gamma += c.gammaStep;             // с шагом gammaStep
        }
    }
    else if(c.method == "hyun") {
        c.gamma = c.gammaBounds[0];             // начинаем с левой границы интервала значений gamma
        while(c.gamma <= c.gammaBounds[1]) {    // и до правой границы итервала gamma
            hyun(T, c);
            c.gamma += c.gammaStep;             // с шагом gammaStep
        }
    }

    c.gamma = gammaDefault;
    std::vector<double> EulerT(c.NumberOfIterations);  // temperature T(t)
    EulerT[0] = c.T0;                                  // start condition
    euler(EulerT, c);

    std::vector<double> HyunT(c.NumberOfIterations);
    HyunT[0] = c.T0;
    hyun(HyunT, c);

    std::vector<double> AnalytT(c.NumberOfIterations);
    AnalytT[0] = c.T0;
    Analyt(AnalytT, c);

    writeSolutionsToCSV(EulerT, HyunT, AnalytT, c);

    writeL2ToCSV(EulerT, HyunT, AnalytT, c, euler, hyun);

    return 0;
}