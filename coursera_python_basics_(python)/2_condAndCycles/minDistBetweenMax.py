next = int(input())
prev = next
cur = next
counter = 0
minDist = 10 ** 6
flagStart = 0
while next != 0:
    if flagStart:
        counter += 1
    if cur > prev and cur > next:
        if minDist > counter and flagStart:
            minDist = counter
        flagStart = 1
        counter = 0
    prev = cur
    cur = next
    next = int(input())
if minDist == 10 ** 6:
    minDist = 0
print(minDist)
