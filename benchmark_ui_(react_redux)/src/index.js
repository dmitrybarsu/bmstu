import React from 'react';
import {render} from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {createStore} from 'redux';
import {rootReducer} from './reducers/rootReducer';
import {Provider} from 'react-redux'; //чтобы подписаться и диспатчить
//Provider внешний блок над всем приложением. блок <App /> уже внутри
//так мы связываем react и reducers

const store = createStore(rootReducer);

const app = (
    <Provider store={store}>
        <App />
    </Provider>
);
render(app, document.getElementById('root'));
serviceWorker.unregister();
