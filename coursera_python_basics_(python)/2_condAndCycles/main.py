# тип BOOL - ссылается на один из двух объектов в памяти
# true или false.
# Переменные bool - неизменяемы
# Строки сравниваются в лексикографическом порядке
# (как упорядоченные слова в словаре)

# Приоритеты от большего к меньшему
# 1)арифм операции
# 2)сравнения(=,!=,...)
# 3)not
# 4)and
# 5)or

# num = int(input())
# isEven = num % 2 == 0
# print(isEven)

# Задача: два события.
# пересекаются ли события во времени

# start1 = int(input())
# end1 = int(input())
# start2 = int(input())
# end2 = int(input())

# isS1in2 = start2 <= start1 <= end2
# isE1in2 = start2 <= end1 <= end2
# isS2in1 = start1 <= start2 <= end1
# isE2in1 = start1 <= end2 <= end1
# answer = isS1in2 or isE1in2 or isS2in1 or isE2in1

# answer = start1 <= end2 and start2 <= end1
# print(answer)

# Задача посчитать модуль числа
# если первое условие не выполнилось,
# дальше не проверяем (ошибки не будет)
# c "или" также, если одно истинно,
# дальше не смотрим
# x = int(input())
# if x < 0 and x // 0 == 0:
#     x = -x
# print(x)

# x = int(input())
# if x >= 0:
#     print(x)
# else:
#     print(-x)

# Задача: определить, в какой координатной
# четверти находится точка
# x = int(input())
# y = int(input())
#
# if x > 0:
#     if y > 0:
#         print(1)
#     else:
#         print(4)
# else:
#     if y > 0:
#         print(2)
#     else:
#         print(3)

# Хороший тест заходит в каждую ветвь программы!!!

# Задача: по числу выводить его название
# на английском 1, 2, 3
# x = int(input())
# if x == 1:
#     print("One")
# elif x == 2:
#     print("Two")
# elif x == 3:
#     print("Three")
# else:
#     print("Other")

# ЦИКЛ WHILE

# i = 1
# while i <= 100:
#     print(i, end=" ")
#     i += 1
# print("!")

# В python можно после цикла while поставить else
# блок else выполнится после нормального завершения
# цикла (без break)

# now = int(input())
# maxNum = now
# while now != 0:
#     now = int(input())
#     if now > maxNum and now != 0:
#         maxNum = now
# else:
#     print(maxNum)

# сумма чисел последовательности
# cur = int(input())
# sumSeq = cur
# while cur != 0:
#     cur = int(input())
#     sumSeq += cur
# print(sumSeq)

# вывести только положительные числа
# cur = int(input())
# while cur != 0:
#     if cur > 0:
#         print(cur)
#     cur = int(input())

# cur = -1
# while cur != 0:
#     cur = int(input())
#     if cur <= 0:
#         continue
#     print(cur)
