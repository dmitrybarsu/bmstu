//
// Created by sergey on 20.05.19.
//

#include <cmath>

#include "math_model.hpp"

MathModel::MathModel(int num_y): num_x_(num_y * 2 - 1), num_y_(num_y),
					delta_x_(x_len_ / (num_y * 2 - 2)),
					delta_y_(y_len_ / (num_y - 1)){}

int MathModel::getPlaceInSolutionMatrix(int y, int x) {
	return static_cast<int>(static_cast<double>(num_x_ + num_x_ - y + 1) / 2.0
			* static_cast<double>(y) + static_cast<double>(x));
}

std::pair<std::vector<std::vector<double>>, std::vector<double>> MathModel::operator()() {
	double square_delta_x_reverse = 1 / (delta_x_ * delta_x_);
	double square_delta_y_reverse = 1 / (delta_y_ * delta_y_);
	int equations_num = static_cast<int>(static_cast<double>(num_x_+ num_y_) / 2.0 * static_cast<double>(num_y_));
	std::vector<std::vector<double >> matr(equations_num);
	std::vector<double> free_vec(equations_num);
	for (int i = 0; i < equations_num; ++i) {
		std::vector<double> line(equations_num);
		matr[i] = line;
	}
	// inner nodes filling
	for (int i = 1; i < num_y_- 1; ++i) {
		for (int j = 1; j < num_x_ - i - 1; ++j) {
			std::vector<double> coeffs(equations_num);
			int point_num = getPlaceInSolutionMatrix(i, j);
			int point_num_y_shift_up = getPlaceInSolutionMatrix(i + 1, j);
			int point_num_x_shift_up = getPlaceInSolutionMatrix(i, j + 1);
			int point_num_y_shift_down = getPlaceInSolutionMatrix(i - 1, j);
			int point_num_x_shift_down = getPlaceInSolutionMatrix(i, j - 1);
			coeffs[point_num_x_shift_up] = square_delta_x_reverse;
			coeffs[point_num] = -2 * square_delta_x_reverse + -2 * square_delta_y_reverse;
			coeffs[point_num_x_shift_down] = square_delta_x_reverse;
			coeffs[point_num_y_shift_down] = square_delta_y_reverse;
			coeffs[point_num_y_shift_up] = square_delta_y_reverse;
			matr[point_num] = coeffs;
			free_vec[point_num] = 0;
		}
	}

	// left border
	for (int i = 0; i < num_y_; ++i) {
		int pos = getPlaceInSolutionMatrix(i, 0);
		matr[pos][pos] = 1;
		free_vec[pos] = left_cond_;
	}

	// down_border
	for (int i = 1; i < num_x_; ++i) {
		int pos = getPlaceInSolutionMatrix(0, i);
		matr[pos][pos] = 1;
		free_vec[pos] = down_cond_;
	}

	//top_border
	for (int i = 1; i < num_y_; ++i) {
		int pos = getPlaceInSolutionMatrix(num_y_ - 1, i);
		matr[pos][pos] = 1;
		free_vec[pos] = top_cond_;
	}

	// right_border
	const double sqrt2 = sqrt(2);
	for (int i = 1; i < num_y_- 1; ++i) {
		int pos = getPlaceInSolutionMatrix(i, num_x_ - i - 1);
		int left_down_pos = getPlaceInSolutionMatrix(i - 1, num_x_ - i - 2);
		matr[pos][pos] = -1 / (delta_x_ * sqrt2);
		matr[pos][left_down_pos] = - matr[pos][pos];
		free_vec[pos] = right_cond_;
	}

	return {matr, free_vec};
	/*syst = std::make_shared<GaussLAESystem>(std::move(matr), std::move(free_vec));
	return syst;*/
}
