#include <iostream>
#include <queue>
#define V 4

using namespace std;

//TODO: понять, как совершается обход двудольного графа
//TODO:

// проверяем граф G[V][V] на двудольность, src - индекс вершины, с которой начинаем обход
bool isBipartite(int G[][V], int src) {
    // -1 не посетили вершину
    // 1 вершина одной доли графа
    // 0 вершина другой доли графа
    // изначально помечаем все вершины, как непосещенные
    int colorArr[V];
    for (int i = 0; i < V; ++i)
        colorArr[i] = -1;

    // начальную вершину отнесем к первой доле
    colorArr[src] = 1;

    // создаем FIFO очередь и заносим в нее начальную вершину
    queue <int> q;
    q.push(src);

    // пока в очереди есть вершины
    while (!q.empty()){
        // сохраняем самую левую вершину и удаляем ее из очереди
        int u = q.front();
        q.pop();

        /*// если есть переходы вершин в самих себя, значит это цикл и граф недвудольный
        if (G[u][u] == 1)
            return false;*/

        // идем по всем вершинам, находим инцидентные вершине u
        for (int v = 0; v < V; ++v) {
            // если есть ребро между u и v && вершина v непосещена
            if (G[u][v] && colorArr[v] == -1) {
                // назначим ей цвет не такой же как у u (если u = 0, v = 1 - 0 = 1, иначе v = 1 - 1 = 0)
                colorArr[v] = 1 - colorArr[u];
                // добавляем инцидентную вершину (v) в очередь
                q.push(v);
            }
            // если ребро есть, но цвета вершин совпали, значит граф не двудольный
            /*else if (G[u][v] && colorArr[v] == colorArr[u])
                return false;*/
        }
    }
    // здесь мы обошли все вершины и отнесли их к одной из долей, граф - двудольный
    return true;
}

void petriNet(std::vector<std::vector<int>>& PosToTransfer,std::vector<std::vector<int>>& TransferToPos, std::vector<int>& chipsNumOnPos) {
    // test pass all trnsfers
    std::vector<bool> alive;
    // k - max chips in one poistion
    int k = 3;
    // not more then 1 in one time
    //common chips num
    int commonChipsNum = 0;
    for(int chipsNumOnPo : chipsNumOnPos)
        commonChipsNum += chipsNumOnPo;
    // markers
    std::vector<std::vector<int>> markers(T_MAX, std::vector<int>(chipsNumOnPos.size(), 0));
    // start marker
    for(int i = 0; i < chipsNumOnPos.size(); i++)
        markers[0][i] = chipsNumOnPos[i];
    std::cout << "Marker : ";
    for(int i = 0; i < chipsNumOnPos.size(); i++)
        std::cout << "P" << i + 1 << " = " << chipsNumOnPos[i] << ' ';
    std::cout << std::endl << std::endl;

    //TODO: симуляция прохода фишек по модели: надо пройти по всем переходам и узнать сработает ли он
    int posNum = PosToTransfer.size();
    int transfersNum = PosToTransfer[0].size();
    std::vector<bool> activeTransfers(transfersNum, false);

    for(int t = 0; t < T_MAX; t++) {
        std::cout << "Traversal №" << t << ":" << std::endl;
        for(int j = 0; j < transfersNum; j++) {
            // проверяем сработает ли текущий переход
            bool transferFlag = true;
            for(int i = 0; i < posNum; i++) {
                // если есть входящее ребро и число фишек на позиции перед входом < мин необх числа => переход закрыт
                if (PosToTransfer[i][j] != 0 && chipsNumOnPos[i] < PosToTransfer[i][j])
                    transferFlag = false;
                //std::cout << PosToTransfer[i][j] << std::endl;
            }
            // если переход может сработать, то выполняем этот переход: перебрасываем фишки через переход
            if (transferFlag) {
                // убираем фишки до перехода
                for(int m = 0; m < posNum; m++) {
                    if (PosToTransfer[m][j] != 0) {
                        chipsNumOnPos[m] -= PosToTransfer[m][j];
                    }
                }
                // добавляем фишки после перехода
                for(int m = 0; m < posNum; m++) {
                    if (TransferToPos[j][m] > 0) {
                        chipsNumOnPos[m]++;
                    }
                }
                std::cout << "T" << j + 1 << " worked." << std::endl;
                std::cout << "Marker : ";
                for(int m = 0; m < chipsNumOnPos.size(); m++)
                    std::cout << "P" << m + 1 << " = " << chipsNumOnPos[m] << ' ';
                std::cout << std::endl;
            }
        }

        std::cout << std::endl;
    }
}


int main() {
    // задали связи вершин графа (матрица смежности двудольного графа)
    // гор - переходы
    // вер - позиции
    // ребро из позиции в переход -> смотрим
    int G[][V] = {
            {0, 1, 0, 1},
            {1, 0, 1, 0},
            {0, 1, 0, 1},
            {1, 0, 1, 0}
    };
    // передали связи графа и вершину для начала обхода
    std::cout << isBipartite(G, 0);
    return 0;
}