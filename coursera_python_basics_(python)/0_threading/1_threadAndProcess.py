'''
GIL - нельзя использовать несколько процессоров одноврменно
1)процессы - процессоры (у меня 8 процессоров)
2)потоки - ядра (у меня 4 ядра)
внутри одного процесса может выполняться несколько потоков
3)блокировка - когда один поток ждет другого или разделяемая память
4)безопансое хранилище threading.local()
(делаем данные локальными для каждого потока,
глобальные данные не меняются)
Что такое IO поток и CPU поток?
'''
import time
from threading import Thread
from multiprocessing import Pool

COUNT = 50000000


def countdown(n):
    while n > 0:
        n -= 1


start = time.time()
countdown(COUNT)
end = time.time()
print('Последовательное решение -', end - start)

t1 = Thread(target=countdown, args=(COUNT // 2,))
t2 = Thread(target=countdown, args=(COUNT // 2,))
start = time.time()
t1.start()
t2.start()
t1.join()
t2.join()
end = time.time()
print('Решение в двух потоках -', end - start)


pool = Pool(processes=2)
start = time.time()
r1 = pool.apply_async(countdown, [COUNT // 2])
r2 = pool.apply_async(countdown, [COUNT // 2])
pool.close()
pool.join()
end = time.time()
print('Решение в двух процессах -', end - start)