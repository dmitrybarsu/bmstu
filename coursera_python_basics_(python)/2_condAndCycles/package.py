l1 = int(input())
w1 = int(input())
h1 = int(input())
l2 = int(input())
w2 = int(input())
h2 = int(input())
l = int(input())
w = int(input())
h = int(input())

answer = "NO"
# ставим коробки рядом друг с другом
if max(h1, h2) <= h:
    # 1 - 4 вращаем правую коробку вокруг левой
    # 4 - 8 вращаем контейнер
    if w1 + w2 <= w and max(l1, l2) <= l or \
            w1 + l2 <= w and max(l1, w2) <= l or \
            l1 + l2 <= l and max(w1, w2) <= w or \
            l1 + w2 <= l and max(w1, l2) <= w or \
            w1 + w2 <= l and max(l1, l2) <= w or \
            w1 + l2 <= l and max(l1, w2) <= w or \
            l1 + l2 <= w and max(w1, w2) <= l or \
            l1 + w2 <= w and max(w1, l2) <= l:
        answer = "YES"
# ставим одну коробку поверх другой
if h1 + h2 <= h:
    # проверяем поместится ли каждая коробка в контейнер
    if (l1 <= l and w1 <= w or l1 <= w and w1 <= l) and \
            (l2 <= l and w2 <= w or l2 <= w and w2 <= l):
        answer = "YES"

print(answer)
