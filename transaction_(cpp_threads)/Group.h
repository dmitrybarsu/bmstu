//
// Created by dmitry on 28.12.2020.
//

#ifndef CPP_THREADS_TRANSACTION_GROUP_H
#define CPP_THREADS_TRANSACTION_GROUP_H


class Group {
private:
    double sumSalary;
    int workersNum;
public:
    Group() { sumSalary = 0; workersNum = 0; }
    void update(double workerSalary) { sumSalary += workerSalary; workersNum++; }
    double avgSalary() const { return sumSalary / workersNum; }
};


#endif //CPP_THREADS_TRANSACTION_GROUP_H
