from typing import Optional

from fastapi import FastAPI
from pydantic import BaseModel

# from fastapi import HTTPException
# from db import PhraseInput
# from db import PhraseOutput
# from db import Database

app = FastAPI(title="Random phrase")


# db = Database()

# @app.get(
#     "/get",
#     response_description="Random phrase",
#     description="Get random phrase from database",
#     response_model=PhraseOutput,
# )
# async def get():
#     try:
#         phrase = db.get(db.get_random())
#     except IndexError:
#         raise HTTPException(404, "Phrase list is empty")
#     return phrase
#
# @app.post(
#     "/add",
#     response_description="Added phrase with *id* parameter",
#     response_model=PhraseOutput,
# )
# async def add(phrase: PhraseInput):
#     phrase_out = db.add(phrase)
#     return phrase_out
#
# @app.delete("/delete", response_description="Result of deleting")
# async def delete(id: int):
#     try:
#         db.delete(id)
#     except ValueError as e:
#         raise HTTPException(404, str(e))

class Item(BaseModel):
    name: str
    price: int
    is_offer: Optional[bool] = None


@app.get("/")
def read_root():
    return {"Hello": "World"}


# сначала пути без параметров
@app.get("/items/me")
def read_item(item_id: Optional[int], q: Optional[str] = None):
    return {"item_id": item_id, "q": q}


@app.put("/items/{item_id}")
def update_item(item_id: int, item: Item):
    return {"item_name": item.name, "item_id": item_id}
