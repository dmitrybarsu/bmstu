import subprocess
import re
import os
from datetime import datetime


def get_commits():
    lines = subprocess.check_output(['git', 'log'], stderr=subprocess.STDOUT).decode("utf-8").split('\n')
    commits = []
    current_commit = {}
    for line in lines:
        if not line.startswith(' '):
            if line.startswith('commit '):
                if current_commit:
                    current_commit['message'] = current_commit['message'][0]
                    commits.append(current_commit)
                    current_commit = {}
            else:
                try:
                    key, value = line.split(':', 1)
                    current_commit[key.lower()] = value.strip()
                except ValueError:
                    pass
        else:
            leading_4_spaces = re.compile('^    ')
            current_commit.setdefault('message', []).append(leading_4_spaces.sub('', line))
    if current_commit:
        current_commit['message'] = current_commit['message'][0]
        commits.append(current_commit)
    return commits


commits = get_commits()

if len(commits):
    with open('branch_history.md', 'w') as file:
        file.write("# Branch history\n")
        commits.sort(key=lambda c: datetime.strptime(c["date"], '%a %b %d %H:%M:%S %Y %z'), reverse=False)
        file.write('\n')
        file.write('Date | Author | Message\n')
        file.write('-------|--------|--------\n')
        for commit in commits:
            datetime_info = datetime.strptime(commit["date"], '%a %b %d %H:%M:%S %Y %z')
            author = re.split(r' <', commit['author'])[0]
            message = commit['message']
            file.write(f'{datetime_info.year}.{datetime_info.month}.{datetime_info.day} | {author} | {message}')
            file.write('\n')

    # при коммите, создаем файл branch_history.md заново
    # но в репо он не обновляется
    # если запускаю скрипт локально - все ОК
    # os.system("git config --global user.email '---'")
    # os.system("git config --global user.name '---'")
    # os.system("cat branch_history.md")
    # os.system("git status")
    # os.system("git add .")
    # os.system(f"git commit -m 'update branch_history.md {str(datetime.now())}'")
    # os.system("git push -o ci.skip https://{user.name}:{user.password}@gitlab.com/{user.name}/ignore-md-update-in-ci.git --all")