sumSeq = 0
elementsInSeq = 0
cur = int(input())
while cur != 0:
    sumSeq += cur
    elementsInSeq += 1
    cur = int(input())
if elementsInSeq == 0:
    print(0)
else:
    print(sumSeq / elementsInSeq)
