#include <cstdio>
#define SIZE 3

// Функция умножения матрицы на вектор и сложение результирующего вектора с другим вектором
__global__ void addVector(float const *vec1, float const *matrix, float *result, const int size) {
    int tid=threadIdx.x+blockIdx.x*blockDim.x;
    float sum=0;
    if(tid < size){
        for(int i=0; i < size; i++)
            sum += vec1[i]*matrix[(tid * size)+i];
        result[tid] = sum + vec1[tid];  // тут еще складываю с другим вектором + vec1[tid]
    }
}

__host__ int main()
{
    //Выделяем память на процессоре под векторы
    auto* vec1 = new float[SIZE];
    auto* vec2 = new float[SIZE];
    auto* res = new float[SIZE];
    auto* matrix = new float[SIZE * SIZE];

    //Инициализируем значения векторов
    vec1[0] = 2;
    vec1[1] = 1;
    vec1[2] = 0;
    matrix[0] = 1;
    matrix[1] = 2;
    matrix[2] = 3;
    matrix[3] = 4;
    matrix[4] = 5;
    matrix[5] = 6;
    matrix[6] = 7;
    matrix[7] = 8;
    matrix[8] = 9;

    //Указатели на память видеокарте
    float* devVec1;
    float* devVec2;
    float* devMatrix;
    float* devRes;

    //Выделяем память для векторов на видеокарте
    cudaMalloc((void**)&devVec1, sizeof(float) * SIZE);
    cudaMalloc((void**)&devVec2, sizeof(float) * SIZE);
    cudaMalloc((void**)&devMatrix, sizeof(float) * SIZE * SIZE);
    cudaMalloc((void**)&devRes, sizeof(float) * SIZE);

    //Копируем данные в память видеокарты
    cudaMemcpy(devVec1, vec1, sizeof(float) * SIZE, cudaMemcpyHostToDevice);
    cudaMemcpy(devVec2, vec2, sizeof(float) * SIZE, cudaMemcpyHostToDevice);
    cudaMemcpy(devMatrix, matrix, sizeof(float) * SIZE * SIZE, cudaMemcpyHostToDevice);

    addVector<<<10, 10>>>(devVec1, devMatrix, devRes, SIZE);

    //Обработчик события
    cudaEvent_t syncEvent;
    cudaEventCreate(&syncEvent);    //Создаем event
    cudaEventRecord(syncEvent, nullptr);  //Записываем event
    cudaEventSynchronize(syncEvent);  //Синхронизируем event

    //Только теперь получаем результат расчета
    cudaMemcpy(res, devRes, sizeof(float) * SIZE, cudaMemcpyDeviceToHost);

    //Результаты расчета
    for (int i = 0; i < SIZE; i++)
        printf("sum[%d]: %.6f\n", i , res[i]);

    cudaEventDestroy(syncEvent);

    cudaFree(devVec1);
    cudaFree(devVec2);
    cudaFree(devMatrix);
    cudaFree(devRes);

    delete[] vec1;
    delete[] vec2;
    delete[] res;
}