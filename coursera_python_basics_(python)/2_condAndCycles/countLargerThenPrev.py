counter = 0
prev = int(input())
cur = prev
while cur != 0:
    if cur > prev:
        counter += 1
    prev = cur
    cur = int(input())
print(counter)
