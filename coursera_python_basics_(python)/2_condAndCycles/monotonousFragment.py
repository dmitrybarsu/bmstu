maxFragment = 1
upFragment = 1
downFragment = 1
num = int(input())
prev = num
while num != 0:
    if num > prev:
        upFragment += 1
        if downFragment > maxFragment:
            maxFragment = downFragment
        downFragment = 1
    elif num < prev:
        downFragment += 1
        if upFragment > maxFragment:
            maxFragment = upFragment
        upFragment = 1
    else:
        if maxFragment < upFragment:
            maxFragment = upFragment
        if maxFragment < downFragment:
            maxFragment = downFragment
        upFragment = 1
        downFragment = 1
    prev = num
    num = int(input())
if maxFragment < upFragment:
    maxFragment = upFragment
if maxFragment < downFragment:
    maxFragment = downFragment
print(maxFragment)
