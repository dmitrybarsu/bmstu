n = int(input())
i = 1
sumSquare = 0
while i <= n:
    sumSquare += i ** 2
    i += 1
print(sumSquare)
