import React from 'react';
import TestFunctionsList from "./container/test_functions";
import Description from "./container/description";
import Header from "./container/header"
import Navbar from "./container/navbar";
import TestFunction from "./container/test_function";
function App() {
  return (
      <div className="field pt-2">
          <Header className="header" />
          <div className="run"><a href="#" className="btn btn-primary">Run benchmarks</a></div>
          <Navbar />
          <div className="alert alert-info" role="alert">
              You can add your own test_function here
              <p><button type="button" className="btn btn-primary">Add new test function</button></p>
          </div>
          <TestFunction />
      </div>
  );
}

export default App;
