from tkinter import *
from tkinter import ttk
import random
import numpy as np
import heapq


# TODO: написать генератор препятствий с полигонами до 8 января
# TODO: переписать код, добавить readme и добавить на git


def fill_matrix(cells_num, constraints_density, start, end):
    matrix = np.zeros((cells_num, cells_num))
    for i in range(cells_num):
        for j in range(cells_num):
            if random.random() > 1 - constraints_density:
                matrix[i][j] = 1
    matrix[start[0]][start[1]] = 2
    matrix[end[0]][end[1]] = 3

    return matrix


def draw_map(matrix, cell_length, border, field):
    colors = {0: "grey", 1: "black", 2: "#000080", 3: "red", 4: "yellow"}

    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            field.create_rectangle(
                i * cell_length + border,
                j * cell_length + border,
                i * cell_length + cell_length - 2 * border,
                j * cell_length + cell_length - 2 * border,
                fill=colors[matrix[j][i]],
                width=border
            )


def draw_graph(neighbours_dist_to_points, field, cell_length, k_neighbours):
    for i in range(len(neighbours_dist_to_points)):
        a_x = neighbours_dist_to_points[i][0][1][1] * cell_length + cell_length / 2
        a_y = neighbours_dist_to_points[i][0][1][0] * cell_length + cell_length / 2
        for j in range(1, min(len(neighbours_dist_to_points[i]), k_neighbours + 1)):
            b_x = neighbours_dist_to_points[i][j][1][1] * cell_length + cell_length / 2
            b_y = neighbours_dist_to_points[i][j][1][0] * cell_length + cell_length / 2
            l = field.create_line(a_x, a_y, b_x, b_y, width=0.08 * cell_length, fill='#FFA500')
            field.tag_raise(l)


def line_intersection(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
        return False

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div

    x1, y1 = line1[0]
    x2, y2 = line1[1]
    AB1 = np.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
    AP1 = np.sqrt((x - x1) ** 2 + (y - y1) ** 2)
    PB1 = np.sqrt((x - x2) ** 2 + (y - y2) ** 2)

    x1, y1 = line2[0]
    x2, y2 = line2[1]
    AB2 = np.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
    AP2 = np.sqrt((x - x1) ** 2 + (y - y1) ** 2)
    PB2 = np.sqrt((x - x2) ** 2 + (y - y2) ** 2)

    if abs(AB1 - AP1 - PB1) < 0.1 and abs(AB2 - AP2 - PB2) < 0.1:
        return True

    return False


def constraint_intersections(matrix, point1, point2, length, field):
    constr_coords = []
    constr_lines = []
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            if matrix[j][i] == 1:
                constr_coords.append((i, j))
                constr_lines.append([i * length, j * length, i * length + length, j * length])
                constr_lines.append([i * length, j * length, i * length, j * length + length])
                constr_lines.append([i * length + length, j * length, i * length + length, j * length + length])
                constr_lines.append([i * length, j * length + length, i * length + length, j * length + length])

    for constr_line in constr_lines:
        A = (point1[0] * length + length / 2, point1[1] * length + length / 2)
        B = (point2[0] * length + length / 2, point2[1] * length + length / 2)
        C = (constr_line[1], constr_line[0])
        D = (constr_line[3], constr_line[2])

        if line_intersection((A, B), (C, D)):
            return True

    return False


def dijkstra(adj, start, target):
    # минимальное расстояние до каждой вершины
    d = {start: 0}
    # родители каждой вершины
    parent = {start: None}
    # очередь с приоритетами
    pq = [(0, start)]
    visited = set()
    while pq:
        # расстояние до вершины и ее номер
        du, u = heapq.heappop(pq)
        if u in visited: continue
        if u == target:
            break
        visited.add(u)
        # смотрим все смежные
        for v, weight in adj[u]:
            # если такой вершины еще не было или можем дойти до нее с меньшим весом - обновим вершину
            if v not in d or d[v] > du + weight:
                d[v] = du + weight
                parent[v] = u
                # добавим ее в очередь
                heapq.heappush(pq, (d[v], v))

    way = [target]
    vertex_id = target
    while vertex_id:
        if vertex_id in parent and vertex_id:
            vertex_id = parent[vertex_id]
            way.append(vertex_id)
        else:
            vertex_id = 0

    return way


def prm(matrix, probe_points_density, k_neighbours, field, cell_length, border, start, end):
    # random points
    probe_points = []
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            if matrix[i][j] == 0 and random.random() > 1 - probe_points_density:
                matrix[i][j] = 4
                probe_points.append((i, j))

    draw_map(matrix, cell_length, border, field)

    probe_points.append(start)
    probe_points.append(end)
    # print(probe_points[-2:])

    # join k neighbors
    neighbours_dist_to_points = []
    for i in range(len(probe_points)):
        neighbours_i_dist_to_points = []
        for j in range(len(probe_points)):
            if not constraint_intersections(matrix, probe_points[i], probe_points[j], cell_length, field):
                cur_dist = abs(probe_points[i][0] - probe_points[j][0]) + abs(probe_points[i][1] - probe_points[j][1])
                neighbours_i_dist_to_points.append((cur_dist, probe_points[j]))
        neighbours_i_dist_to_points.sort(key=lambda n: n[0], reverse=False)
        neighbours_dist_to_points.append(neighbours_i_dist_to_points)
        # print(neighbours_i_dist_to_points)

    # draw lines between neighbours
    draw_graph(neighbours_dist_to_points, field, cell_length, k_neighbours)

    # find min dist in graph
    graph = {}
    point_to_coord = {}
    coord_to_point = {}
    # [(вес ребра - расстояние до смежной, координаты смежной точки)]
    for i in range(len(neighbours_dist_to_points)):
        point_to_coord[i] = neighbours_dist_to_points[i][0][1]
        coord_to_point[neighbours_dist_to_points[i][0][1]] = i

    for i in range(len(neighbours_dist_to_points)):
        adj_list = []
        for j in range(1, len(neighbours_dist_to_points[i])):
            adj_list.append((coord_to_point[neighbours_dist_to_points[i][j][1]], neighbours_dist_to_points[i][j][0]))
        graph[i] = adj_list

    for key, value in graph.items():
        print(f"{key}: {value}")

    start_vertex_id = coord_to_point[start]
    target_vertex_id = coord_to_point[end]

    optimal_way = dijkstra(graph, start_vertex_id, target_vertex_id)
    # print(optimal_way)

    # draw optimal way
    for i in range(1, len(optimal_way) - 1):
        if len(optimal_way) > 1:
            a_y, a_x = point_to_coord[optimal_way[i]]
            b_y, b_x = point_to_coord[optimal_way[i - 1]]
            a_x = a_x * cell_length + cell_length / 2
            a_y = a_y * cell_length + cell_length / 2
            b_x = b_x * cell_length + cell_length / 2
            b_y = b_y * cell_length + cell_length / 2
            field.create_line(a_x, a_y, b_x, b_y, width=0.08 * cell_length, fill='red')

    return matrix


class App:
    def __init__(self, master):
        self.master = master
        self.frame = ttk.Frame(master)
        self.frame.grid()

        # window's title
        self.master.title("Motion Planning")

        # scale window
        size = 1200
        shift_x = int(master.winfo_screenwidth() / 2 - size / 2)
        shift_y = int(master.winfo_screenheight() / 2 - size / 2)
        self.master.geometry(f"+{shift_x}+{shift_y}")

        # init_map
        k_neighbours = 4
        constraints_density = 0.1
        probe_points_density = 0.3
        cells_num = 5
        cell_length = int(size / cells_num)
        border = 0.03 * cell_length
        start = (0, cells_num - 1)
        end = (cells_num - 1, 0)

        field = Canvas(master, bg="#2E8B57", height=size, width=size, borderwidth=0, highlightthickness=0)
        field.grid()

        matrix = fill_matrix(cells_num, constraints_density, start, end)
        # draw_map(matrix, cell_length, border, field)
        prm(matrix, probe_points_density, k_neighbours, field, cell_length, border, start, end)


def main():
    root = Tk()
    App(root)
    root.mainloop()


if __name__ == '__main__':
    main()
