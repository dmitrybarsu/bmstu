//
// Created by dmitry on 07.10.2020.
//

#include "methods.h"

void Hyun(std::vector<double> &T, struct Constants c) {
    for(int i = 1; i < T.size(); i++) {
        double k1 = c.step * (-c.gamma * (T[i - 1] - c.T1));
        double k2 = c.step * (-c.gamma * (T[i - 1] + k1 - c.T1));
        T[i] = T[i - 1]  + (k1 + k2) / 2;
    }
}