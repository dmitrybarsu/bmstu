#include <cstdio>
#define SIZE 4


// Функция сложения двух векторов
__global__ void addVector(float const *left, float const *right, float *result) {
    //Получаем id текущей нити.
    unsigned int idx = threadIdx.x;

    //Расчитываем результат.
    result[idx] = left[idx] + right[idx];
}

__host__ int main()
{
    //Выделяем память на процессоре под векторы
    auto* vec1 = new float[SIZE];
    auto* vec2 = new float[SIZE];
    auto* vec3 = new float[SIZE];

    //Инициализируем значения векторов
    for (int i = 0; i < SIZE; i++){
        vec1[i] = 1;
        vec2[i] = 2;
    }

    //Указатели на память видеокарте
    float* devVec1;
    float* devVec2;
    float* devVec3;

    //Выделяем память для векторов на видеокарте
    cudaMalloc((void**)&devVec1, sizeof(float) * SIZE);
    cudaMalloc((void**)&devVec2, sizeof(float) * SIZE);
    cudaMalloc((void**)&devVec3, sizeof(float) * SIZE);

    //Копируем данные в память видеокарты
    cudaMemcpy(devVec1, vec1, sizeof(float) * SIZE, cudaMemcpyHostToDevice);
    cudaMemcpy(devVec2, vec2, sizeof(float) * SIZE, cudaMemcpyHostToDevice);

    //initVector<<<32,32>>>(devVec3);
    addVector<<<4, 4>>>(devVec1, devVec2, devVec3);

    //Ловим ошибки
    cudaError_t cudaStatus;
    cudaStatus = cudaGetLastError();

    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "Cannot launch CUDA kernel: %s.\n");
        cudaGetErrorString(cudaStatus);
        return 1;
    }
    //Обработчик события
    cudaEvent_t syncEvent;
    cudaEventCreate(&syncEvent);    //Создаем event
    cudaEventRecord(syncEvent, nullptr);  //Записываем event
    cudaEventSynchronize(syncEvent);  //Синхронизируем event

    //Только теперь получаем результат расчета
    cudaMemcpy(vec3, devVec3, sizeof(float) * SIZE, cudaMemcpyDeviceToHost);

    //Результаты расчета
    for (int i = 0; i < SIZE; i++)
        printf("sum[%d]: %.6f\n", i , vec3[i]);

    cudaEventDestroy(syncEvent);

    cudaFree(devVec1);
    cudaFree(devVec2);
    cudaFree(devVec3);

    delete[] vec1;
    delete[] vec2;
    delete[] vec3;
}