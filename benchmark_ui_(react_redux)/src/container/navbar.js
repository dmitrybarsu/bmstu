import React from 'react';
import '../css/main.css'
export default class Navbar extends React.Component {
    render() {
        return (
            <div className="menu">
                <nav className="navbar navbar-light bg-light">
                    <a className="navbar-brand" href="#">Writing benchmarks</a>
                </nav>
                <nav className="navbar navbar-light selected">
                    <a className="navbar-brand" href="#">Test functions</a>
                </nav>
                <nav className="navbar navbar-light bg-light">
                    <a className="navbar-brand" href="#">Benchmark settings</a>
                </nav>
                <nav className="navbar navbar-light bg-light">
                    <a className="navbar-brand" href="#">Select algo versions</a>
                </nav>
                <nav className="navbar navbar-light bg-light">
                    <a className="navbar-brand" href="#">Reports</a>
                </nav>
            </div>
        )
    }
}