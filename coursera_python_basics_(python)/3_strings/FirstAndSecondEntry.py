string = input()
pos1 = string.find('f')
if pos1 == -1:
    print(-2)
else:
    pos2 = string.find('f', pos1 + 1)
    if pos2 == -1:
        print(-1)
    else:
        print(pos2)
