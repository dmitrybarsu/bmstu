#include <iostream>
#include <vector>
#include <fstream>
#include <random>
#define OPENING_ERROR -1
#define T_MAX 16
#define MAX_REPEAT_MARKER 8

/*
Возможные улучшения:
TODO: учитывать кратность (несколько ребер из позиции в переход)
TODO: вместо случайного выбора перехода использовать BFS.
TODO: визуализация графа из уникальных меток.
*/

int readFormFile(std::vector<std::vector<int>>& PosToTransfer,std::vector<std::vector<int>>& TransferToPos, std::vector<int>& chipsNumOnPos) {
    std::ifstream inputFile;
    inputFile.open("../inputFile.txt");
    if(!inputFile.is_open()) {
        std::cerr << "ERROR: Opening file." << std::endl;
        return OPENING_ERROR;
    }
    std::cout << "INFO: File with links successfully opened." << std::endl;

    std::cout << "INFO: Start reading input data from file." << std::endl;
    int posNum = 0;
    int transferNum = 0;
    inputFile >> posNum >> transferNum;
    std::vector<std::vector<int>> PtoT(posNum, std::vector<int>(transferNum, 0));
    int PtoTnum = 0;
    inputFile >> PtoTnum;
    for(int i = 0; i < PtoTnum; i++) {
        int p = 0;
        int t = 0;
        inputFile >> p >> t;
        PtoT[p][t] = 1;
    }

    std::vector<std::vector<int>> TtoP(transferNum, std::vector<int>(posNum, 0));
    int TtoPnum = 0;
    inputFile >> TtoPnum;
    for(int i = 0; i < TtoPnum; i++) {
        int t = 0;
        int p = 0;
        inputFile >> t >> p;
        TtoP[t][p] = 1;
    }
    int chipsNum = 0;
    inputFile >> chipsNum;
    std::vector<int> chipsPos(posNum);
    for(int i = 0; i < posNum; i++)
        inputFile >> chipsPos[i];

    /*
    std::cout << "PtoT" << std::endl;
    for(int i = 0; i < PtoT.size(); i++) {
        for(int j = 0; j < PtoT[0].size(); j++)
            std::cout << PtoT[i][j] << ' ';
        std::cout << std::endl;
    }
    std::cout << "TtoP" << std::endl;
    for(int i = 0; i < TtoP.size(); i++) {
        for(int j = 0; j < TtoP[0].size(); j++)
            std::cout << TtoP[i][j] << ' ';
        std::cout << std::endl;
    }
    std::cout << "Chips" << std::endl;
    for(int i = 0; i < posNum; i++)
        std::cout << chipsPos[i] << std::endl;
    */

    std::cout << "INFO: File successfully read." << std::endl << std::endl;
    inputFile.close();

    PosToTransfer = PtoT;
    TransferToPos = TtoP;
    chipsNumOnPos = chipsPos;
    return 0;
}

void petriNet(std::vector<std::vector<int>>& PosToTransfer,std::vector<std::vector<int>>& TransferToPos, std::vector<int>& chipsNumOnPos) {
    // not more then 1 in one time
    //common chips num
    int startChipsNum = 0;
    for(int chipsNumOnPo : chipsNumOnPos)
        startChipsNum += chipsNumOnPo;
    // markers
    std::vector<std::vector<int>> markers(T_MAX, std::vector<int>(chipsNumOnPos.size(), 0));
    // start marker
    for(int i = 0; i < chipsNumOnPos.size(); i++)
        markers[0][i] = chipsNumOnPos[i];
    std::cout << "Marker : ";
    for(int i = 0; i < chipsNumOnPos.size(); i++)
        std::cout << "P" << i + 1 << " = " << chipsNumOnPos[i] << ' ';
    std::cout << std::endl << std::endl;
    int uniqMarkerCounter = 1;
    int repeatMarkerCounter = 0;

    int posNum = PosToTransfer.size();
    int transfersNum = PosToTransfer[0].size();

    std::vector<bool> activeTransfers(transfersNum, false);

    // k - max chips in one poistion
    int k = 3;
    bool limitation = true;

    bool safety = true;
    bool preservation = true;

    for(int t = 0; t < T_MAX && repeatMarkerCounter <= MAX_REPEAT_MARKER; t++) {
        std::vector<bool> transferFlag(transfersNum, true);
        for(int j = 0; j < transfersNum; j++) {
            // проверяем сработает ли текущий переход
            for(int i = 0; i < posNum; i++) {
                // если есть входящее ребро и число фишек на позиции перед входом < мин необх числа => переход закрыт
                if (PosToTransfer[i][j] != 0 && chipsNumOnPos[i] < PosToTransfer[i][j])
                    transferFlag[j] = false;
            }
        }
        // запишем Id всех возможных сейчас переходов
        std::vector<int> validTid;
        for(int i = 0; i < transferFlag.size(); i++)
            if(transferFlag[i])
                validTid.push_back(i);
        int validT = 0;
        // если возможно более одного перехода, выполним один случайный
        if(validTid.size() > 1) {
            std::random_device rd;
            std::mt19937 mt(rd());
            std::uniform_int_distribution<int> dist(0, validTid.size() - 1);
            validT = (int)dist(mt);
            // опускаем все флаги переходов, кроме случайно выбранного
            for(int i = 0; i < transferFlag.size(); i++)
                if(i != validTid[validT])
                    transferFlag[i] = false;
        }
        // если переход может сработать, то выполняем этот переход: перебрасываем фишки через переход
        if (transferFlag[validTid[validT]]) {
            // убираем фишки до перехода
            for(int m = 0; m < posNum; m++) {
                if (PosToTransfer[m][validTid[validT]] != 0) {
                    chipsNumOnPos[m] -= PosToTransfer[m][validTid[validT]];
                }
            }
            // добавляем фишки после перехода
            for(int m = 0; m < posNum; m++) {
                if (TransferToPos[validTid[validT]][m] > 0) {
                    chipsNumOnPos[m]++;
                }
            }
            std::cout << "T" << validTid[validT] + 1 << " worked." << std::endl;
            activeTransfers[validTid[validT]] = true;
            std::cout << "Marker : ";
            for(int m = 0; m < chipsNumOnPos.size(); m++)
                std::cout << "P" << m + 1 << " = " << chipsNumOnPos[m] << ' ';
            std::cout << std::endl;

            // если хотя бы с одним значениям по позициям не совпадает => добавим макер в уникальные
            int uniqOkCounter = 0;
            for(int h = 0; h < uniqMarkerCounter; h++) {
                for(int m = 0; m < markers[0].size(); m++) {
                    if (markers[h][m] != chipsNumOnPos[m]) {
                        uniqOkCounter++;
                        break;
                    }
                }
            }
            if(uniqOkCounter == uniqMarkerCounter) {
                for(int h = 0; h < markers[0].size(); h++)
                    markers[uniqMarkerCounter][h] = chipsNumOnPos[h];
                uniqMarkerCounter++;
            } else {
                // если текущий марке не уникален, значит инкрементируем число повторяющихся маркеров
                repeatMarkerCounter++;
            }
        }
        // test Petri net characteristics
        for(int h = 0; h < chipsNumOnPos.size(); h++)
            if(chipsNumOnPos[h] > k)
                limitation = false;
        for(int h = 0; h < chipsNumOnPos.size(); h++)
            if(chipsNumOnPos[h] > 1)
                safety = false;
        int chipsCounter = 0;
        for(int h = 0; h < chipsNumOnPos.size(); h++)
            chipsCounter += chipsNumOnPos[h];
        if(chipsCounter != startChipsNum)
            preservation = false;
    }

    std::cout << std::endl;
    std::cout << "Petri net characteristics:" << std::endl;
    int counterActiveT = 0;
    for(auto && activeTransfer : activeTransfers)
        if(activeTransfer)
            counterActiveT++;
    if (counterActiveT == activeTransfers.size())
        std::cout << "All transitions have been completed => alive Petri net (OK)" << std::endl;
    else
        std::cout << "Not all transitions have been completed => Petri net is not alive" << std::endl;

    if(limitation)
        std::cout << "During the simulation in one position there were no more than k = " << k << " chips => net limitation (OK)" << std::endl;
    else
        std::cout << "During the simulation there were more k = " << k << " chips in one position => net is not limited" << std::endl;

    if(safety)
        std::cout << "For the entire time of modeling in a particular position at a particular time there was no more than 1 chip => net safety (OK)" << std::endl;
    else
        std::cout << "For the entire time of modeling in a particular position at a particular time there was more than 1 chip => net is not safety" << std::endl;

    if(preservation)
        std::cout << "The number of chips in the network is constant => net persistent (OK)" << std::endl;
    else
        std::cout << "The number of chips in the network is not constant => net is not persistent" << std::endl;

    std::cout << std::endl;
    for(int i = 0; i < uniqMarkerCounter; i++) {
        std::cout << "uniqMarker №" << i << ':' << std::endl;
        for(int j = 0; j < markers[0].size(); j++) {
             std::cout << markers[i][j] << ' ';
        }
        std::cout << std::endl;
    }
}

int main() {
    /*// матрица ребер из позиций в переходы
    // позиции - строки, переходы столбцы
    std::vector<std::vector<int>> fromPtoT = {
          //{T1,T2,T3,T4,T5,T6,T7}
            {0, 1, 1, 0, 0, 0, 0},
            {0, 1, 0, 0, 0, 0, 0},
            {0, 0, 0, 1, 1, 0, 0},
            {0, 0, 1, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 1, 0},
            {0, 0, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 1, 0, 0, 0},
            {0, 0, 0, 0, 1, 0, 0}
    };
    // матрица ребер из переходов в позиции
    // переходы - строки, позиции столбцы
    std::vector<std::vector<int>> fromTtoP = {
          //{P1,P2,P3,P4,P5,P6,P7,P8,P9}
            {1, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 1},
            {0, 0, 1, 0, 1, 0, 0, 0, 0},
            {0, 0, 1, 0, 0, 1, 0, 0, 0},
            {0, 1, 0, 0, 0, 0, 1, 0, 0},
            {0, 0, 0, 1, 0, 0, 1, 0, 0},
    };*/
    std::vector<std::vector<int>> PosToTransfer, TransferToPos;
    std::vector<int> chipsNumOnPos;
    if(readFormFile(PosToTransfer, TransferToPos, chipsNumOnPos) == OPENING_ERROR)
        return OPENING_ERROR;

    petriNet(PosToTransfer, TransferToPos, chipsNumOnPos);

    return 0;
}
