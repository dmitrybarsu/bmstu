#include <iostream>
#include <omp.h>
#include <ctime>
#include <random>
#include <vector>
using namespace std;

#include "math_model.hpp"
#include "plotter.hpp"

#define SLAE_DIM 3
const double EPS = 1e-3;

double ** generateSymmetricalPositiveMatrix(int size) {
    //создать случайную симметричную
    //диагональные элементы больше строки или столбца
    // create matrix
    auto ** matrix = new double * [size];
    for(int i = 0; i < size; i++)
        matrix[i] = new double [size];
    //set random
    random_device random_device;
    mt19937 generator(random_device());
    uniform_int_distribution<> distribution(0, 1000);
    //fill matrix
    for(int i = 0; i < size; i++) {
        matrix[i][i] = size + distribution(generator) / 1000.; // size + 2 * random [0;1]
        for(int j = i + 1; j < size; j++) {
            matrix[i][j] = matrix[j][i] = distribution(generator) / 1000.; //random [0;1]
        }
    }
    return matrix;
}
double * generateVector(int size) {
    auto * vector = new double [size];
    //set random
    random_device random_device;
    mt19937 generator(random_device());
    uniform_int_distribution<> distribution(0, 1000);
    for(int i = 0; i < size; i++){
        vector[i] = 1e16 + distribution(generator);
        //cout << vector[i] << " ";
    }
    return vector;
}

void printSLAE2(double **matrix, double *vector, int size) {
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++)
            cout << matrix[i][j]<< " ";
        cout << "= " << vector[i];
        cout << endl;
    }
    cout << endl;
}
void printVectorX(double *vector, int size) {
    for(int i = 0; i < size; i++)
        cout <<"x" << i + 1 << " = " << vector[i] << " ";
    cout << endl << endl;
}

double JacobiMethod(double **A, double *b, int size, int parallel, double** result) {
    auto **B = new double *[size];
    for (int i = 0; i < size; i++)
        B[i] = new double[size];
    auto *d = new double[size];
    auto *curX = new double[size];  // x_i
    auto *nextX = new double[size]; // x_i+1
    cout << "Parallel algorithm" << endl;
    //first step: Ax = b to x = Bx + d
    printSLAE2(A, b, size);
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++) {
            B[i][j] = - A[i][j] / A[i][i];
        }
        B[i][i] = 0;
        //second step: x0 = d - initial approximation
        curX[i] = d[i] = b[i] / A[i][i];
    }
    //third step: find ||B|| = max sum of elements in string B
    double maxSum = 0., curMaxSum = 0.;
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size; j++) {
            curMaxSum += (B[i][j] > 0) ? B[i][j] : - B[i][j];
        }
        maxSum = (curMaxSum > maxSum) ? curMaxSum : maxSum;
        curMaxSum = 0;
    }
    cout << "||B|| = " << maxSum << endl << endl;

    //forth step: stop criterion selection
    double eps = 1e-3;
    if(0 < maxSum && maxSum < 0.5) { // ||xi+1 - xi|| < EPS
        eps = EPS;
    } else if( 0.5 <= maxSum && maxSum < 1) { // ||xi+1 - xi|| < (1 - ||B||) / ||B|| * EPS
        eps = (1. - maxSum) / maxSum * EPS;
    } else if(maxSum >= 1){
        cout << "No convergence" << endl;
        //return 0;
    }
    else
        cout << "SLAE contains null strings" << endl;
    cout << "stop criterion eps = " << eps << endl << endl;

    //fifth step: iterAlgo
    double maxDiff = 2e-3;
    unsigned int numberOfIteration = 0;

    double startTime = omp_get_wtime();
    omp_set_dynamic(0);  // correct from 4 threads to 8
    omp_set_num_threads(4);
    while(maxDiff > eps) {  // stop criterion check
        // find nextX (xi+1)
        std::cout << "scientific:\n" << std::scientific;
        cout << "max(||xi+1 - xi||) = " << maxDiff << endl << endl;

        if(parallel) {
            #pragma omp parallel for shared(nextX, curX, B, d, size) default(none)
            for(int i = 0; i < size; i++) {
                nextX[i] += d[i];
                for (int j = 0; j < size; j++)
                    nextX[i] += B[i][j] * curX[j];
            }
        }
        else {
            for(int i = 0; i < size; i++) {
                nextX[i] += d[i];
                for (int j = 0; j < size; j++)
                    nextX[i] += B[i][j] * curX[j];
            }
        }

        //maxDiff =  max(||xi+1 - xi||)
        maxDiff = eps; // reset old value
        for(int i = 0; i < size; i++) {
            double diff = (nextX[i] - curX[i] > 0) ? nextX[i] - curX[i] : curX[i] - nextX[i]; // diff > 0
            maxDiff = (diff > maxDiff) ? diff : maxDiff;
        }
        for(int i = 0; i < size; i++) {
            curX[i] = nextX[i];
            nextX[i] = 0.;  // reset old value
        }
        numberOfIteration++;
        cout << "Iteration " << numberOfIteration << endl;
    }
    double endTime = omp_get_wtime();
    cout << "Solution found at iteration " << numberOfIteration << endl;
    printVectorX(curX, size);
    for(int i = 0; i < size; i++)
        (*result)[i] = curX[i];
    cout << "max(||xi+1 - xi||) = " << maxDiff << endl << endl;
    return endTime - startTime;
}

/*void checkJacobiMethod() {
    double ** AA = new double * [SLAE_DIM];
    for(int i = 0; i < 3; i++)
        AA[i] = new double[SLAE_DIM];
    AA[0][0] = 10;
    AA[0][1] = 1;
    AA[0][2] = -1;
    AA[1][0] = 1;
    AA[1][1] = 10;
    AA[1][2] = -1;
    AA[2][0] = -1;
    AA[2][1] = 1;
    AA[2][2] = 10;
    double *bb = new double[SLAE_DIM];
    bb[0] = 11;
    bb[1] = 10;
    bb[2] = 10;
    JacobiMethod(AA, bb, SLAE_DIM, 0);

    int size = 50;
    double ** A = generateSymmetricalPositiveMatrix(size);
    double * b = generateVector(size);
    printSLAE2(A, b, size);

    double seqTime = JacobiMethod(A, b, size, 0);
    double parallelTime = JacobiMethod(A, b, size, 1);

    cout << "Sequential time = " << seqTime << endl;
    cout << "Parallel time   = " << parallelTime << endl;
}*/

int main() {
    // generate slae
    int SIZE = 16;  //16
    MathModel get_lae(SIZE);
    auto slae = get_lae();
    auto matrixVector = slae.first;
    auto vectorVector = slae.second;
    // vector to double
    auto vector = vectorVector.data();
    int size = matrixVector.size();
    auto matrix = new double *[size];
    for (int i = 0; i < size; i++)
        matrix[i] = matrixVector[i].data();
    /*double *vector = &vectorVector[0];
    auto **matrix = new double *[size];
    for (int i = 0; i < size; i++)
        matrix[i] = new double[size];
    for (int i = 0; i < size; i++)
        matrix[i] = &matrixVector[i][0];*/
    //printSLAE2(matrix, vector, size);

    //slae solver
    auto *result = new double[size];
    double seqTime = JacobiMethod(matrix, vector, size, 0, &result);
    /*for(int i = 0; i < size; i++)
        cout << result[i] << " ";
    cout << endl;*/
    double parallelTime = JacobiMethod(matrix, vector, size, 1, &result);
    std::vector<double> resultV;
    resultV.reserve(size);
    for(int i = 0; i < size; i++)
            resultV.push_back(result[i]);
    auto vectorPtr = std::make_shared<std::vector <double>>(resultV);
    Plotter plt(SIZE);
    plt.Plot(vectorPtr);
    cout << "Sequential time = " << seqTime << endl;
    cout << "Parallel time   = " << parallelTime << endl;

    //checkJacobiMethod();
    //TODO: матрицу векторов привести к матрице double и решить слау, проверить правильность
    //TODO: запустить программу с 1, 2, 4, 8 потоками записать время; c размером матрицы 4, 8, 16, 32, 64, 128
    //TODO: написать отчет с постановкой задачи (решение слау методом Якоби), описанием omp и графиками
    delete [] matrix;
    return 0;
}