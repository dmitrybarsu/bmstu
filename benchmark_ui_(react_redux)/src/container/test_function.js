import React from 'react';
import '../css/main.css'
import keane from '../img/keane.png'
import himmelblau from '../img/himmelblau.png'
import alpine2 from '../img/alpine2.png'

export default class TestFunction extends React.Component {
    render() {
        return (
            <div className="testFunction">
                <img src={keane}></img>
                <img src={alpine2}></img>
                <img src={himmelblau}></img>
            </div>
        )
    }
}