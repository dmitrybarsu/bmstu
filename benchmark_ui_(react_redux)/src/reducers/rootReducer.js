import {combineReducers} from 'redux'
import {testFunctionsReducer} from './testFunctionsReducer'
import {activeFunctionReducer} from './activeFunctionReducer'

export const rootReducer = combineReducers({
    testFunctions: testFunctionsReducer,
    activeFunction: activeFunctionReducer
})
