//
// Created by dmitry on 28.12.2020.
//

#ifndef CPP_THREADS_TRANSACTION_CONSTANTS_H
#define CPP_THREADS_TRANSACTION_CONSTANTS_H

#define WORKERS_NUM 160000
#define POSITIONS 9
const char* positions[POSITIONS] = {"python-junior-developer", "python-middle-developer", "python-senior-developer",
                                    "cpp-junior-developer", "cpp-middle-developer", "cpp-senior-developer",
                                    "js-junior-developer", "js-middle-developer", "js-senior-developer"};
#define MIN_EXP 0
#define MAX_EXP 15
#define MIN_SALARY 40000.
#define MAX_SALARY 120000.

#endif //CPP_THREADS_TRANSACTION_CONSTANTS_H