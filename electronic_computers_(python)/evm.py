'''
Задание 1
1)Решить задачу компоновки для 7 элементов+++
S = [[0, 2, 3, 0, 0, 0, 0],
     [2, 0, 1, 0, 0, 0, 0],
     [3, 1, 0, 1, 0, 1, 0],
     [0, 0, 1, 0, 1, 3, 0],
     [0, 0, 0, 1, 0, 0, 3],
     [0, 0, 1, 3, 0, 0, 1],
     [0, 0, 0, 0, 3, 1, 0]]
2)Добавить решение комб. задачи+++
2.5)Дописать итерационный алго+++
S = [[0, 0, 0, 0, 0, 1, 0, 0],
     [0, 0, 0, 1, 0, 0, 0, 1],
     [0, 0, 0, 0, 1, 0, 1, 0],
     [0, 1, 0, 0, 0, 1, 0, 1],
     [0, 0, 1, 0, 0, 0, 1, 0],
     [1, 0, 0, 1, 0, 0, 0, 0],
     [0, 0, 1, 0, 1, 0, 0, 1],
     [0, 1, 0, 0, 0, 0, 1, 0]]
V = [[0, 1], [2, 3, 4], [5, 6, 7]]
3)Добавить перебор всех вариантов N
3.5)Решить задачу для 30 элементов
left_bound = 3
right_bound = 7
def add_new_partition(array, all_partitions):
    for i in range(len(array)):
        if array[i] < left_bound or array[i] > right_bound:
            return 0
    all_partitions.append(array)
    return all_partitions


def erlih(n):
    all = []
    l = n - 1
    array = [1] * n
    while l != 0:
        k = 0
        m = 0
        add_new_partition(array[:l + 1], all)
        if array[l] == array[l - 1]:
            new_multi = array[l] + 1
            ostatok = array[l - 1] + array[l] - new_multi
            array[l] = array[l - 1] = 0
        else:
            new_multi = array[l - 1] + 1
            while array[l - 1] == array[l - 1 - k]:
                k += 1
            ostatok = k * array[l - 1] + array[l] - new_multi
            for i in range(l, l - (k + 1), -1):
                array[i] = 0
        for i in range(l + 1):
            if new_multi >= array[i]:
                for j in range(l, i, -1):
                    array[j] = array[j - 1]
                array[i] = new_multi
                break
        for i in range(l + 1):
            if array[i] == 0:
                m = i
                break
        for i in range(m, m + ostatok + 1):
            array[i] = 1
        l = m - 1 + ostatok
    return all
'''

import numpy as np

S = np.genfromtxt('S.csv', delimiter=",")
vars = [
    [3, 6, 7, 7, 7],
    [4, 5, 7, 7, 7],
    [4, 6, 6, 7, 7],
    [5, 5, 6, 7, 7],
    [5, 6, 6, 6, 7],
    [6, 6, 6, 6, 6],
    [3, 3, 3, 7, 7, 7],
    [3, 3, 4, 6, 7, 7],
    [3, 3, 5, 5, 7, 7],
    [3, 3, 5, 6, 6, 7],
    [3, 3, 6, 6, 6, 6],
    [3, 4, 4, 5, 7, 7],
    [3, 4, 4, 6, 6, 7],
    [3, 4, 5, 5, 6, 7],
    [3, 4, 5, 6, 6, 6],
    [3, 5, 5, 5, 5, 7],
    [3, 5, 5, 5, 6, 6],
    [4, 4, 4, 4, 7, 7],
    [4, 4, 4, 5, 6, 7],
    [4, 4, 4, 6, 6, 6],
    [4, 4, 5, 5, 5, 7],
    [4, 4, 5, 5, 6, 6],
    [4, 5, 5, 5, 5, 6],
    [5, 5, 5, 5, 5, 5],
    [3, 3, 3, 3, 4, 7, 7],
    [3, 3, 3, 3, 5, 6, 7],
    [3, 3, 3, 3, 6, 6, 6],
    [3, 3, 3, 4, 4, 6, 7],
    [3, 3, 3, 4, 5, 5, 7],
    [3, 3, 3, 4, 5, 6, 6],
    [3, 3, 3, 5, 5, 5, 6],
    [3, 3, 4, 4, 4, 5, 7],
    [3, 3, 4, 4, 4, 6, 6],
    [3, 3, 4, 4, 5, 5, 6],
    [3, 3, 4, 5, 5, 5, 5],
    [3, 4, 4, 4, 4, 4, 7],
    [3, 4, 4, 4, 4, 5, 6],
    [3, 4, 4, 4, 5, 5, 5],
    [4, 4, 4, 4, 4, 4, 6],
    [4, 4, 4, 4, 4, 5, 5],
    [3, 3, 3, 3, 3, 3, 5, 7],
    [3, 3, 3, 3, 3, 3, 6, 6],
    [3, 3, 3, 3, 3, 4, 4, 7],
    [3, 3, 3, 3, 3, 4, 5, 6],
    [3, 3, 3, 3, 3, 5, 5, 5],
    [3, 3, 3, 3, 4, 4, 4, 6],
    [3, 3, 3, 3, 4, 4, 5, 5],
    [3, 3, 3, 4, 4, 4, 4, 5],
    [3, 3, 4, 4, 4, 4, 4, 4],
    [3, 3, 3, 3, 3, 3, 3, 3, 6],
    [3, 3, 3, 3, 3, 3, 3, 4, 5],
    [3, 3, 3, 3, 3, 3, 4, 4, 4],
    [3, 3, 3, 3, 3, 3, 3, 3, 3, 3]
]


def find_min_S_Vi_index(indexes):  # добавить 3 > 1 1 1
    S_Vi = [0] * len(S[0])
    min_index = len(S[0]) + 1
    min_sum = len(S[0]) ** 2 + 1
    for j in indexes:
        for i in indexes:
            S_Vi[j] += S[i][j]
        if min_sum > S_Vi[j]:  # если одинаковые числа выбираем с наим. индексом  и если обнулена
            min_sum = S_Vi[j]
            min_index = j
    '''       
    print("start S_Vi")
    for i in range(len(S_Vi)):
        if S_Vi[i] != 0:
            print(int(S_Vi[i]))
    print("OK")
    '''
    incid_array = []
    incid_arrays = []
    for j in indexes:
        for i in indexes:
            if min_sum == S_Vi[j] and S[i][j] > 0:
                incid_array.append(S[i][j])
        if len(incid_array) > 0:
            incid_arrays.append([j, incid_array])
            incid_array = []
    min_len = 10 ** 6
    for i in range(len(incid_arrays)):
        if min_len > len(incid_arrays[i][1]):
            min_len = len(incid_arrays[i][1])
            min_index = incid_arrays[i][0]
    # print("min_index", min_index)
    return min_index


def Vi_max(indexes, N):  # начинаю заполнять с самых маленьких
    V = [find_min_S_Vi_index(indexes)]
    for i in indexes:  # добавил к мин строке инцидентные
        if S[V[0]][i] > 0:
            V.append(i)
    while len(V) < max(N):  # надо добавить элемент
        V = add_element(V)
    while len(V) > max(N):  # надо убрать лишний элемент
        V = del_element(V, indexes)
    for i in range(len(N)):
        if N[i] == max(N):
            N.pop(i)
            return V


def Vi_cur(indexes, N):
    V = [find_min_S_Vi_index(indexes)]
    for i in indexes:  # добавил к мин строке инцидентные
        if S[V[0]][i] > 0:
            V.append(i)
    while len(V) < N[0]:
        V = add_element(V)
    while len(V) > N[0]:
        V = del_element(V, indexes)
    N.pop(0)
    return V


def Vi(indexes, N):
    V = [find_min_S_Vi_index(indexes)]
    for i in indexes:  # добавил к мин строке инцидентные
        if S[V[0]][i] > 0:
            V.append(i)
    # print("V = ", V)
    for i in range(len(N)):
        if len(V) == N[i]:
            N.pop(i)
            return V
    if len(V) > max(N):
        while len(V) > max(N):  # надо убрать лишний элемент
            V = del_element(V, indexes)
        for i in range(len(N)):
            if N[i] == max(N):
                N.pop(i)
                # print("V_after_del = ", V)
                return V
    if len(V) < min(N):
        while len(V) < min(N):  # надо добавить элемент
            V = add_element(V)
        for i in range(len(N)):
            if N[i] == min(N):
                N.pop(i)
                return V
    else:
        while len(V) < max(N):
            V = add_element(V)
        for i in range(len(N)):
            if N[i] == max(N):
                N.pop(i)
                return V


def Vi_min(indexes, N):  # начинаю заполнять с самых маленьких контейнеров
    V = [find_min_S_Vi_index(indexes)]
    for i in indexes:  # добавил к мин строке инцидентные
        if S[V[0]][i] > 0:
            V.append(i)
    # print("inzid_vert ", V)
    while len(V) < N:  # надо добавить элемент
        V = add_element(V, indexes)
    while len(V) > N:  # надо убрать лишний элемент
        V = del_element(V, indexes)
    return V


def SVi(Vi, indexes):
    S_Vi = []
    for j in Vi:
        sum = 0
        for i in indexes:
            sum += S[i][j]
        S_Vi.append(sum)
    return S_Vi


def SVik(Vi):
    S_Vi = []
    for j in Vi:
        sum = 0
        for i in Vi:
            sum += S[i][j]
        S_Vi.append(sum)
    return S_Vi


def del_element(V_i, indexes):
    SV_i = SVi(V_i, indexes)
    # print("SV_i-------", SV_i)
    SV_ik = SVik(V_i)
    # print("SV_ik-------", SV_ik)
    max_delta = -1
    max_delta_index = -1
    for i in range(len(V_i)):
        # print("deltaV %d = %d" % (i, SV_i[i] - SV_ik[i]))
        if SV_i[i] - SV_ik[i] > max_delta:
            max_delta = SV_i[i] - SV_ik[i]
            max_delta_index = i
    # print("max_delta = ", max_delta)
    V_i.pop(max_delta_index)
    return V_i


def add_element(V_i, indexes):
    inzid_indexes = []
    '''
    for i in V_i:  # для каждой вершины в V_i
        for j in indexes:  # проходим по строке и добавляем индексы кандидатов на добавление в V_i
            if S[i][j] != 0 and j not in V_i and j not in inzid_indexes:
                inzid_indexes.append(j)
    if len(inzid_indexes) == 0:  # если нет инцидентных, а добавить надо
    for i in indexes:  # для каждой вершины в V_i
        for j in indexes:  # проходим по строке и добавляем индексы кандидатов на добавление в V_i
            if S[i][j] != 0 and j not in V_i and j not in inzid_indexes:
                inzid_indexes.append(j)
    '''
    for i in V_i:  # для каждой вершины в V_i
        for j in indexes:  # проходим по строке и добавляем индексы кандидатов на добавление в V_i
            if S[i][j] != 0 and j not in V_i and j not in inzid_indexes:
                inzid_indexes.append(j)
    inzid_links_number = []
    for i in inzid_indexes:  # для каждого кандидата считаю кол-во связей с эл-тами в V_i
        sum = 0
        for j in V_i:
            sum += S[j][i]
        inzid_links_number.append(sum)
    # print("Кандидаты", inzid_indexes)
    # print("Связи    ", inzid_links_number)
    max = -1
    add_elem_index = 0
    for i in range(len(inzid_links_number)):
        if max < inzid_links_number[i]:
            max = inzid_links_number[i]
            add_elem_index = i
    # print("Макс кол-во связей %d, i = %d" %(max, add_elem_index))
    V_i.append(inzid_indexes[add_elem_index])  # добавляю эл-т с макс кол-вом связей
    # print("Vi after add", V_i)
    return V_i


def find_comp_vectors(N):
    indexes = []
    for i in range(len(S)):
        indexes.append(i)  # все индексы S
    V = []
    for i in range(len(N)):
        # V.append(Vi(indexes, N))
        # V.append(Vi_max(indexes, N))
        # V.append((Vi_cur(indexes, N)))
        V.append(Vi_min(indexes, N[i]))  # отправляю все оставшиеся индексы и слагаемое разбиения
        new_indexes = []  # для добавления других вершин в очередной контейнер
        for j in range(len(indexes)):  # каждый раз отбрасываем те вершины, которые добавили в очередной контейнер
            if indexes[j] not in V[i]:
                new_indexes.append(indexes[j])
        indexes = new_indexes  # убираю из indexes все уже использованные вершины
    return V


'''
iteration algo
'''


def internal_indexes_array(V):
    internal_array = []
    for i in range(len(V)):
        for j in range(len(V[i])):  # по строкам
            for k in range(j + 1, len(V[i])):  # по столбцам
                internal_array.append([V[i][j], V[i][k]])
    # print(internal_array)
    return internal_array


def count_q(Vi, partition):
    Q = 0
    indexes = []
    for i in range(len(Vi)):
        for j in range(len(Vi[i])):
            indexes.append(Vi[i][j])
    leng = partition[0]
    v_index = 0
    counter = 0
    for i in indexes:
        for j in indexes[leng:]:
            Q += S[i][j]
        counter += 1
        if leng == counter:
            v_index += 1
            leng += partition[v_index]
        if leng == len(indexes):
            break
    # print("Q = ", Q)
    return Q


def count_external_links_q(Vi):
    Q = 0
    for i in range(len(S)):  # тут надо считать по индексам в Vi[]
        for j in range(i + 1, len(S)):
            Q += S[i][j]
    internal_array = internal_indexes_array(Vi)
    for i in range(len(internal_array)):
        Q -= S[internal_array[i][0]][internal_array[i][1]]
    return Q


def sum_S_V(i, Vi):
    sum = 0
    for j in Vi:
        sum += S[i][j]
    return sum


def find_Vi(j, V):
    for i in range(len(V)):
        if j in V[i]:
            return V[i]


def calculate_max_indexes_in_R(Vn, V):  # нет проверки на то что max R отрицательный----------------------------------------
    V_indexes = []
    for i in range(len(V)):
        for j in range(len(V[i])):
            V_indexes.append(V[i][j])
    R = [[0 for j in range(len(V_indexes[len(Vn):]))] for i in range(len(Vn))]
    k = 0
    max = 0
    indexes = [0, 0]
    for i in Vn:
        p = 0
        for j in V_indexes[len(Vn):]:
            R[k][p] = sum_S_V(i, find_Vi(j, V)) - sum_S_V(i, find_Vi(i, V)) + \
                      sum_S_V(j, find_Vi(i, V)) - sum_S_V(j, find_Vi(j, V)) - 2 * sum_S_V(i, [j])
            if i == 29 and j == 28:
                print("---------------------------------------------")
                print("%d - %d + %d -%d -%d" % (
                    sum_S_V(i, find_Vi(j, V)), sum_S_V(i, find_Vi(i, V)), sum_S_V(j, find_Vi(i, V)), sum_S_V(j, find_Vi(j, V)),
                    2 * sum_S_V(i, [j])))
            if max < R[k][p]:
                indexes = [i, j]
                max = R[k][p]
            p += 1
        k += 1
    arr = []
    print("  ", end=' ')
    for i in range(len(V)):
        if V[i] != Vn:
            for j in range(len(V[i])):
                print(V[i][j], end=' ')
    print("")
    for i in range(len(R)):
        arr.append(Vn[i])
        print(Vn[i], end=' ')
        for j in range(len(R[i])):
            print(int(R[i][j]), end=' ')
        print("")
        arr = []
    # print("R_max=", R[indexes[0]][indexes[1]])
    print(indexes)
    return indexes


def find_Vi_index(k, V):
    for i in range(len(V)):
        for j in range(len(V[i])):
            if k == V[i][j]:
                return [i, j]


def iter_improvemment(V, partition):
    # print(V)
    # count_q(V, partition)
    for i in range(len(V)):
        print("iteration %d" % (i + 1))
        indexes = calculate_max_indexes_in_R(V[i], V)
        while indexes != [0, 0]:
            Vi = find_Vi_index(indexes[0], V)
            Vj = find_Vi_index(indexes[1], V)
            Vtmp = V[Vi[0]][Vi[1]]
            V[Vi[0]][Vi[1]] = V[Vj[0]][Vj[1]]
            V[Vj[0]][Vj[1]] = Vtmp
            print("i = %d, Q = %d" % (i, count_q(V, partition)))
            indexes = calculate_max_indexes_in_R(V[i], V)
    print("i = %d, Q = %d" % (i, count_q(V, partition)))
    print(V)
    return count_external_links_q(V)


def solver():
    all_partitions = vars
    for i in range(len(all_partitions)):
        # all_partitions[i] = sorted(all_partitions[i], reverse=True)
        all_partitions[i] = all_partitions[i]
        # print(all_partitions[i])
    # print(all_partitions[6])
    Vectors = []
    for i in range(len(all_partitions)):
        Vectors.append(find_comp_vectors(all_partitions[i]))
    min = len(S) ** 10
    best_partition = 0
    for i in range(len(Vectors)):
        x = iter_improvemment(Vectors[i])
        print(all_partitions[i], "Q = ", x)
        if min >= x:
            min = x
            best_partition = i
    print(best_partition, min)


# solver()


def solver_for_report():
    partition = [3, 3, 3, 7, 7, 7]
    vector = find_comp_vectors(partition)
    # count_q(vector, partition)
    # print(count_external_links_q(vector))
    # print(vector)
    iter_improvemment(vector, partition)


# solver_for_report()


def test_posled_algo():
    partition = [3, 2, 2]
    V = find_comp_vectors(partition)
    print(V)
    Q = count_external_links_q(V)
    print("Q = ", Q)


# test_posled_algo()


def test_iter_algo():
    Vector = [[0, 1], [2, 3, 4], [5, 6, 7]]
    print(iter_improvemment(Vector))


# test_iter_algo()


'''
Задача размещения
S = [[0, 0, 0, 3, 0, 0, 2, 3, 0],
     [0, 0, 2, 0, 2, 0, 0, 0, 0],
     [0, 2, 0, 1, 0, 0, 0, 0, 0],
     [3, 0, 1, 0, 0, 5, 0, 0, 0],
     [0, 2, 0, 0, 0, 2, 0, 0, 4],
     [0, 0, 0, 5, 2, 0, 5, 0, 0],
     [2, 0, 0, 0, 0, 5, 0, 6, 2],
     [3, 0, 0, 0, 0, 0, 6, 0, 0],
     [0, 0, 0, 0, 4, 0, 2, 0, 0]]
'''

T1 = [[-1 for i in range(3)] for i in range(10)]
T2 = [[-1 for i in range(5)] for i in range(6)]
T_test = [[-1 for i in range(3)] for i in range(3)]
magic_number = 1000  # k[i] = '-' ~ k[i] = 1000


def r():
    ro = []
    for i in range(len(S)):
        sum = 0
        for j in range(len(S)):
            sum += S[i][j]
        ro.append(sum)
    return ro


def find_max_index(k):
    # print(k)
    maxi = - magic_number * 2
    index = 0
    for i in range(len(k)):
        if k[i] != magic_number and k[i] > maxi:
            maxi = k[i]
            index = i
    # print(k[index])
    return index


def displace(T, max_index):
    for i in range(len(T)):
        for j in range(len(T[0])):
            if T[i][j] == -1:
                T[i][j] = max_index
                return 0


def length(T, index_1, index_2):
    indexes = []
    for i in range(len(T)):
        for j in range(len(T[0])):
            if T[i][j] == index_1:
                indexes.append([i, j])
            if T[i][j] == index_2:
                indexes.append([i, j])
            if len(indexes) == 2:
                break
    return np.fabs(indexes[0][0] - indexes[1][0]) + np.fabs(indexes[0][1] - indexes[1][1])


def find_Q(T):
    Q = 0
    for i in range(len(S)):
        for j in range(i + 1, len(S)):
            Q += S[i][j] * length(T, i, j)
    return Q


k_array = []
import copy


def algo_displacement(T):  # ------------------------------------------------
    k = [0 for i in range(len(S))]
    T[0][0] = 0  # разметил V0 в лев верх угол
    k[0] = magic_number
    ro = r()
    for l in range(len(k)):  # цикл размещения очередной вершины
        for i in range(len(S)):  # подсчет k[i]
            sum = 0
            if k[i] != magic_number:
                for j in range(len(S[0])):  # считаю Sij
                    if k[j] == magic_number:
                        sum += S[i][j]
                k[i] = 2 * sum - ro[i]
                # print("K[%d]= 2 * (sum = %d) - (ro = %d) = %d" %(i, sum, ro[i], k[i]))
        max_index = find_max_index(k)  # нахожу, какой элемент надо разместить
        # print("max_i =", max_index)
        # print("k =", k)
        deepcopy = copy.deepcopy(k)
        k_array.append(deepcopy)
        k[max_index] = magic_number
        displace(T, max_index)  # размещаю этот элемент
        '''
        print("T_next")
        for i in range(len(T)):
            print(T[i])
        '''
    return T


import csv


def write_to_csv():
    FILENAME = "K.csv"
    array = np.array(k_array)
    array = array.transpose()
    '''
    for i in range(len(k_array)):
        print(k_array[i])
    array = array.transpose()
    print("Transpose--------------------------------------------------")
    for i in range(len(k_array)):
        print(k_array[i])
    '''
    new_arr = [[0 for i in range(len(array[0]))] for i in range(len(array))]
    for i in range(len(array)):
        for j in range(len(array[i])):
            if array[i][j] == 1000:
                new_arr[i][j] = "-"
            else:
                new_arr[i][j] = int(array[i][j])
    with open(FILENAME, "w", newline="") as file:
        writer = csv.writer(file)
        writer.writerows(new_arr)


'''Improvemnet'''


def find_L_V(T):
    ro = r()  # сумма связей по строке S
    L_V = [0 for i in range(len(S))]  # средняя длина связей
    for i in range(len(S)):
        sum = 0
        for j in range(len(S)):
            sum += S[i][j] * length(T, i, j)
        L_V[i] = 1 / ro[i] * sum  # считаю среднюю длину связей для каждого элемента
    return L_V


def index_L_V_max(L_V):
    maxi = 0
    index = 0
    for i in range(len(L_V)):
        if L_V[i] > maxi:  # нахожу номер V с макс ср длиной
            maxi = L_V[i]
            index = i
    return index


def find_T_swap(T, index):
    T_swap = []
    for i in range(len(T)):
        for j in range(len(T[0])):
            if T[i][j] == index:
                T_swap.append(i)
                T_swap.append(j)  # нашел индексы V с макс ср длиной в массиве T
                return T_swap


def l_xy(T, k):
    indexes = []
    for i in range(len(T)):
        for j in range(len(T[0])):
            if T[i][j] == k:
                indexes.append(j)  # lx
                indexes.append(i)  # ly
                break
    return [indexes[0] + 1, len(T) - indexes[1]]


def find_xc_yc(ro, T, index):
    x_c = y_c = 0
    for j in range(len(S)):
        l = l_xy(T, j)
        x_c += 1 / ro[index] * S[index][j] * l[0]
        y_c += 1 / ro[index] * S[index][j] * l[1]  # нашел расстояния, определяющие на какие V будем менять
        print("ro=%d, S[%d][%d]=%d, length_x=%f, length_y=%f" % (ro[index], index, j, S[index][j], l[0], l[1]))
    return [x_c, y_c]


def create_T_swap_array(T, x_c, y_c):
    T_swap_array = []
    if x_c == int(x_c) and y_c == int(y_c):
        T_swap_array.append([int(x_c), int(y_c)])
    if x_c != int(x_c) and y_c == int(y_c):
        T_swap_array.append([int(x_c), int(y_c)])
        T_swap_array.append([int(x_c) + 1, int(y_c)])
    if x_c == int(x_c) and y_c != int(y_c):
        T_swap_array.append([int(x_c), int(y_c)])
        T_swap_array.append([int(x_c), int(y_c) + 1])
    if x_c != int(x_c) and y_c != int(y_c):
        T_swap_array.append([int(x_c), int(y_c)])
        T_swap_array.append([int(x_c) + 1, int(y_c)])
        T_swap_array.append([int(x_c), int(y_c) + 1])
        T_swap_array.append([int(x_c) + 1, int(y_c) + 1])

    for i in range(len(T_swap_array)):
        tmp = T_swap_array[i][0] - 1
        T_swap_array[i][0] = len(T) - T_swap_array[i][1]
        T_swap_array[i][1] = tmp
    return T_swap_array


def find_delta_L_mas(T, L_V, T_swap, T_swap_array, index):
    delta_L = []
    delta = []
    for i in range(len(T_swap_array)):
        T_tmp = T[T_swap_array[i][0]][T_swap_array[i][1]]
        T[T_swap_array[i][0]][T_swap_array[i][1]] = index
        T[T_swap[0]][T_swap[1]] = T_tmp  # заменяю Vшку, у которой LV_i макс (index), на i-тую Vшку из массива
        L_V_new = find_L_V(T)
        '''
        print("L_V",  L_V)
        print("L_V'", L_V_new)
        print("L_V'(V1)= %f, L_V(V1)= %f, L_V'(Vx)= %f, L_V(Vx)= %f" % (L_V_new[index], L_V[index], L_V_new[T_tmp], L_V[T_tmp]))
        '''
        sum = 0
        for j in range(len(L_V)):
            sum += L_V_new[j] - L_V[j]
        delta.append(sum)
        # print("delta_sum =", delta)
        delta_L.append(L_V_new[index] - L_V[index] + L_V_new[T_tmp] - L_V[T_tmp])
        # print("delta_L", L_V_new[index] - L_V[index] + L_V_new[T_tmp] - L_V[T_tmp])
        T[T_swap[0]][T_swap[1]] = index
        T[T_swap_array[i][0]][T_swap_array[i][1]] = T_tmp
    # return delta_L
    return delta_L


def improvement(T):
    print("T = ")
    for i in range(len(T)):
        print(T[i])
    ro = r()
    L_V = find_L_V(T)
    print("L_V_start")
    for i in range(len(L_V)):
        print("%.3f," % (L_V[i]), end=' ')
    print("\nL_V_end")
    index = index_L_V_max(L_V)
    print("max_index_L(V) = ", index, "L(V)_max = ", L_V[index])
    x_c, y_c = find_xc_yc(ro, T, index)
    T_swap = find_T_swap(T, index)  # V[T_swap[0]][T_swap[1] = index - это буду менять
    T_swap_array = create_T_swap_array(T, x_c, y_c)  # сохраняю в массив все Vшки близкие к x_c, y_c
    print(x_c, y_c)
    print(T_swap_array)
    '''
    print("T = ")
    for i in range(len(T)):
        print(T[i])
    print("L_V = ", L_V)
    print("x =", x_c, "y =", y_c)
    print("index = ", index)
    print("T_swap =", T_swap)
    print("T_swap",T[T_swap[0]][T_swap[1]])
    print("T_swap_array =", T_swap_array)
    print(T[T_swap_array[0][0]][T_swap_array[0][1]])
    print(T[T_swap_array[1][0]][T_swap_array[1][1]])
    '''
    delta_L = find_delta_L_mas(T, L_V, T_swap, T_swap_array, index)
    print("delta_L", delta_L)
    mini = 10 ** 6
    ind = -1
    for i in range(len(delta_L)):
        if mini > delta_L[i] and delta_L[i] < 0:  # delta_L[i] < -0.1
            mini = delta_L[i]
            ind = i
    if ind != -1:
        T_tmp = T[T_swap_array[ind][0]][T_swap_array[ind][1]]
        T[T_swap_array[ind][0]][T_swap_array[ind][1]] = index
        T[T_swap[0]][T_swap[1]] = T_tmp
    print("Q_after =", find_Q(T))
    return T


def best_disp(T):
    while True:
        T_1 = T = improvement(T)
        T_2 = T = improvement(T)
        T_3 = T = improvement(T)
        if T_1 == T_3:
            return T_1


T = algo_displacement(T1)
write_to_csv()
print("Q_start =", find_Q(T))
T = best_disp(T)
print("T = ")
for i in range(len(T)):
    print(T[i])
print("Q_after =", find_Q(T))
