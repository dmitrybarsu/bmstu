a = float(input())
b = float(input())
c = float(input())
d = float(input())
e = float(input())
f = float(input())

eps = 1e-6

# пробуем переставить строки
if abs(a) < eps < abs(c):
    a, c = c, a
    b, d = d, b
    e, f = f, e

if abs(a) > eps:
    b /= a
    e /= a
    a = 1

    d = d - c * b / a
    f = f - c * e / a
    c = 0

    if abs(d) < eps < abs(f):
        print("нет решений 1")
    if abs(d) < eps and abs(f) < eps:
        if abs(b) < eps:
            x = e / a
            print(x, "у - любой")
        else:
            print("x - любой, у - любой")
    if abs(d) > 0:
        y = f / d
        x = e / a - b / a * y
        print(x, y)
else:
    if abs(b) > eps:
        y = e / b
        if abs(d * y - f) < eps:
            print("x - любой", y)
        else:
            print("нет решений 2")
    if abs(b) < eps < abs(e):
        print("нет решений")
    if abs(b) < eps and abs(e) < eps:
        if abs(d) < eps and abs(f) < eps:
            if abs(c) < eps:
                print("x - любой, у - любой")
            else:
                x = 0
                print(x, "y - любой")
        if abs(d) < eps < abs(f):
            print("нет решений 3")
        if abs(d) > 0:
            y = f / d
            print("x - любой", y)
