cmake_minimum_required(VERSION 3.17)
project(cpp_threads_transaction)

set(CMAKE_CXX_STANDARD 14)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -pthread")

add_executable(cpp_threads_transaction main.cpp Database.h Worker.cpp Worker.h Group.h Constants.h)