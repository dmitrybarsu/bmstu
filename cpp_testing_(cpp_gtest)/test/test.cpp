//
// Created by dmitry on 15.10.2020.
//
#include <gtest/gtest.h>
#include "../functions.c"
#include "../header.h"

TEST(Tests, isNumber) {
    EXPECT_EQ(isNumber("1"), 1);
    EXPECT_EQ(isNumber("10."), 1);
    EXPECT_EQ(isNumber(".10"), 0);
    EXPECT_EQ(isNumber(".1.0"), 0);
    EXPECT_EQ(isNumber("012"), 0);
}

TEST(Tests, findRadiusOfTheCircumscribedCircle) {
    struct Triangle triangle;
    triangle.x1 = -3;
    triangle.y1 = 0;
    triangle.x2 = 0;
    triangle.y2 = -3;
    triangle.x3 = 3;
    triangle.y3 = 0;
    ASSERT_DOUBLE_EQ(findRadiusOfTheCircumscribedCircle(triangle), 3);
}

TEST(Tests, matrixDet) {
    double matrix[3][3] = {
            {1, 0, -2},
            {0.5, 3, 1},
            {0, 2, -1}
    };
    ASSERT_DOUBLE_EQ(matrixDet(matrix), -7);
}

TEST(Tests, findCenterOfTheCircumscribedCircle) {
    const double eps = 1e-6;
    struct Triangle triangle;
    triangle.x1 = -3;
    triangle.y1 = 0;
    triangle.x2 = 0;
    triangle.y2 = -3;
    triangle.x3 = 3;
    triangle.y3 = 0;
    struct Square square;
    findCenterOfTheCircumscribedCircle(triangle, square);
    ASSERT_NEAR(square.x, 0, eps);
    ASSERT_NEAR(square.y, 0, eps);
}

TEST(Tests, createSquare) {
    const double eps = 1e-6;
    struct Triangle triangle;
    triangle.x1 = -3;
    triangle.y1 = 0;
    triangle.x2 = 0;
    triangle.y2 = -3;
    triangle.x3 = 3;
    triangle.y3 = 0;
    struct Square square = createSquare(triangle);

    ASSERT_DOUBLE_EQ(square.sideEdge, 6);
    ASSERT_NEAR(square.x, 0, eps);
    ASSERT_NEAR(square.y, 0, eps);
}
int main(int argc, char **argv) {
    ::testing::GTEST_FLAG(output) = "xml:../../test/report.xml";
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
