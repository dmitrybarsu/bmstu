import threading
import time


def complexCalculations(start=0, end=0):
    result = 0
    for i in range(start, end):
        result += 1


task1 = threading.Thread(target=complexCalculations,
                         kwargs={'end': 10 ** 6})
task2 = threading.Thread(target=complexCalculations,
                         kwargs={'start': 10 ** 6,
                                 'end': 2 * 10 ** 6})

startTime = time.time()
# запускаем поток
task1.start()
task2.start()
# join - блокируем все дальнейшие вычисления
# пока порожденный поток не завершится
task1.join()
task2.join()
endTime = time.time()
print(endTime - startTime)
