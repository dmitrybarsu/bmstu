//
// Created by dmitry on 28.12.2020.
//

#ifndef CPP_THREADS_TRANSACTION_DATABASE_H
#define CPP_THREADS_TRANSACTION_DATABASE_H

#include <vector>
#include <random>

#include "Worker.h"
#include "Constants.h"

class Database {
private:
    std::vector<Worker> workers;
public:
    Database() {
        std::random_device rd;
        std::mt19937 mt(rd());
        std::uniform_real_distribution<double> randomSalary(MIN_SALARY, MAX_SALARY);
        std::uniform_int_distribution<int> randomPositionId(0, POSITIONS - 1);
        std::uniform_int_distribution<int> randomExperience(MIN_EXP, MAX_EXP - 1);
        for(int i = 0; i < WORKERS_NUM; i++) {
            Worker worker(randomSalary(mt), randomPositionId(mt),randomExperience(mt));
            workers.push_back(worker);
        }
    }
    std::vector<Worker>& getWorkers() { return workers; };
};


#endif //CPP_THREADS_TRANSACTION_DATABASE_H
