//
// Created by dmitry on 07.10.2020.
//

#include <vector>
#include <iostream>
#include "base.h"

class ResultToConsole : public interfaceShowResultsPlugin {
public:
    virtual std::string getPluginName(std::string objectName)const {
        objectName = "ResultToConsole";
        return objectName;
    }
    virtual void PrintResults(const std::vector<std::string>& solverNames, const std::vector<std::vector<double>>& T,const struct Constants &c) {
        std::cout << "Результаты решения каждым методом в зависимости от числа итераций" << std::endl;
        for(int i = 0; i < T.size(); i++) {
            std::cout << solverNames[i] << " ";
            for(int j = 0; j < T[0].size(); j++)
                std::cout << T[i][j] << " ";
            std::cout << std::endl;
        }
        std::cout << "t" << " ";
        for(int i = 0; i < c.NumberOfIterations; i++)
            std::cout << i << ' ';
        std::cout << std::endl << std::endl;
    };
    virtual void PrintL2(std::vector<std::vector<double>>& T, struct Constants& c, const std::vector<interfaceSolverPlugin *>& solverPlugins, std::vector<int>& selectedSolversId) {
        std::cout << "Значение L2 нормы для каждого метода в зависимости от шага" << std::endl;
        // запоминаю названия решателей
        std::vector<std::string> solverNames;
        solverNames.reserve(selectedSolversId.size());
        for(int i : selectedSolversId)
            solverNames.push_back(solverPlugins[i]->getPluginName(plgName));
        solverNames.emplace_back("Step");

        //печатаю названия решателей в файл
        std::cout << solverNames[0];
        for(int i = 1; i < solverNames.size(); i++)
            std::cout << ";" << solverNames[i];
        std::cout << std::endl;

        // записываю в файл L2 ошибку для каждого метода и для каждого значения шага
        double defaultStep = c.step;
        while(c.step < defaultStep * c.NumberOfIterations) {
            // шаг фиксирован, запускаем решение каждым методом
            analit(T[selectedSolversId.size()], c);
            for(int j = 0; j < selectedSolversId.size(); j++) {
                solverPlugins[selectedSolversId[j]]->Solver(T[j], c);
                std::cout << L2(T[j], T[selectedSolversId.size()]) << ';';
            }
            std::cout << c.step << std::endl;
            c.step += defaultStep;
        }
        c.step = defaultStep;
        std::cout << std::endl;
    }
    ~ResultToConsole() override {};
};

extern "C" interfaceBasePlugin* registerPlugin()
{
    return new ResultToConsole();
}