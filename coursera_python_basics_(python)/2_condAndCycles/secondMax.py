cur = int(input())
firstMax = 0
secondMax = 0

while cur != 0:
    if cur >= firstMax:
        secondMax = firstMax
        firstMax = cur
    elif cur > secondMax:
        secondMax = cur
    cur = int(input())
print(secondMax)
