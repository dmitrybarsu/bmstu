import numpy as np
import matplotlib.pyplot as plt
import conj_gradients as cg
N = 18
n = (N - 2) ** 2
A = - cg.create_slae(N)
f = np.ones((n,))


def func(T):
    return np.dot(A, T) + f


def ab4(h, T, step, f):
    return T[step - 1] + h * (55 / 24 * f(T[step - 1]) - 59 / 24 * f(T[step - 2]) + 37 / 24 * f(T[step - 3])
                              - 3 / 8 * f(T[step - 4]))


def rk4(y_n, h, f):
    k1 = h * f(y_n)
    k2 = h * f(y_n + k1 / 2)
    k3 = h * f(y_n + k2 / 2)
    k4 = h * f(y_n + k3)
    return y_n + (k1 + 2 * k2 + 2 * k3 + k4) / 6


def ode_solve():
    T = np.zeros((times, n,))
    T_init = np.zeros((n,))
    T[1] = rk4(T_init, d_t, func)
    T[2] = rk4(T[1], d_t, func)
    T[3] = rk4(T[2], d_t, func)
    for i in range(4, times):
        T[i] = ab4(d_t, T, i, func)

    max_T = T.max()
    for i in range(3, len(T)):
        T_avg[i] = np.average(T[i])
        if i % (int(times / 10)) == 0 or i == len(T) - 1 or i <= 3:
            z_real = np.zeros((N, N))
            j = 0
            for s in range(N):
                for k in range(N):
                    if 0 < s < N - 1 and 0 < k < N - 1:
                        z_real[s][k] = T[i][j]
                        j += 1
            x_n = np.linspace(0, 1, N)
            plt.contourf(x_n, x_n, z_real, levels=[max_T / 7 * i for i in range(8)], cmap='viridis', vmin=0, vmax=max_T)
            plt.colorbar()
            plt.show()
    return T


d_t = .0001
t_fin = .30
times = int(t_fin / d_t)
T_avg = np.zeros((times,))
TT = ode_solve()
plt.plot(np.linspace(0, t_fin, times), T_avg, color="orange")
plt.grid()
plt.show()